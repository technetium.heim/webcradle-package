## Dependency
Web Cradle is dependence on Laravel 5.4

## Private Package
- to use webcradle, it required login user and password / SSH to the git repositories for download.

# How to Use Web Cradle

1.  Create a new Laravel project, open composer.json.
2. @composer.json, add:

		"repositories": [
		        { "type": "vcs", "url": "git@gitlab.com:technetium.heim/webcradle-package.git" }
		    ],
3. @composer.json> "require" , add:
`"technetium.heim/webcradle" : "dev-master"  `

4. @cmd, run `composer install` / `composer update`
5. It will ask for username and password for the git repo. Fill in and continue.
6.  @ root\config\app.php, (setup webcradle basic)
  - add under providers: `Cradle\basic\CradleServiceProvider::class,` 
  - add under aliases: `'Cradle' => Cradle\basic\facades\Cradle::class,`

7. @root\config\app.php, (Update Laravel translation to Waavi Translation)
 - Replace `Illuminate\Translation\TranslationServiceProvider::class` with  `Waavi\Translation\TranslationServiceProvider::class`  

8. @App\Http\Kernal.php, 
- add under middleware:  `\Cradle\basic\defaults\CradleGlobalMiddleware::class,`, 

9.  Setup database login in .env

10.  FIX issue [SQLSTATE[42000] Error][1] -  @cmd prompt, run `php artisan cradle:fix` to fix SQL error.

11.  @cmd prompt, run `php artisan cradle:init` to initiate all webcradle files.  it will ask for migration and seed.
12. run `php artisan migrate`
13. Delete unused files:
	- Laravel default User Model. `App\User.php`
	- Laravel default Auth Files,  `Controllers\Auth` folder

## Initiate webcradle
@cmd, project directory 
`php artisan cradle:init` The command will copy files from webcradle (basic, user, modules) to Project Files.

* basic-init (once)
  * Replace `config/auth.php` -> `App\User` to `App\Models\User` 
  * New `config/project.php` -> For user to customize project parameter for WC. 
  * New Controllers -> intermediary controller file `CradleController`, include default WC controller trait.
  * Replace exceptions -> include default WC exception trait.
  * New Model -> intermediary model file `AppCradleModel`, include default WC model trait.
  * Replace Public files
  
* basic-update
  * Replace assets files
  * Replace or New `config/cradleConfig.php` -> Default WC parameters. 

* user-once
	* New User Model

* module-once
  *  New `config\role.php`
  *  New `config\translator.php`

* module-update
	* New dataset seed file

> Init webcradle is required for web cradle to work. Only Run Once, rerun it will re-init all the files.


 [1]: https://technetiumdoc.000webhostapp.com/index.php/docs/000-laravel-index/prefix-700-reference/laravel-773-migration/#Issue_1
