<?php

namespace App\Providers;

use Laravel\Socialite\Contracts\User as ProviderUser;
use Laravel\Socialite\Contracts\Provider;

class SocialAccountServiceProvider
{
    public function createOrGetUser(Provider $provider, $social_model, $user_model)
    {
        // $model, $model_usergroup
        $providerUser = $provider->stateless()->user();
        $providerName = class_basename ($provider);

        // $account = SocialAccount::whereProvider($providerName)
        //     ->whereProviderUserId($providerUser->getId())
        //     ->first();
        $account = $social_model->whereProvider($providerName)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $account = $social_model->fill([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $providerName
            ]);

            $user = $user_model->whereEmail($providerUser->getEmail())->first();

            if (!$user) {

                $user = $user_model->create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),
                    'password' => bcrypt(str_random()),
                ]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;

        }

    }
}