<div class="form-group">
    <div class="">
        @if ( config('cradleConfig.socialite_fb') == 'enable' )
        <div class="padding-top padding-h">
            <a href="{{ route('redirect.social', ['provider' => 'facebook']) }}" class="btn btn-facebook-block" >
            Login With Facebook 
            </a>
        </div>
      	@endif

      	@if ( config('cradleConfig.socialite_google') == 'enable' )
        <div class="padding-top padding-h">
            <a href="{{ route('redirect.social', ['provider' => 'google']) }}" class="btn btn-google-plus-block" hidden>
                Login With Google+
            </a>
        </div>
        @endif
    </div>
</div>