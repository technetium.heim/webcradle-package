<?php

namespace Cradle\modules\socialite\foundation;

use Illuminate\Http\Request;
use App\Providers\SocialAccountServiceProvider;
use Socialite;

trait SocialiteTrait
{
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();   
    }   

    public function callback(SocialAccountServiceProvider $service, $provider)
    {
        $user = $service->createOrGetUser(Socialite::driver($provider), $this->modelSocial(), $this->modelUser() );
        auth()->login($user, true);

        session()->flash('hint','Success Login');

        return redirect()->route($this->redirectRoute() );
    }

}
