<?php

namespace App;

use App\Models\AppCradleModel;

class SocialAccount extends AppCradleModel
{
    protected $fillable = ['user_id', 'provider_user_id', 'provider'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getProviderName () {
    	return str_replace('Provider', '', $this->provider);
    }
}
