<?php

namespace App\Http\Controllers\user\socialite;

use App\Http\Controllers\CradleController;
use Cradle\modules\socialite\foundation\SocialiteTrait;

class SocialAuthController extends CradleController
{   
    use SocialiteTrait;

    /*
    Model for user group.
    Default: new \App\User;
    */
    protected function modelUser() 
    {
       return new \App\User;
    }

    /*
    Model for social account linked to user
    Default:  new \App\SocialAccount;
    */
    protected function modelSocial() 
    {
       return new \App\SocialAccount;
    }

    /*
    Route name after login
    Default:  cradle.home
    */
    protected function redirectRoute() 
    {
        return 'cradle.home';
    }
}
