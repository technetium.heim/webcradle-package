<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Carbon\Carbon;

use Cradle\modules\menu\models\Menu;
use Cradle\modules\menu\models\MenuItem;

class InsertDashboardToMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $menu = Menu::where('name','admin')->get();
        $menu_id = $menu[0]->id;
        
        DB::table("menu_items")->insert([
            "menu_id" => $menu_id,
            "icon" => "fa-th-large",
            "text" => "Dashboard",
            "action" => "route",
            "target" => "cradle.admin.dashboard",
            "route" => "admin/dashboard",
            "sort" => 0,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $menu = Menu::where('name','admin')->get();
        $menu_id = $menu[0]->id;
        
        MenuItem::where([['menu_id', '=', $menu_id],['text', '=', 'Dashboard']])->forceDelete();
    }
}
