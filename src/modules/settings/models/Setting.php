<?php

namespace Cradle\modules\settings\models;

use App\Models\AppCradleModel;

class Setting extends AppCradleModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
}