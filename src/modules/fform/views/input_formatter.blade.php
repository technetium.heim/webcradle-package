@php
	$autocomplete = $input->autocomplete ? '': 'autocomplete=off';
	$readonly = $input->readonly ? 'readonly': '';  
	$required = $input->required ? 'required': '';  
	$disabled = $input->disabled ? 'disabled': '';
	$checked = $input->checked ? 'checked': '';
	$inithide = $input->inithide ? 'hidden': '';

	$properties = $readonly." ".$required." ".$disabled;

	$value = $input->value? $input->value:'';
	$value = old($input->name) ? old($input->name): $value;
@endphp

{{--  <!-- Input Label Format  --> --}}
<div id="input-container-{{ $input->name }}" class="form-group {{ $inithide }} {{ $errors->has($input->name) ? ' has-error' : '' }}">
<label class="col-xs-12 col-sm-3 text-left"> 
    {{ ucwords($input->label) }} 
	@if ( $input->required )
	 *
	@endif
</label>

{{--  <!-- Input Box Format  --> --}}
<div class="col-xs-12 col-sm-9"> 
	@if ( $input->type == 'textarea' )
	    <textarea class="form-control {{ $input->class }}" name="{{ $input->name }}" rows="4" {{ $autocomplete }} {{ $properties }} >{{ $value }}</textarea>
	@elseif ( $input->type == 'dropdown' )
		<select class="form-control {{ $input->class }}" name="{{ $input->name }}" {{ $properties }}>
			<option value="" readonly>Select your option</option>
			@foreach( $input->options as $option)
		  		<option value="{{ $option->value }}" 
		  		@if ($option->value == $value)
		  			selected
		  		@endif
		  		>{{ $option->name }}</option>	
		  	@endforeach
		</select>
		@if( $input->disabled ) <input class="{{ $input->class }}" type="hidden" name="{{ $input->name }}"> @endif
	@elseif ( $input->type == 'date' )
		<div class="input-group date">
		    <div class="input-group-addon bg-white">
		       	<i class="fa fa-calendar fa-lg datepicker" aria-hidden="true"></i>
		    </div>
		    <input type="text" class="form-control {{ $input->class }}" name="{{ $input->name }}"  @if($value) value={{$value}} @endif readonly>
		</div>
	@elseif ( $input->type == 'birthday' )
		<div class="input-group date">
		    <div class="input-group-addon bg-white">
		       	<i class="fa fa-calendar fa-lg datepicker" aria-hidden="true" data-date-end-date="-10y" data-date-start-view="years"></i>
		    </div>
		    <input type="text" class="form-control {{ $input->class }}" name="{{ $input->name }}" @if($value) value={{$value}} @endif readonly >
		</div>
	@elseif ( $input->type == 'multiple' )
		<select class="selectpicker form-control {{ $input->class }}" name="{{ $input->name }}[]" multiple {{ $properties }}>
		  	@foreach( $input->options as $option)
		  		<option value="{{ $option->value }}">{{ $option->name }}</option>	
		  	@endforeach
		</select>
	@elseif ( $input->type == 'checkbox' )
		@foreach( $input->options as $option)
			<div class="checkbox">
		    	<label><input class="{{ $input->class }}" type="{{ $input->type }}" name="{{ $input->name }}" {{ $properties }} value="{{ $option->value }}">{{ ucwords($option->name) }}</label>
			</div>
		@endforeach
	@elseif ( $input->type == 'checkbox-inline' )
		@foreach( $input->options as $option)
			<div class="checkbox">
		    	<label class="checkbox-inline"><input class="{{ $input->class }}" type="checkbox" name="{{ $input->name }}" {{ $properties }} value="{{ $option->value }}">{{ ucwords($option->name) }}</label>
			</div>
		@endforeach
	@elseif ( $input->type == 'radio' )
		@foreach( $input->options as $option)
			<div class="radio">
	    		<label><input class="{{ $input->class }}" type="{{ $input->type }}" name="{{ $input->name }}" {{ $properties }} value="{{ $option->value }}">{{ ucwords($option->name) }}</label>
			</div>
		@endforeach
	@elseif ( $input->type == 'radio-inline' )
		@foreach( $input->options as $option)
	    	<label class="radio-inline"><input class="{{ $input->class }}" type="radio" name="{{ $input->name }}" {{ $properties }} value="{{ $option->value }}">{{ ucwords($option->name) }}</label>
		@endforeach
	@else
	    <input class="form-control {{ $input->class }}" type="{{ $input->type }}" name="{{ $input->name }}" value="{{ $value }}" {{ $properties }} {{ $autocomplete }}>
	@endif
	@if ($errors->has($input->name))
	    <span class="help-block">
	        <strong>{{ $errors->first($input->name) }}</strong>
	    </span>
	@endif
</div>

</div>

