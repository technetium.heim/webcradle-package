
@php
	$form_id = $form->id ? 'id='.$form->id : '';

@endphp

@if ( $form->show_panel )

    @component('WCView::general.components.panels.main')
    	@slot('content')
@endif

	@if ( $form->show_form )
        <div class="row padding">
    		<form {{ $form_id }} class="form-horizontal" role="form" method="{{ $form->method }}" >
    			{{ csrf_field() }}
    @endif	   	
    			{!! $form->rendered_inputs !!}

    @if ( $form->show_form )
    			<div class="row pull-right">
                    @if( $form->show_btn_submit == true)
    	               <button type="submit" class="btn btn-primary" @if( $form->btn_submit_modal_dismiss )  data-dismiss="modal" @endif>{{ $form->btn_submit_label }}</button>
    	            @endif
                    @if( $form->show_btn_cancel == true )
                    <button class="btn btn-primary" @if( $form->btn_cancel_modal_dismiss)  data-dismiss="modal" @endif>{{ $form->btn_cancel_label }}</button>
                    @endif
    	        </div>
    		</form>
    	</div>
    @endif

@if ( $form->show_panel )

    	@endslot
    @endcomponent
@endif

<script type="text/javascript"> 

        // read each relation
        $('.datepicker').datepicker({
            autoclose: true,
            clearBtn: true,
            maxViewMode: 'decade',
        }).on('changeDate', function(e) {
            var date = $(this).datepicker('getDate');
            date = moment(date).format('YYYY-DD-MM');
            $(this).parent().siblings('input').val(date);
        }).on('hide', function(e) {
            if ($(this).parent().siblings('input').val() == "Invalid date") {
                $(this).parent().siblings('input').val("");
            } 
            
        });


        $('.selectpicker').selectpicker({
          iconBase: 'fa',
          tickIcon: 'fa-check'
        });

</script>