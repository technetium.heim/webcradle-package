<?php
namespace Cradle\modules\fform\foundation;

use Cradle\modules\fform\foundation\CInput;

class CForm 
{
	protected $form_action;
	protected $form_method = "GET";
	protected $form_target;
	protected $form_autocomplete;
	protected $form_name;
	protected $form_id;	
	protected $form_button;
	protected $form_formatter = 'FormView::form_formatter';

	protected $inputs=[];
	protected $inputs_name = [];

	protected $show_btn_submit = true;
	protected $show_btn_cancel = true;
	protected $btn_submit_label = "Submit";
	protected $btn_cancel_label = "Cancel";
	protected $btn_submit_modal_dismiss = false;
	protected $btn_cancel_modal_dismiss = false;

	protected $show_form = true;
	protected $show_inputs = true;
	protected $show_panel = false;

	public function setURL( $url ) 
	{
		$this->form_action = $url;
	}

	public function setMethod( $method )
	{
		$this->form_method = $method;		
	}

	public function setTarget( $target )
	{
		$this->form_target = $target;
	}

	public function setName( $name )
	{
		$this->form_name = $name;
	}

	public function setFormId( $id )
	{
		$this->form_id = $id;
	}

	public function offAuto()
	{
		$this->form_autocomplete = 'off';
	}

	public function onAuto()
	{
		$this->form_autocomplete = 'on';
	}

	public function render()
	{
		return view($this->form_formatter)->withForm( $this->toHtmlData() )->render();
	}

	protected function toHtmlData()
	{
		return (object)[
			'action' => $this->form_action,
			'method' => $this->form_method,
			'target' => $this->form_target,
			'autocomplete'	=> $this->form_autocomplete,
			'id' => $this->form_id,
			'rendered_inputs' => $this->convertInputsHtml(),
			'show_btn_submit' => $this->show_btn_submit,
			'show_btn_cancel' => $this->show_btn_cancel,
			'btn_submit_label' => $this->btn_submit_label,
			'btn_cancel_label' => $this->btn_cancel_label,
			'btn_submit_modal_dismiss' => $this->btn_submit_modal_dismiss,
			'btn_cancel_modal_dismiss' => $this->btn_cancel_modal_dismiss,
			'show_form' => $this->show_form,
			'show_inputs' => $this->show_inputs,
			'show_panel' => $this->show_panel,
		];

	}

	// Input
	public function newInput( $name, $type='text')
	{
		$input = new CInput;
		$this->saveInput( $input, $name, $type);
		return $this->inputs[$name];
	}

	public function getInput( $name )
	{
		return $this->inputs[$name];
	}

	public function addInput( Cinput $input,  $name, $type='text' )
	{	
		$this->saveInput( $input, $name, $type);
		return $this->inputs[$name];
	}

	protected function saveInput( Cinput $input, $name, $type )
	{
		$input->setName( $name );
		$input->setType ( $type );
		$this->inputs[$name] = $input;
		array_push( $this->inputs_name, $name);
	}

	public function removeInput ( $name )
	{
		array_diff($this->inputs_name, array($name));
		unset( $this->inputs[$name] );
	}

	public function renderInputsOnly()
	{
		$this->show_inputs = true;
		$this->show_form = false;
		return $this->render();
	}

	protected function convertInputsHtml()
	{
		$show_inputs = '';
		foreach ( $this->inputs_name as $name){
			$show_inputs .= $this->inputs[$name]->toHtml();			
		}

		return $show_inputs;
	}

	// Button
	// public function 
	public function showSubmitButton( $label= 'Submit')
	{
		$this->btn_submit_label = $label;
		$this->show_btn_submit = true;
	}

	public function showCancelButton( $label = 'Cancel')
	{
		$this->btn_cancel_label = $label;
		$this->$show_btn_cancel= true;
	}

	public function hideSubmitButton()
	{
		$this->show_btn_submit = false;
	}

	public function hideCancelButton()
	{
		$this->show_btn_cancel = false;
	}

	public function CancelButtonModalDismiss( $toggle )
	{
		$this->btn_cancel_modal_dismiss = $toggle;
	}

	public function SubmitButtonModalDismiss( $toggle )
	{
		$this->btn_submit_modal_dismiss = $toggle;
	}
}