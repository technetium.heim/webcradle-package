<?php
namespace Cradle\modules\fform\foundation;

class CInput 
{
	protected $input_name;
	protected $input_label;
	protected $input_id;
	protected $input_type = 'text';	
	protected $input_value;	
	protected $input_placeholder;
	protected $input_class;

	protected $input_align;

	// bool status
	protected $input_autocomplete = true;
	protected $input_readonly = false;
	protected $input_required = false;
	protected $input_disabled = false;
	protected $input_checked = false;
	protected $input_inithide = false;

	protected $input_formatter = 'FormView::input_formatter';

	protected $input_options = [] ;
	public function setName( $name )
	{
		$this->input_name = $name;
		return $this;
	}

	public function setLabel( $label )
	{
		$this->input_label = $label;
		return $this;
	}

	public function setId( $id )
	{
		$this->input_id = $id;
		return $this;
	}

	public function setType( $type )
	{
		$this->input_type = $type;
		return $this;
	}

	public function setValue( $value )
	{
		$this->input_value = $value;
		return $this;
	}

	public function setProp( $prop , $action = 'set' )
	{
		if ($action == 'set') $toggle = true;
		elseif ($action == 'unset') $toggle = false;

		switch ($prop) {
			case 'autocomplete':
				$this->input_autocomplete = $toggle;
				break;
			case 'readonly':
				$this->input_readonly = $toggle;
				break;
			case 'required':
				$this->input_required = $toggle;
				break;
			case 'disabled':
				$this->input_disabled = $toggle;
				break;
			case 'inithide':
				$this->input_inithide = $toggle;
				break;
			default:
	
				break;
		}
		return $this;
	}	

	public function setProps( array $props )
	{
		foreach ( $props as $prop ){
			$this->setProp( $prop );
		}
		return $this;
	}

	public function setClass( $class )
	{
		$this->input_class = $class;
		return $this;
	}

	public function setOptions( array $options)
	{
		$this->input_options = $options;
		return $this;
	}

	public function addOptions( array $options)
	{
		foreach($options as $value => $name){
			$this->addOption( $value, $name );
		}
		return $this;
	}

	public function addOption( $value, $name )
	{
		$option = (object)[
			'value' => $value,
			'name' => $name,
		];

		array_push($this->input_options, $option);

		return $this;
	}

	public function unsetProp( $prop )
	{
		$this->setProp( $prop , 'unset' );
		return $this;
	}

	public function unsetProps(array $props )
	{
		foreach ( $props as $prop ){
			$this->setProp( $prop , 'unset');
		}
		return $this;
	}	

	public function setPlaceholder( $placeholder )
	{
		$this->input_placeholder = $placeholder;
		return $this;
	}

	public function toHtml()
	{
		return view($this->input_formatter)->withInput( $this->toHtmlData() )->render();
	}

	protected function toHtmlData()
	{
		return (object)[
			'name' 	=> $this->input_name,
			'label' => $this->input_label? $this->input_label: $this->input_name,
			'id'	=> $this->input_id,
			'type'	=> $this->input_type,
			'value' => $this->input_value,
			'class' => $this->input_class,
			'placeholder ' => $this->input_placeholder,
			'autocomplete' => $this->input_autocomplete,
			'readonly' => $this->input_readonly,
			'required' => $this->input_required,
			'inithide' => $this->input_inithide,
			'checked' => $this->input_checked,
			'disabled' => $this->input_disabled,
			'options' => $this->input_options,
		];
	}


}