<?php
namespace Cradle\modules\fform;


use Cradle\modules\fform\foundation\CForm;

use Cradle\modules\fform\Finput;

class FForm 
{
		
	public function makeForm()
	{
		return new CForm;
	}

	public function makeInput()
	{
		return new CInput;
	}

	public function makeButton()
	{
		
	}
}