<?php 

namespace Cradle\modules\fform\facades;

use Illuminate\Support\Facades\Facade;


class FForm extends Facade 
{
	protected static function getFacadeAccessor()
	{
		return "fast-form";
	}

}