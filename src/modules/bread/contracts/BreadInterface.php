<?php 
namespace Cradle\modules\bread\contracts;
use Illuminate\Http\Request;

interface BreadInterface
{
    public function index(Request $request);

    public function browse(Request $request);
    
    public function read(Request $request, $id);
    
    public function edit(Request $request);
 
    public function add(Request $request);

    public function delete(Request $request); 
}
