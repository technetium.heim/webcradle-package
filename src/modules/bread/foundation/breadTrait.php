<?php

namespace Cradle\modules\bread\foundation;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Cradle;
use Route;

trait BreadTrait
{
   // Parameter to controle bread index table show timestamp
   protected $showTableTimestamp = false;
   protected $showReadTimestamp = true;
   protected $showInputTimestamp = true;


   public function index(Request $request) 
   {  
      $settings = $this->settings();

      // Parameter for Page
      // rows is the data for the table.
      // cols is the column name to be show
      $bread = (object) [
         'cols' => $this->cols('table'),
         'inputs' => $this->inputs(),
         'settings' => $settings
      ];  
      
      $page_info = $this->setTitle( $settings['title'].' Bread');
      $page_info['load_page'] = "browse";
      return view($this->commonPage(), $page_info)->withBreads($bread);
   }

   public function browse(Request $request) 
   {

      $rows =  $this->model();    
      
      //* START: Read as child table AJAX (in READ Page)
      if ($request->access == 'child-table') {               
         $rows = $rows->find($request->child_id);
         $rows = $this->appendExtraColsData($rows); 
         
         // Replace dataset id to value 
         if (!empty($this->datasetCols())) {
            $rows = $this->replaceDatasetValue($rows);
         }

         $response_data = (object) [
            'cols' =>  $this->cols('table'),
            'rows' =>  $rows,  
            'table_id' => $request->table_id,
            'relation_type' => $request->relation_type,
            // 'inputs' => $this->inputs(),
         ];


         if ($request->expectsJson()) {
            return response()->json($response_data);
         }
      }
      //* END : Read as child table

      //* START: Read for common browse AJAX
      $currentPage = (int)$request->current;
      $rowCount = $request->rowCount;
      $sort = $request->sort;
      $sortColumn = key($sort);
      $sortOrder = $sort[key($sort)];
      $searchPhrase = $request->searchPhrase;
    
      // search
      if ($searchPhrase != null) {
         foreach($this->searchableCols() as $column) {
            $rows = $rows->orwhere($column, 'LIKE', "%$searchPhrase%");
         }
      }

      // Get total size
      $total = $rows->count();

      // Sort and Paginate
      $rows = $rows->orderBy($sortColumn,$sortOrder)->paginate($rowCount,['*'], 'page', $currentPage);

      // Additional column data from other Model
      $rows = $this->appendExtraColsData($rows);

      // Replace dataset id to value 
      if (!empty($this->datasetCols())) {
         $rows = $this->replaceDatasetValue($rows);
      }

      $rows->map(function ($row){
            $row->read = $this->settings()['route_read'].'/'.$row->id;
         return $row;
      });

      $response_data = (object) [
         'current' => $currentPage,
         'rowCount' => $rowCount,
         'rows' =>  $rows->items(),
         'total' =>  $total
      ];   
      
      if ($request->expectsJson()) {
         return response()->json($response_data);
      }
      
      //* END: Read for common browse AJAX

      // return redirect()->back()->withBrowse($response_data);
   }

   public function read(Request $request, $id='')
   {  
      // AJAX Read Input funtion
      if( isset($request->access) ){
         switch ($request->access) {
            case 'pivot-setlist':
               $response_data = $this->readMyChildSetlist($request);
               break;
            
            case 'my-child-input':
               $response_data = $this->readMyChildInput($request);
               break;

            case 'my-child-input-data':
               $response_data = $this->readMyChildInputData($request);
               break;

            case 'as-child-input':
                $response_data = $this->readAsChildInput($request);
               break;

            case 'as-child-input-data':
               $response_data = $this->readAsChildInputData($request);
               break;

            default:
               
               break;   
         }
         if ($request->expectsJson()) {   
            return response()->json($response_data);
         }
      }

      $data = $this->model()->find($id);

      // Ajax Read ID
      // read additional column data
      $this->processReadData($data);

      // Add relation table to get col.
      if ($request->expectsJson()) {
         return response()->json($data);
      }
      //* END:  Ajax read relation (modal_edit)

      // If load read page.
      $settings = $this->settings();

      $relations = $this->readRelationSettings($id);

      $bread = (object) [
         'cols' => $this->cols('read'),
         'data' => $data,
         'relations' => $relations,
         'inputs' => $this->inputs(),
         'settings' => $settings,
      ];  


      // dd($bread);
      $page_info = $this->setTitle( 'Bread READ');
      $page_info['load_page'] = "read";

      return view($this->commonPage(), $page_info)->withBreads($bread);
   }

   public function edit(Request $request) 
   {  
      if( isset($request->access) ){
         switch ($request->access) {
            case 'edit-my-child':
               $response_data = $this->editMyChild($request);

               if ($request->expectsJson()) {
                  $response = ($response_data)? ['status' => 'success'] :  [ 'id' => 'Please fill in all data.'];
                  return response()->json($response);
               }
               break;
            case 'edit-as-child':
               //Continue with common code
               break;
            default:
               # code...
               break;
         }
      }

      $v = $this->validator($request->all(), $this->validateEditRules()); 
     
      if ($request->expectsJson()) {
         //Validate using ajax 
         if ($v->fails())  return response()->json($v->errors());
      }else {
         $v->validate();
      }

      $model = $this->model();
      $row =  $model::find($request->input('id'));

      foreach ($this->cols('save') as $column) {
               // save this model data     
              $row->$column =  $request->input($column); 
      }

      $row->save();
     
      $this->updateExtraColsData($row, $request);
     
      $response['status'] = "success";

      if ($request->expectsJson()) {
         return response()->json($response,200);
      }

      return redirect()->back();
   }

   public function add(Request $request)
   {  
      if( isset($request->access) ){
         switch ($request->access) {
            case 'set-pivot':
               $method = $request->method;
               $this->model()->find($request->parent_id)->$method()->sync($request->child_id);
               return redirect()->back();
               break;
            case 'add-my-child':
               $response_data = $this->addMyChild($request);

               if ($request->expectsJson()) {
                  $response = ($response_data)? ['status' => 'success'] :  [ 'id' => 'Please fill in all data.'];
                  return response()->json($response);
               }
               break;
            case 'add-as-child':
                //Continue with common code
               break;
            default:
               # code...
               break;
         }
      }
      // validate
      $v = $this->validator($request->all(), $this->validateAddRules()); 
     
      if ($request->expectsJson()) {
         //Validate using ajax 
         if ($v->fails())  return response()->json($v->errors());
      }else {
         $v->validate();
      }

      $model = $this->model();

      foreach ($this->defaultValue() as $key => $value) {         
         $model->$key =  $value;
      }

      foreach ($this->cols('save') as $column) {         
         if ($request->input($column) != null) {    
               // save this model data     
              $model->$column =  $request->input($column); 
         } 
      }
      
      $model->save();

      $row = $this->model()->find($model->id);
      
      $this->updateExtraColsData($row, $request);

      $response['status'] = "success";

      if ($request->expectsJson()) {
         return response()->json($response,200);
      }

      return redirect()->back();
   }

   // MAIN FUNCTION: DELETE MAIN ROW, DELETE RELATION MANY ROW, UNSET PIVOT 
   public function delete(Request $request)
   {
      if ($request->relation_type) {
         $method = $request->method;
         switch ($request->relation_type) {
            case 'relation_pivot':
               $this->model()->find($request->id)->$method()->detach($request->child_id);         
               return redirect()->back();
               break;
            case 'relation_many':
               $this->model()->find($request->id)->$method()->find($request->child_id)->delete();         
               return redirect()->back();
               break;
            default:
               # code...
               break;
         }
      }

      $model = $this->model();
      $row = $model->find($request->input('id'));
      
      // delete child also ->Depreciated, replace by soft delete trait.
      // foreach($this->relationSettings() as $key => $value){
      //    $type = $value[1];
      //    $method = $key;
      //    if ($type != 'relation_pivot') {
      //       $row->$key()->delete();
      //    }
      // }

      $row->delete();

      return redirect()->back();
   } 

   // This should be in controller 
   // Parameter:
   // Model, exclude cols, action, inputs
  

   // cols -> column shown in table 
   protected function cols($type)
   {
      $cols = Schema::getColumnListing($this->table());

      switch ($type) {
         case 'all':
            $custom_extra = $this->customExtraCols();
            $extra = $this->extraCols();
            break;

         case 'table':
            $extra = $this->extraCols();
            $custom_extra = $this->customExtraCols();
            $exclude = $this->getExcludeCols();
            break;
         
         case 'input':
            $custom_extra = $this->customExtraCols();
            $extra = $this->extraCols();
            $exclude = $this->getExcludeInput();
            break;

         case 'save':
            $exclude = $this->excludeSave();
            break;

         case 'extra':
            $cols = [];
            $custom_extra = $this->customExtraCols();
            $extra = $this->extraCols();
            break;

         case 'read':
            $exclude = $this->getExcludeColsRead();
            break;

         default:
            return $cols;
            break;
      }

      // Add Extra cols to Columns
      if (isset($extra)) {
         $relation_settings = $this->relationSettings();
         foreach($extra as $key => $add_col ) {
            $method = $add_col[0];
            if(!isset($add_col[2])){
               if(isset($add_col[1])) $excludes = is_array($add_col[1])? $add_col[1]: array($add_col[1]);
               switch ($relation_settings[$method][1]) {
                  case 'relation_one':
                     $db_cols = $this->getRelationTableCols($method);                  
                     $db_cols = array_diff($db_cols, array($this->model()->$method()->getForeignKeyName()));
                     $db_cols = array_diff($db_cols, array_keys($this->inputsDefaultProps()));
                     $db_cols = array_diff($db_cols, $excludes);
                     foreach($db_cols as $one_add_col){
                        array_push($cols, $one_add_col);
                     }
                     break;
                  
                  default:
                     array_push($cols, $key);
                     break;
               } 
            }else{
               array_push($cols, $key);
            }          
         }
      }

      // Add Custom Extra cols to Columns
      if (isset($custom_extra)) {
         foreach($custom_extra as $add_col ) {
            array_push($cols, $add_col);
         }
      }

      if (isset($exclude)) {
         foreach ($exclude as $remove_col) {
            if(($key = array_search($remove_col , $cols)) !== false) {
               unset($cols[$key]);
            } 
         }
      }

      return $cols;      
   }

   // Add Extra Columns data (pivot, one to one , one to many)
   protected function appendExtraColsData($rows) 
   {
      $rows = $this->appendCustomExtraColsData($rows);

      $relation_settings = $this->relationSettings();
      foreach($rows as $row) {
         foreach( $this->extraCols() as $key => $value) {
            $method = $value[0];
            $db_col = $value[1];
            $type = isset($value[2])? $value[2]: 'relation' ;
            if( $type == 'relation'){
               // get relation setting ->type
               $relation_type = $relation_settings[$method][1];
               
               if ($relation_type  == 'relation_pivot') {
                  if ($this->model()->has($method)) {
                     $row->$key = $this->model()->find($row->id)->$method()->select($db_col)->get()->toArray();
                   }
               }

               if ( $relation_type == 'relation_one' ){
                  if ($this->model()->has($method)) {
                     $row = $this->readRelationOneData($row, $method);
                  }
               }

               if ( $relation_type == 'relation_many' ){
                  if ($this->model()->has($method)) {
                     $row->$key = $this->model()->find($row->id)->$method()->select($db_col)->get()->toArray();
                  }
         
               }
            }elseif( $type == 'custom' ){
               $row->$key = 123;
            }
         }
      }
      // return array of data
      return $rows;
   }


   protected function readRelationOneData($row, $method)
   {
      $db_cols = $this->getRelationTableCols($method);                  
      $db_cols = array_diff($db_cols, array($this->model()->$method()->getForeignKeyName()));
      $db_cols = array_diff($db_cols, array_keys($this->inputsDefaultProps()));
      
      if($this->model()->find($row->id)->$method) {
         $child = $this->model()->find($row->id)->$method;
      
         foreach ($db_cols as $col) {
            $row->$col = isset($child->$col)? $child->$col: "";
         }   
      }    

      return $row;          
   }

   // Addition Process for Bread Read (in AJAX only)
   protected function processReadData($data) 
   {
      // Reformat data for 'multiple' type input in Bread Read 
      $cols =  $this->typeInput();
      $extraCols = $this->extraCols();
      foreach ($cols as $key => $col) {
         if (in_array('multiple', $col)){
            $return_value = [];
            if ( $data->{$col[2]} != null ){
               foreach($data->{$col[2]} as $col_key) {
                  array_push($return_value, $col_key->id);
               }
            }
            $new_key = $key.'_val';
            $data->$new_key = $return_value;
         }
      }
      

      foreach( $this->relationSettings() as $key => $value) {
         if ($value[1] == 'relation_one' ){
            $method = $key;
            
            $data = $this->readRelationOneData($data, $method);
         
         }
      }

      return $data;
   }

   // cols -> column shown in action input form 
   protected function inputs()
   {
      $columns = $this->cols('input');

      $inputs = [];
      // type
      // prop : required , locked ,
      $props = $this->propInput();
      $default_props = $this->inputsDefaultProps();
      $props = array_merge( $default_props , $props );
      $types = $this->typeInput();

      // Setup data for input formatter
      foreach ($columns as $col) {
            
         $prop = (isset($props[$col]))?  $props[$col] : [] ;
         $type = (isset($types[$col][0]))?  $types[$col][0] : 'text';
         $options=[];

         // Process Input field for dropdown and multiple select
         if ($type == 'dropdown' || $type == 'multiple') {
            $access_type = (isset($types[$col][1]))?  $types[$col][1] : '';
            $access = (isset($types[$col][2]))?  $types[$col][2] : [];
            if ($access_type == 'dataset') {
               // Cradle Service Provider method (get dataset list by key)
               $options = Cradle::datasets($access);

            }elseif($access_type == 'relation'){

               $value_col  = (isset($types[$col][3]))?  $types[$col][3] : 'id';
               $display_col = (isset($types[$col][4]))?  $types[$col][4] : 'name';
               $options = $this->model()->$access()->getModel()->select($value_col, $display_col)->get();
               
               foreach($options as $option) {
                  $option->value = $option->$value_col;
                  $option->name = $option->$display_col;
               }

            }else{ // normal options
               $options = $access;
               foreach( $options as $key => $option) {
                  $option->value = $key;
                  $option->name = $option;
               }
            }
         }

         // foreign key col locked
         foreach($this->relationParentSettings() as $method) {
            if($col == $this->model()->$method()->getForeignKey() ){
               $type = 'dropdown';
               $prop = ['required'];
             
               $parent_rows = $this->model()->$method()->getModel()->all();

               $options = [];
               foreach($parent_rows as $row) {
                  $option = (object)[];
                  $option->value = $row->id;
                  $option->name = (isset($row->key))?  $row->id ." - ". $row->key: $row->id;
                  array_push($options, $option);
               }
            } 
         }
         
         $input = (object)[
            'name' => $col,
            'type' => $type,
            'properties' => $prop,
            'options' => $options
         ];

         array_push($inputs, $input);
      }

      return $inputs;
   }

   

   protected function readRelationSettings($id) {
         $relation_settings = $this->relationSettings();
         $relation_read_cols = $this->relationColsRead();

         $relations= (object)[];
       
         foreach($relation_settings as $relation_method => $relation_value) {
            // check if route exist, get data from controller or just Read raw.
            $r_index_name = 'bread.'.$relation_value[0].'.index';
            $r_browse_name = 'bread.'.$relation_value[0].'.browse';
            $r_read_name = 'bread.'.$relation_value[0].'.read';
            $route_index = (Route::has($r_index_name))? route($r_index_name): '';
            $route_browse = (Route::has($r_browse_name))? route($r_browse_name): '';
            $route_read = (Route::has($r_read_name))? route($r_read_name): '';

            $relation = (object)[
                  'method' => $relation_method, 
                  'item' => $relation_value[0],
                  'type' => $relation_value[1],                   
                  'id_list' => implode(',',$this->model()->find($id)->$relation_method()->pluck('id')->toArray()),
                  'route_index' => $route_index, // read page link to child index page
                  'route_browse' => $route_browse, // read page get child table data from child controller 
                  'route_read' =>  $route_read, // action modal get child input and data.
            ];

            // If no child controller
            $relation->read_relation = 0;
            if (array_key_exists($relation_method, $relation_read_cols)) {
               if (!empty($relation_read_cols[$relation_method])) $relation->cols = $relation_read_cols[$relation_method];
               else $relation->cols = $this->getRelationTableCols($relation_method);
               $relation->table = $this->model()->find($id)->$relation_method;
            }else{
               if ($route_index == "" ) {
                  $relation->cols = $this->getRelationTableCols($relation_method);
                  $relation->table = $this->model()->find($id)->$relation_method;
               }else{
                  $relation->read_relation = 1; // read from controller
               }
            }
            
            if(array_key_exists('table', $relation)){
               // Replace dataset id to value 
               if (!empty($this->datasetCols())) {
                  foreach($this->datasetCols() as $col) {
                     $relation->table->$col = Cradle::datasetName($col, $relation->table->$col);
                  }
               }
            }     
                

            $relations->$relation_method = $relation;
         }
         return $relations;
   }

  

   protected function replaceDatasetValue($rows)
   {
      $rows->map(function ($item){

         $dataset_cols = $this->datasetCols();

         foreach ($dataset_cols as $col) {
            $item->$col = Cradle::datasetName($col, $item->$col);
         }   
         return $item;
      }); 

      return $rows;
   }

   protected function updateExtraColsData($row, $request) {
      // save additional pivot column data
      $relation_settings= $this->relationSettings();
      foreach($this->extraCols() as $column_name => $value) {
         $method = $value[0];
         $db_col = $value[1];
         $relation_type = $relation_settings[$method][1];
         
         switch ($relation_type) {
            case 'relation_pivot':
                  $row->$method()->sync($request->$column_name);
               break;

            case 'relation_one':
               $db_cols = $this->getRelationTableCols($method);                  
               $db_cols = array_diff($db_cols, array($this->model()->$method()->getForeignKeyName()));
               $db_cols = array_diff($db_cols, array_keys($this->inputsDefaultProps()));

               $save_data = [];
               foreach ($db_cols as $col) {
                  $save_data[$col] = $request->$col;
               }
               
               if( !$row->$method ){
                  $row->$method()->create($save_data);
               }else{
                  $row->$method()->update($save_data);
               }

               break;

            default:
               # code...
               break;
         }
      }
   }

   // Use to get input list if this is child controller 
   protected function readAsChildInput(Request $request) {
      $method = $request->method; 
      $parent_id = $request->parent_id;

      //get parent method
      $parent_method = $this->relationParentSettings()[$method];
      $foreign = $this->model()->$parent_method()->getForeignKey();

      $html= "";
      foreach( $this->inputs() as $input){
         if ($input->name == $foreign) array_push($input->properties, 'locked' );
         $html .= view('BreadView::input_formatter')->withInput($input)->render();
      }

      return (object)[
         'html' => $html,
         'foreign' => $foreign,
         'parent_id' => $parent_id,
         'route_add' => $this->settings()['route_add']
      ];
   }

   // Use to get input list if child contoller not exist
   protected function readMyChildInput(Request $request) {
      $method = $request->method; 
      $id = $request->parent_id;
      $html= "";
      foreach( $this->getRelationTableCols($method) as $input_name){
         $input = $this->prepareChildInput($input_name, $method);
         $html .= view('BreadView::input_formatter')->withInput($input)->render();
      }

      return (object)[
         'html' => $html,
         'foreign' => $this->model()->$method()->getForeignKeyName(),
         'parent_id' => $id
      ];
   }

   protected function readAsChildInputData(Request $request) 
   {
      $method = $request->method; 
      $id = $request->child_id; 

      //get parent method
      $parent_method = $this->relationParentSettings()[$method];
      $foreign = $this->model()->$parent_method()->getForeignKey();

      $html= "";
      foreach( $this->inputs() as $input){
         if ($input->name == $foreign) array_push($input->properties, 'locked' );
         $html .= view('BreadView::input_formatter')->withInput($input)->render();
      }

      return (object)[
         'html' => $html,
         'data' => $this->model()->find($id),
         'route_edit' => $this->settings()['route_edit']
      ];

   }

   protected function readMyChildInputData(Request $request) 
   {
      $method = $request->method; 
      $id = $request->child_id; 

      $html= "";
      foreach( $this->getRelationTableCols($method) as $input_name){
         $input = $this->prepareChildInput($input_name, $method);
         $html .= view('BreadView::input_formatter')->withInput($input)->render();
      }

      return (object)[
         'html' => $html,
         'data' => $this->model()->$method()->getModel()->find($id),
      ];
   }

   // only use to get setlist
   protected function readMyChildSetlist(Request $request) {
      $method = $request->method;
      $parent_id = $request->parent_id;
      $selected = $this->model()->find($parent_id)->$method()->allRelatedIds();
      $setlist = $this->model()->$method()->getModel()->select('id','name')->get();

      return  (object) [
            'setlist' => $setlist,
            'selected' => $selected
         ]; 
   }

   // Use to add new row add child row from parent controller 
   protected function editMyChild($request)
   {
      $method = $request->method;
      $child_cols = $this->getRelationTableCols($method);
      $child_id = $request->id;
      $child_model = $this->model()->$method()->getModel()->find($child_id);

      foreach ($child_cols as $column) {         
         if ($request->input($column) != null && !array_key_exists($column, $this->inputsDefaultProps())) {    
            // save this model data     
            $child_model->$column =  $request->input($column); 
         }else {
            if(!array_key_exists($column, $this->inputsDefaultProps())) {
               return false;
            }
         }
      }
      
      return $child_model->save();
   }

   // Use to add new row add child row from parent controller 
   protected function addMyChild($request)
   {
      $method = $request->method;
      $child_cols = $this->getRelationTableCols($method);
      $child_model = $this->model()->$method()->getModel();

      foreach ($child_cols as $column) {         
         if ($request->input($column) != null) {    
            // save this model data     
            $child_model->$column =  $request->input($column); 
         }else {
            if(!array_key_exists($column, $this->inputsDefaultProps())) {
               return false;
            }
         }
      }

      return $child_model->save();
   }

   protected function prepareChildInput( $input_name, $method)
   {
      $properties = (isset($this->inputsDefaultProps()[$input_name]))? $this->inputsDefaultProps()[$input_name] : [];
      if ($input_name == $this->model()->$method()->getForeignKeyName())
         $properties = ['locked'];

      return (object)[
         'name' => $input_name,
         'type' => 'text',
         'properties' => $properties,
         'options' => []
      ];
   }

   // BASE FUNCTIONS 
   protected function getRelationTableCols($method) 
   {
      $relation_table = with($this->model()->$method()->getModel())->getTable();
      return Schema::getColumnListing($relation_table);
   }

   protected function validator($request, $rules)
   {   
      return Validator::make($request, $rules);        
   }

   protected function table()
   {
      return with($this->model())->getTable();
   }

   protected function inputsDefaultProps( $is_parent = false )
   {
      $default_inputs = [
         'updated_at' => ['locked'],
         'created_at' => ['locked'],
         'deleted_at' => ['locked'],
      ];

      if( !$is_parent ) array_push( $default_inputs, ['id'  => ['locked']]  );

      return $default_inputs;
   }  

   protected function getExcludeCols()
   {
      $timestamp_Cols = $this->getTimestampCols();
      $excludeCols = $this->excludeCols();
      
      if( !$this->showTableTimestamp ) {
         return array_unique( array_merge( $timestamp_Cols, $excludeCols ) );
      }

      return $excludeCols;
   }

   protected function getExcludeInput()
   {
      $timestamp_Cols = $this->getTimestampCols();
      $excludeInput = $this->excludeInput();
     
      if( !$this->showInputTimestamp ) {
         return array_unique( array_merge( $timestamp_Cols, $excludeInput ) );
      }

      return $excludeInput;
   }

   protected function getExcludeColsRead()
   {
      $timestamp_Cols = $this->getTimestampCols();
      $excludeColsRead = $this->excludeColsRead();
     
      if( !$this->showReadTimestamp ) {
         return array_unique( array_merge( $timestamp_Cols, $excludeColsRead ) );
      }

      return $excludeColsRead;
   }

   protected function getTimestampCols()
   {
      return ['created_at','updated_at','deleted_at'];
   }

   protected function customExtraCols()
   {
      return [];
   }

   protected function appendCustomExtraColsData($rows)
   {
      return $rows;
   }
   

   //START: Set default value
      protected function excludeCols()
      {
         return [];
      }

      protected function excludeColsRead()
      {
         return [];
      }
   //END
}  