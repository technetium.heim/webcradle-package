@component('WCView::general.components.modals.extend.center') 
    @slot('modal_id', 'modal-bread-edit')
    @slot('modal_content_class', 'master-admin')
    @slot('modal_header')
        @component('WCView::general.components.navbars.extend.modal.primary')
            @slot('title')
                Edit {{ $breads->settings['item'] }}
            @endslot
            @slot('left_action','dismiss')
            @slot('left_icon','fa-times')
        @endcomponent
    @endslot
    @slot('modal_body')
        <div class="row padding">
            <div class="col-md-8 col-md-offset-2">
                <input type="hidden" name="id" />

                <form id="form-bread-edit" class="form-horizontal" role="form" method="POST">
                    {{ csrf_field() }}
                    
                        @foreach ($breads->inputs as $input)                                    
                            @include('BreadView::input_formatter')
                        @endforeach

                    <div class="row pull-right">
                        <button id="btn-bread-edit" class="btn btn-primary">Ok</button>
                        <button class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>

                <form id="form-bread-edit-relation" class="form-horizontal" role="form" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="child_id"/>
                    <input type="hidden" name="item"/>
                    <input type="hidden" name="method"/>
                    <input type="hidden" name="relation_type"/>
                    <input type="hidden" name="route_child_read"/>
                    <div class="relation-input">

                    </div>
                    <div class="row pull-right">
                        <button id="btn-bread-edit-relation" class="btn btn-primary">Ok</button>
                        <button class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    @endslot
    @slot('modal_footer')
    @endslot
    @slot('modal_script')
        <script type="text/javascript">
            $('#modal-bread-edit').on('show.bs.modal', function(e) {
                verifyEditParent();
                // readDB();
            })

             function verifyEditParent() {
                // initialize form
                $('#modal-bread-edit #form-bread-edit').addClass('hidden');
                $('#modal-bread-edit #form-bread-edit-relation').addClass('hidden');

                item = $('#modal-bread-edit input[name=item]').val();
                type = $('#modal-bread-edit input[name=relation_type]').val();
                // alert(item);
                if (item){
                // LOAD CHILD
                    $('#modal-bread-edit #form-bread-edit-relation').removeClass('hidden');                    
                    // Ajax get read data
                    getEditChildData( type, item );                    

                }else {
                // LOAD MAIN
                    $('#modal-bread-edit #form-bread-edit').removeClass('hidden');
                    // Set Title 
                    setEditModalTitle( "Edit", "{{ $breads->settings['item'] }}" ); 
                    readDB( "#form-bread-edit", "{{ $breads->settings['route_read'] }}" );
                    ajax_validate("#btn-bread-add", "{{ $breads->settings['route_add'] }}" );
                }
               
            }


             function getEditChildData( type, item ) {
                id = $('#modal-bread-edit input[name="id"]').val();
                method = $('#modal-bread-edit input[name=method]').val();

                // Condition: Relation Pivot - No edit in relation pivot child
                
                // Condition: Relation One
                if (type == 'relation_one') {
                    uc_item = item.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                        return letter.toUpperCase();
                    });
                    $('#modal-bread-edit .navbar-title span').html("Add "+uc_item);
                }

                // Condition: Relation Many
                if (type == 'relation_many') {
                    // Set Title 
                    setEditModalTitle( "Edit", item );

                    child_id = $('#modal-bread-edit input[name="child_id"]').val();

                    // if have controller
                    route_child_read = $('#modal-bread-edit input[name="route_child_read"]').val();
                    if ( route_child_read != "" ) {
                        
                        runAjaxChildEditAccess( route_child_read , 'as-child-input-data', method, { 'child_id' : child_id });
                    }else{
                        // if no controller
                        runAjaxChildEditAccess( "{{ $breads->settings['route_read'] }}", 'my-child-input-data', method , { 'child_id' : child_id });
                    }


                }
            }

            function runAjaxChildEditAccess(target_route , access , method="", add_vars="" ) {
                
                data= {
                        "access" : access,
                        "method" : method
                };

                $.each( add_vars , function(key, value) {
                    data[key] = value;
                });

                $.ajax({
                    type: "GET",
                    url: target_route,
                    data: data,
                    dataType: "json",
                    success: function(response) {
                        // alert (JSON.stringify(response));
                        // call print setlist function
                        responseAjaxChildEditAccess(access, response);

                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        // Not yet : read again for 3 times
                        alert(xhr.status);
                    }
                });
            }

            function responseAjaxChildEditAccess(access, response) {
                // response for ajax
                switch(access) {
                    case "as-child-input-data":
                        $('#modal-bread-edit .relation-input').html(response.html);
                        fillReadData( '#form-bread-edit-relation' , response.data );
                        ajax_validate("#btn-bread-edit-relation", response.route_edit );
                        break;
                    case "my-child-input-data":
                        $('#modal-bread-edit .relation-input').html(response.html);
                        fillReadData( '#form-bread-edit-relation' , response.data );                        
                        ajax_validate("#btn-bread-edit-relation", "{{ $breads->settings['route_edit'] }}", {'access':'edit-my-child'} );
                        break;
                    default:                        
                }
            }

            function readDB(form_id, target_route) {
                // Get id 
                var id = $('#modal-bread-edit input[name=id]').val();              

                $.ajax({
                    type: "GET",
                    url: target_route+'/'+id,
                    dataType: "json",
                    success: function(response) {
                        //fill in form input
                        // alert(JSON.stringify(response));
                        fillReadData(form_id, response );

                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                    }
                });
            }

            function fillReadData(form_id, response ) {
                $('#modal-bread-edit '+ form_id +' input').each(function(index) {
                    if(typeof response[$(this).attr('name')] !== 'undefined') {
                        $(this).val(response[$(this).attr('name')]);
                    }
                });

                $('#modal-bread-edit '+ form_id +' textarea').each(function(index) {
                    if(typeof response[$(this).attr('name')] !== 'undefined') {
                        $(this).val(response[$(this).attr('name')]);
                    }
                });

                $('#modal-bread-edit '+ form_id +' select').each(function(index) {
                    if(typeof response[$(this).attr('name')] !== 'undefined') {
                        $(this).val(response[$(this).attr('name')]);
                    }
                });

                $('#modal-bread-edit '+ form_id +' .selectpicker').each(function(index) {
                    name = $(this).attr('name').replace('[]','');
                    if(typeof response[name+'_val'] !== 'undefined') {
                        $(this).selectpicker('val', response[name+'_val']);
                    }
                });
            }
            
            ajax_validate("#btn-bread-edit", "{{ $breads->settings['route_edit'] }}" );

            function setEditModalTitle( action_string, item ){
                uc_item = item.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                    return letter.toUpperCase();
                });
                $('#modal-bread-edit .navbar-title span').html( action_string +" "+ uc_item);
            }
        </script>
    @endslot
@endcomponent