@component('WCView::general.components.modals.extend.center') 
    @slot('modal_id', 'modal-bread-delete')
    @slot('modal_content_class', 'master-admin')
    @slot('modal_header')
        @component('WCView::general.components.navbars.extend.modal.primary')
            @slot('title')
                Delete {{ $breads->settings['item'] }}
            @endslot
            @slot('left_action','dismiss')
            @slot('left_icon','fa-times')
        @endcomponent
    @endslot
    @slot('modal_body')
        <div class="row padding">
            <div class="col-md-8 col-md-offset-2">
                <form class="form-horizontal" role="form" method="POST" action="{{ $breads->settings['route_delete'] }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="id"/>
                    <input type="hidden" name="name"/>
                    <input type="hidden" name="item">
                    <input type="hidden" name="method"/>
                    <input type="hidden" name="relation_type"/>
                    <input type="hidden" name="child_id"/>

                    <div class="delete-msg">
                    </div>

                    <div>
                    <button type="submit" class="btn btn-danger">Confirm</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>                
            </div>   
        </div>
    @endslot
    @slot('modal_footer')
    @endslot
    @slot('modal_script')
        <script type="text/javascript">
            $('#modal-bread-delete').on('show.bs.modal', function(e) {
                verifyDeleteParent();
                // $('#modal-bread-delete .target').html($('#modal-bread-delete input[name=name]').val());

            })

            function verifyDeleteParent() {
                type = $('#modal-bread-delete input[name=relation_type]').val();
                item = $('#modal-bread-delete input[name=item]').val();
                child_id = $('#modal-bread-delete input[name=child_id]').val();
                name =  $('#modal-bread-delete input[name=name]').val();
                show_name = ( name != 'undefined' && name!='' )? name:child_id;

                delete_msg = "";
                if (type == 'relation_pivot') {
                    setDeleteModalTitle( 'Unset' , item );
                    delete_msg = "Are you sure you want to unset "
                                + "<span class='text-b'>"
                                + item +":"+ show_name
                                + "</span>"
                                +" from "
                                + "<span class='text-b'>{{ $breads->settings['item'] }}</span>";

                }else if (type == 'relation_many'){
                    setDeleteModalTitle( 'Delete' , item );
                    delete_msg = "Are you sure you want to Delete "
                                + "<span class='text-b'>"
                                + item +":"+ show_name
                                + "</span>"
                                +" from "
                                + "<span class='text-b'>{{ $breads->settings['item'] }}</span>";

                }else {
                    id = $('#modal-bread-delete input[name=id]').val();
                    show_name = ( name != 'undefined' && name!='' )? name:id;
                    setDeleteModalTitle( 'Delete' , "{{ $breads->settings['item'] }}" );
                    delete_msg = "Are you sure you want to Delete <span class='text-b'>"+ show_name +"</span>";

                }
                $('#modal-bread-delete .delete-msg').html(delete_msg);
            }

            function setDeleteModalTitle( action_string, item ){
                uc_item = item.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                    return letter.toUpperCase();
                });
                $('#modal-bread-delete .navbar-title span').html( action_string +" "+ uc_item);
            }
        </script>
    @endslot
@endcomponent