{{-- 
<!-- MUst have BREAD DATA -->
1. Load column to show
--}}

@if ( $load_page == 'browse' )
    @include('BreadView::browse')
@endif


@if ( $load_page == 'read')
    @include('BreadView::read')
@endif
