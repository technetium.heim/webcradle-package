{{-- 
<!-- MUst have BREAD DATA -->
1. Load column to show
--}}


<div class="row">
    @component('WCView::general.components.panels.main')
    @slot('title')
        {{--<!-- Header -->--}}
        <div class="row text-2x text-b">
            {{ $breads->settings['item'] }}: 

            @if ( isset($breads->data->name) ) 
                {{ $breads->data->name }}
            @else
                {{ $breads->data->id }}
            @endif

            <div class="pull-right">
                <a class="btn btn-link" href="{{ $breads->settings['route_index'] }}">Back</a> 
            </div>   
        </div>
    @endslot
    @slot('content')
        {{--<!-- Main Item Data List -->--}}
        <input type="hidden" name="read_id" value="{{ $breads->data['id'] }}" >
        @foreach( $breads->cols as $col)
            <div class="row" style="min-height: 30px;">
                <div class="col-xs-5 col-md-3">
                    {{ ucwords($col) }}
                </div>
                <div class="col-xs-7 col-md-9">
                    {{ $breads->data[$col] }}
                </div>
            </div>
        @endforeach

        @if ($breads->relations != null)
            @foreach( $breads->relations as $key => $relation)
                @if( $relation->type == 'relation_one')
                    <hr>
                    <span class="text-lg"> {{ ucwords($relation->item) }} </span>
                    
                    @if( isset($relation->table) )
                        @foreach ($relation->cols as $col ) 
                            <div class="row" style="min-height: 30px;"> 
                                <div class="col-xs-5 col-md-3">
                                    {{ ucwords($col) }}
                                </div>
                                <div class="col-xs-7 col-md-9">
                                    {{ $relation->table->$col }}
                                </div>
                            </div>                           
                        @endforeach
                    @endif
                @endif
            @endforeach
        @endif
        <hr>
        @if($breads->settings['action_edit'] == true)
        <a class="btn btn-primary action-edit" data-target="#modal-bread-edit" data-id="{{ $breads->data['id'] }}">Edit</a>
        @endif
    @endslot
    @endcomponent

    @if ($breads->relations != null)
        @foreach( $breads->relations as $key => $relation)
            @if( $relation->type != 'relation_one')
                <div class="relation-table-container"> 
                @component('WCView::general.components.panels.main')
                    @slot('title')
                        <div class="row text-lg text-b">
                            @if ( $relation->route_index != "")
                            <a class="btn" href="{{ $relation->route_index }}"><span class="fa fa-fw fa-external-link"></span></a>
                            @endif

                            Related Table: {{ ucwords($relation->item) }} 

                            @if($relation->type == 'relation_pivot')
                                <div class="pull-right">
                                    <a class="btn btn-primary action-set" data-target="#modal-bread-add">Set {{ ucwords($relation->item) }}</a>
                                </div>
                            @elseif ($relation->type == 'relation_many') 
                                <div class="pull-right">
                                    <a class="btn btn-primary action-add" data-target="#modal-bread-add">New {{ ucwords($relation->item) }}</a>
                                </div>
                            @endif
                        </div>
                    @endslot
                    @slot('content')
                         <table id="table-{{ $relation->item }}" class="table table-condensed table-hover table-striped relation-table" >
                            <input type="hidden" name="item" value="{{ $relation->item }}" >
                            <input type="hidden" name="method" value="{{ $relation->method }}" >
                            <input type="hidden" name="relation_type" value="{{ $relation->type }}" >
                            <input type="hidden" name="route_child_browse" value="{{ $relation->route_browse }}">
                            <input type="hidden" name="route_child_read" value="{{ $relation->route_read }}">
                            <input type="hidden" name="id_list" value="{{ $relation->id_list }}">
                            <input type="hidden" name="read_relation" value="{{ $relation->read_relation }}">

                            <thead>
                                <tr>
                                @if( isset($relation->table) )
                                    @foreach ($relation->cols as $col ) 
                                        <th>{{ ucwords($col) }}</th>                              
                                    @endforeach
                                    <th></th>
                                @endif
                                </tr>
                            </thead>
                            <tbody>
                                @if( isset($relation->table) )
                                    @if( $relation->table->isEmpty() )
                                        <tr><td colspan="0">No Result.</td></tr>
                                    @else
                                        @foreach ($relation->table as $relation_rows)
                                            <tr>
                                            @foreach($relation->cols as $col)
                                                <td>{{ $relation_rows->$col }}</td>
                                            @endforeach
                                                <td class='action-column' data-id='{{ $relation_rows["id"] }}'
                                                    @if (isset($relation_rows["name"]))
                                                        data-name='{{ $relation_rows["name"] }}'
                                                    @endif
                                                >
                                          
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @endif
                            </tbody>
                        </table>
                    @endslot
                @endcomponent
                </div>
            @endif
        @endforeach
    @endif
</div>

@push('addmodals')
    @include('BreadView::modal_add')
    @include('BreadView::modal_edit')
    @include('BreadView::modal_delete')
@endpush


@push('addscripts')
    <script type="text/javascript"> 

        // read each relation
        $('.datepicker').datepicker({
            autoclose: true,
            clearBtn: true,
            maxViewMode: 'decade',
        }).on('changeDate', function(e) {
            var date = $(this).datepicker('getDate');
            date = moment(date).format('YYYY-DD-MM');
            $(this).parent().siblings('input').val(date);
        }).on('hide', function(e) {
            if ($(this).parent().siblings('input').val() == "Invalid date") {
                $(this).parent().siblings('input').val("");
            } 
            
        });


        $('.selectpicker').selectpicker({
          iconBase: 'fa',
          tickIcon: 'fa-check'
        });


        loadRelationTable();

        function loadRelationTable() {

            if( $('.relation-table').length == 0 ) initActionEvent();


            $('.relation-table').each(function() {             
                table_id = $(this).attr('id');
                route_child_browse = $(this).find('input[name="route_child_browse"]').val();
                relation_type = $(this).find('input[name="relation_type"]').val();
                item = $(this).find('input[name="item"]').val();

                // Condition: Not require to read from controller
                if( $(this).find('input[name="read_relation"]').val() == 0 ) {
                    printActionColumns( table_id , relation_type);
                    initActionEvent();
                    return;
                }               
                
                // Condition: Continue read from contoller
                data= {
                    "access" : 'child-table',
                    "child_id" : $(this).find('input[name="id_list"]').val().split(","),
                    "relation_type" : relation_type,
                    "table_id" : table_id
                };

                $.ajax({
                    type: "GET",
                    url: route_child_browse,
                    data: data,
                    dataType: "json",
                    success: function(response) {
                        // alert(JSON.stringify(response));
      
                        // call print table function
                        printChildTable( response.table_id , response.rows , response.cols, response.relation_type );
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        // Not yet : read again for 3 times
                        alert(xhr.status);
                    }
                }).then(initActionEvent);

            });
        }

        // FUNCTION: print child table for relation (once)
        function printChildTable( table_id , rows , cols, relation_type ) {

            // print table head output
            output_head ="";                        
            $.each(cols, function(key, value){     
                output_head += "<th>"+ value.charAt(0).toUpperCase() + value.substr(1) +"</th>";
            });
                //action row
                output_head += "<th></th>";
           $("#"+ table_id).find("thead tr").html(output_head);

            // print table body output
            output_body ="";
            if ( rows.length == 0 ){
                output_body = ' <tr><td colspan="0">No Result.</td></tr>'
            }else {
                $.each(rows, function(key, row){
                    output_body += "<tr>";
                    $.each(cols, function(i, col){
                        output_body += "<td>"+ outputFormat(row[col]) +"</td>";
                    });

                    // print action
                    output_body += formatActionColumn( relation_type , row['id'] , row['name'] );

                    output_body += "</tr>";
                });
            }
            $("#"+ table_id).find("tbody").html(output_body);       
        }

        // FUNCTION: Check data for relation table, if is collection array, return value for 'name' column 
        function outputFormat( data ){
            if (!$.isArray(data)) { return data; }
            
            var output = '<table class="table-condensed table-tight"><tbody>';

            $.each( data, function(key ,data_rows) {
                output += "<tr>";
                output += "<td class='text-xs'><i class='fa fa-circle'></i></td>";
                $.each( data_rows , function( data_key, data_value ) {                                    
                    output += "<td>"+ data_value +"</td>";
                });
                 output += "</tr>";
            });
            output += "</tbody></table>";
            return output; 
           

        }

        function initActionEvent(){
            if ($(".action-add").length > 0 ) {
                $(".action-add").off().on('click', function(e) {
                    $('#modal-bread-add input[name=id]').val($('input[name="read_id"]').val());
                    $('#modal-bread-add input[name=item]').val($(this).parents('.relation-table-container').find('input[name="item"]').val());
                    $('#modal-bread-add input[name=method]').val($(this).parents('.relation-table-container').find('input[name="method"]').val());
                    $('#modal-bread-add input[name=route_child_read]').val($(this).parents('.relation-table-container').find('input[name="route_child_read"]').val());
                    $('#modal-bread-add input[name=relation_type]').val($(this).parents('.relation-table-container').find('input[name="relation_type"]').val());
                    $($(this).attr("data-target")).modal("show");
                });
            }

            if ($(".action-edit").length > 0 ) {
                $(".action-edit").off().on('click', function(e) {
                    $('#modal-bread-edit input[name=id]').val($('input[name="read_id"]').val());
                    $('#modal-bread-edit input[name=child_id]').val($(this).data("child-id"));
                    $('#modal-bread-edit input[name=item]').val($(this).parents('.relation-table-container').find('input[name="item"]').val());
                    $('#modal-bread-edit input[name=method]').val($(this).parents('.relation-table-container').find('input[name="method"]').val());
                    $('#modal-bread-edit input[name=route_child_read]').val($(this).parents('.relation-table-container').find('input[name="route_child_read"]').val());
                    $('#modal-bread-edit input[name=relation_type]').val($(this).parents('.relation-table-container').find('input[name="relation_type"]').val());
                    $($(this).attr("data-target")).modal("show");
                });
            }

            if ($(".action-delete").length > 0 ) {
                $(".action-delete").off().on('click', function(e) {
                    $('#modal-bread-delete input[name=id]').val($('input[name="read_id"]').val());
                    $('#modal-bread-delete input[name=name]').val($(this).data("name"));
                    $('#modal-bread-delete input[name=child_id]').val($(this).data("child-id"));
                    $('#modal-bread-delete input[name=item]').val($(this).parents('.relation-table-container').find('input[name="item"]').val());
                    $('#modal-bread-delete input[name=method]').val($(this).parents('.relation-table-container').find('input[name="method"]').val());
                    $('#modal-bread-delete input[name=relation_type]').val($(this).parents('.relation-table-container').find('input[name="relation_type"]').val());
                    $($(this).attr("data-target")).modal("show");
                });
            }
            // Init Unset
            if ($(".action-unset").length > 0 ) {
                $(".action-unset").off().on('click', function(e) {
                    $('#modal-bread-delete input[name=id]').val($('input[name="read_id"]').val());
                    $('#modal-bread-delete input[name=name]').val($(this).data("name"));
                    $('#modal-bread-delete input[name=child_id]').val($(this).data("child-id"));
                    $('#modal-bread-delete input[name=item]').val($(this).parents('.relation-table-container').find('input[name="item"]').val());
                    $('#modal-bread-delete input[name=method]').val($(this).parents('.relation-table-container').find('input[name="method"]').val());
                    $('#modal-bread-delete input[name=relation_type]').val($(this).parents('.relation-table-container').find('input[name="relation_type"]').val());
                    $($(this).attr("data-target")).modal("show");
                });
            }

            // Init set
            if ($(".action-set").length > 0 ) {
                $(".action-set").off().on('click', function(e) {
                    $('#modal-bread-add input[name=id]').val($('input[name="read_id"]').val());
                    $('#modal-bread-add input[name=item]').val($(this).parents('.relation-table-container').find('input[name="item"]').val());
                    $('#modal-bread-add input[name=method]').val($(this).parents('.relation-table-container').find('input[name="method"]').val());
                    $('#modal-bread-add input[name=route_child_browse]').val($(this).parents('.relation-table-container').find('input[name="route_child_browse"]').val());
                    $('#modal-bread-add input[name=relation_type]').val($(this).parents('.relation-table-container').find('input[name="relation_type"]').val());
                    $($(this).attr("data-target")).modal("show");
                });
            }

        }

        function printActionColumns( table_id, relation_type){
            $('#'+ table_id +' .action-column').each( function(){
                output = formatActionColumn( relation_type , $(this).data("id") , $(this).data("name") );
                $(this).html(output);
            });        
        }

        function formatActionColumn( relation_type , id , name ){
            // For different type of relation
            // Format Action Column (single)
            output = "<td>";
            if(relation_type  == "relation_pivot") {
                // FORMAT pivot unset       
                output += "<a class='btn btn-sm action-unset' data-target='#modal-bread-delete' data-child-id='"+id+"' data-name='"+name+"'>"
                            + "unset</a>";
            }
            if (relation_type  == "relation_one")  {
                output += "<a class='btn btn-sm action-edit' data-target='#modal-bread-edit' data-id='"+id+"'>"
                            + "<span class='fa fa-fw fa-pencil'></span></a>";
  
                output += "<a class='btn btn-sm action-edit' data-target='#modal-bread-delete' data-id='"+id+"'>"
                            + "unset</a>";    
            }

            if (relation_type  == "relation_many")  {
                output += "<a class='btn btn-sm action-edit' data-target='#modal-bread-edit' data-child-id='"+id+"' >"
                            + "<span class='fa fa-fw fa-pencil'></span></a>";
                
                 output += "<a class='btn btn-sm action-delete' data-target='#modal-bread-delete' data-child-id='"+id+"' data-name='"+name+"'>"
                            + "<span class='fa fa-fw fa-trash'></span></a>";
                 
            }

            output += "</td>";

            return output;
        }

    </script>
@endpush