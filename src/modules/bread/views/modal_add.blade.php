@component('WCView::general.components.modals.extend.center') 
    @slot('modal_id', 'modal-bread-add')
    @slot('modal_content_class', 'master-admin')
    @slot('modal_header')
        @component('WCView::general.components.navbars.extend.modal.primary')
            @slot('title')
                Add New {{ $breads->settings['item'] }}
            @endslot
            @slot('left_action','dismiss')
            @slot('left_icon','fa-times')
        @endcomponent
    @endslot
    @slot('modal_body')
        <div class="row padding">
            <div class="col-md-8 col-md-offset-2">
                <form id="form-bread-add" class="form-horizontal" role="form">
                    {{ csrf_field() }}
                    <input type="hidden" name="id"/>
                    
                    @foreach ($breads->inputs as $input)     
                        @include('BreadView::input_formatter')
                    @endforeach

                    <div class="row pull-right">
                        <a id="btn-bread-add" class="btn btn-primary">Ok</a>
                        <a class="btn btn-primary" data-dismiss="modal">Cancel</a>
                    </div>
                </form>

                {{-- <!--relation input --> --}}
                <form id="form-bread-add-relation" class="form-horizontal" role="form" method="POST" >
                    {{ csrf_field() }}
                    <input type="hidden" name="item"/>
                    <input type="hidden" name="method"/>
                    <input type="hidden" name="relation_type"/>
                    <input type="hidden" name="route_child_read"/>

                    <div class="relation-input">

                    </div>

                    <div class="row pull-right">
                        <button id="btn-bread-add-relation" class="btn btn-primary">Ok</button>
                        <button class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>

            </div>
        </div>

    @endslot
    @slot('modal_footer')
    @endslot
    @slot('modal_script')
        <script type="text/javascript">
             $('#modal-bread-add').on('show.bs.modal', function(e) {
                verifyAddParent();    
            })
  
            function verifyAddParent() {
                // initialize form
                $('#modal-bread-add #form-bread-add').addClass('hidden');
                $('#modal-bread-add #form-bread-add-relation').addClass('hidden');

                item = $('#modal-bread-add input[name=item]').val();
                type = $('#modal-bread-add input[name=relation_type]').val();
                // alert(item);
                if (item){
                    $('#modal-bread-add #form-bread-add-relation').removeClass('hidden');
                    
                    // Ajax get read data
                    getAddChildData( type, item );                    

                }else {

                    $('#modal-bread-add #form-bread-add').removeClass('hidden');
                    // Set Title 
                    $('#modal-bread-add .navbar-title span').html("Add New {{ $breads->settings['item'] }}");
                    ajax_validate("#btn-bread-add", "{{ $breads->settings['route_add'] }}" );
                }
               
            }

            function getAddChildData( type, item ) {
                id = $('#modal-bread-add input[name="id"]').val();
                method = $('#modal-bread-add input[name=method]').val();

                // Condition: Relation Pivot
                if (type == 'relation_pivot') {
                    // Set Title 
                    setAddModalTitle( "Set", item );
                    
                    // get input setlist from child controller
                    runAjaxChildAddAccess( "{{ $breads->settings['route_read'] }}" , 'pivot-setlist', method, {'parent_id' : id } );

                    // Prepare onclick submit event
                    $('#btn-bread-add-relation').on('click', function(event) {
                        event.preventDefault();
                        $('#form-bread-add-relation').attr("action", "{{ $breads->settings['route_add'] }}" );
                        $('<input />').attr('type', 'hidden')
                                  .attr('name', "access")
                                  .attr('value', "set-pivot")
                                  .appendTo('#form-bread-add-relation');
                         $('<input />').attr('type', 'hidden')
                                  .attr('name', "parent_id")
                                  .attr('value', id)
                                  .appendTo('#form-bread-add-relation');
                        $('#form-bread-add-relation').submit();
                    });      
                }

                // Condition: Relation One
                if (type == 'relation_one') {
                    uc_item = item.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                        return letter.toUpperCase();
                    });
                    $('#modal-bread-add .navbar-title span').html("Add "+uc_item);
                }

                // Condition: Relation Many
                if (type == 'relation_many') {
                    // Set Title 
                    setAddModalTitle( "Add", item );

                    // if have controller
                    route_child_read = $('#modal-bread-add input[name="route_child_read"]').val();
                    if ( route_child_read != "" ) {
                        runAjaxChildAddAccess( route_child_read , 'as-child-input', method, {'parent_id': id});
                        
                    }else{
                        // if no controller
                        runAjaxChildAddAccess( "{{ $breads->settings['route_read'] }}" , 'my-child-input', method, {'parent_id': id} );
                    }
                }
            }

            function runAjaxChildAddAccess(target_route , access , method="", add_vars="" ) {
                
                data= {
                        "access" : access,
                        "method" : method
                };

                $.each( add_vars , function(key, value) {
                    data[key] = value;
                });

                $.ajax({
                    type: "GET",
                    url: target_route,
                    data: data,
                    dataType: "json",
                    success: function(response) {
                        // alert (JSON.stringify(response));
                        // call print setlist function
                        responseAjaxChildAddAccess(access, response);

                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        // Not yet : read again for 3 times
                        alert(xhr.status);
                    }
                });
            }

            function responseAjaxChildAddAccess(access, response) {
                // response for ajax
                switch(access) {
                    case 'pivot-setlist':
                        printPivotSetList(response);
                        break;
                    case "as-child-input":
                        $('#modal-bread-add .relation-input').html(response.html);
                        $('#modal-bread-add .relation-input').find('input[name="'+ response.foreign +'"]').val(response.parent_id);
                        $('#modal-bread-add .relation-input').find('select[name="'+ response.foreign +'"]').val(response.parent_id);
                        ajax_validate("#btn-bread-add-relation", response.route_add );
                        break;
                    case "my-child-input":
                        $('#modal-bread-add .relation-input').html(response.html);
                        $('#modal-bread-add .relation-input').find('input[name="'+ response.foreign +'"]').val(response.parent_id);
                        ajax_validate("#btn-bread-add-relation", "{{ $breads->settings['route_add'] }}", {'access':'add-my-child'} );
                        break;
                    default:                        
                }
            }

            // relation pivot function-> Pivot print setlist
            function printPivotSetList(data) {
                output = "";
                $.each(data.setlist, function ( index , rows ) {
                    checked = ($.inArray(rows.id, data.selected) > -1 )? 'checked': '';
                    output += "<div class='form-check'>";
                    output += "<label><input type='checkbox' name='child_id[]' value='"+ rows.id +"' "+ checked +"> "+ rows.name+"</label>";
                    output += "</div>";
                });

                item = $('#modal-bread-add input[name=item]').val();
                if( data.setlist.length == 0 ) output = 'No Data. Please add new '+ item +'.';

                $('#modal-bread-add .relation-input').html(output);
            }

            function setAddModalTitle( action_string, item ){
                uc_item = item.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                    return letter.toUpperCase();
                });
                $('#modal-bread-add .navbar-title span').html( action_string +" "+ uc_item);
            }

        </script>
    @endslot
@endcomponent