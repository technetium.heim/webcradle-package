<div class="row">   
    @component('WCView::general.components.panels.main')
    @slot('title')
        {{--<!-- Header -->--}}
        <div class="row text-2x text-b">
            {{ $breads->settings['title'] }}
            @if ( $breads->settings['action_add'] == "true")
                <div class="pull-right"><button type="button" class="btn btn-default btn-primary" data-toggle="modal" data-target="#modal-bread-add">New {{  $breads->settings['item']  }}</button></div>
            @endif
        </div>
    @endslot
    @slot('content')
        {{--<!-- Table -->--}}
         <table id="bootgrid" class="table table-condensed table-hover table-striped">
            <thead>
                <tr>
                    @foreach ($breads->cols as $column) 
                        @if ($column == 'id')
                            <th data-column-id="id" data-type="numeric" data-order="asc">ID</th>
                        @else
                            <th data-column-id="{{ $column }}" data-formatter="text">{{ ucwords($column) }}</th>
                        @endif
                    @endforeach
                    <th data-column-id="actions" data-formatter="actions" data-sortable="false"></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    @endslot
    @endcomponent     
</div>

@push('addmodals')
    @include('BreadView::modal_add')
    @include('BreadView::modal_edit')
    @include('BreadView::modal_delete') 
@endpush


@push('addscripts')
    <script type="text/javascript">
        $(document).ready(function() {
            var bootgrid = $("#bootgrid").bootgrid({
                ajax: true,
                url: "{!! $breads->settings['route_browse'] !!}",
                ajaxSettings: {
                    method: "GET",
                },
                caseSensitive: false,
                columnSelection: false,
                // responseHandler: function (response) { alert(JSON.stringify(response)); return response; },
                formatters: {
                    'text': function(column,row) {
                        if ($.isArray(row[column.id])) {
                            var output = '<table class="table-condensed table-tight"><tbody>';
                            data = row[column.id];
                            $.each( data, function(key ,data_rows) {
                                output += "<tr>";
                                output += "<td class='text-xs'><i class='fa fa-circle'></i></td>";
                                $.each( data_rows , function( data_key, data_value ) {                                    
                                    output += "<td>"+ data_value +"</td>";
                                });
                                 output += "</tr>";
                            });
                            output += "</tbody></table>";
                            return output; 
                        }
                        return row[column.id];
                    },
                    'actions':function(column,row) {
                        var button="";
                        // alert(JSON.stringify(row));
                        @if($breads->settings['action_edit'] == true)
                        button += '<a class="btn btn-sm action-edit" data-toggle="modal" data-target="#modal-bread-edit" data-id="'+row.id+'"><span class="fa fa-fw fa-pencil"></span></a>'
                        @endif

                        @if($breads->settings['action_delete'] == true)
                        button += '<a class="btn btn-sm action-delete" data-toggle="modal" data-target="#modal-bread-delete" data-id="'+row.id+'"  data-name="'+row.name+'"><span class="fa fa-fw fa-trash-o"></span></a>'
                        @endif

                        @if($breads->settings['action_read'] == true)
                        button += '<a class="btn btn-sm" href="'+row.read+'"><span class="fa fa-fw fa-external-link"></span></a>'
                        @endif
                        return button;
                    }
                },
                css: {
                    header: "bootgrid-header",
                    footer: "full-height"
                }
            }).on("loaded.rs.jquery.bootgrid", function() {
                bootgrid.find(".action-edit").click(function(e) {
                    $('#modal-bread-edit input[name=id]').val($(this).data("id"));
                    $($(this).attr("data-target")).modal("show");
                })
                .end().find(".action-delete").click(function(e) {
                    $('#modal-bread-delete input[name=id]').val($(this).data("id"));
                    $('#modal-bread-delete input[name=name]').val($(this).data("name"));
                    $($(this).attr("data-target")).modal("show");
                });
            });
        });
        

        $('.datepicker').datepicker({
            autoclose: true,
            clearBtn: true,
            maxViewMode: 'decade',
        }).on('changeDate', function(e) {
            var date = $(this).datepicker('getDate');
            date = moment(date).format('YYYY-DD-MM');
            $(this).parent().siblings('input').val(date);
        }).on('hide', function(e) {
            if ($(this).parent().siblings('input').val() == "Invalid date") {
                $(this).parent().siblings('input').val("");
            } 
            
        });

        $('.selectpicker').selectpicker({
          iconBase: 'FontAwesome',
          tickIcon: 'fa fa-check',
        });

    </script>
@endpush