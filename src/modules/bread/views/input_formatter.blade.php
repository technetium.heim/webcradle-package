{{--
	<!-- 
	List of Input type:
		- text (default)
		- dropdown => add parameter options(value)
		- textarea
		- date
		- birthday

	List of Control Parameter:
		- locked
		- required 
	
	Accept Data format:

	$input = (object)[
        'name' => 'name',    				// name , label for input tag (string)
        'type' => 'text',					// type for input tag (string)
        'properties' => ['required'],		// additional parameter. (array[string])
        'options' => []						// optional: for dropdown <option> (array[string])
     ];

	-->
--}}

@php
	$locked = ( in_array( 'locked', $input->properties ) )? 'readonly': '' ;
	$required = ( in_array( 'required', $input->properties ) )? 'required': '' ;
	if ($locked == 'readonly' && $input->type == 'dropdown') $locked = 'disabled';
@endphp

{{--  <!-- Input Label Format  --> --}}
<div class="form-group">
<label class="col-xs-12 col-sm-3 text-left"> 
    {{ ucwords($input->name) }} 
	@if ( in_array( 'required', $input->properties ) )
	 *
	@endif
</label>

{{--  <!-- Input Box Format  --> --}}
<div class="col-xs-12 col-sm-9"> 
	@if ( $input->type == 'textarea' )
	    <textarea class="form-control" name="{{ $input->name }}" rows="4" autocomplete="off" {{ $required }} {{ $locked }}></textarea>
	@elseif ( $input->type == 'dropdown' )
		<select class="form-control" name="{{ $input->name }}" {{ $required }} {{ $locked }}>
			<option value="" readonly>Select your option</option>
			@foreach( $input->options as $option)
		  		<option value="{{ $option->value }}">{{ $option->name }}</option>	
		  	@endforeach
		</select>
		@if( $locked == 'disabled') <input type="hidden" name="{{ $input->name }}"> @endif
	@elseif ( $input->type == 'date' )
		<div class="input-group date">
		    <div class="input-group-addon bg-white">
		       	<i class="fa fa-calendar fa-lg datepicker" aria-hidden="true"></i>
		    </div>
		    <input type="text" class="form-control" name="{{ $input->name }}" readonly>
		</div>
	@elseif ( $input->type == 'birthday' )
		<div class="input-group date">
		    <div class="input-group-addon bg-white">
		       	<i class="fa fa-calendar fa-lg datepicker" aria-hidden="true" data-date-end-date="-10y" data-date-start-view="years"></i>
		    </div>
		    <input type="text" class="form-control" name="{{ $input->name }}" readonly >
		</div>
	@elseif ( $input->type == 'multiple' )
		<select class="selectpicker form-control" name="{{ $input->name }}[]" multiple {{ $required }} {{ $locked }}>
		  	@foreach( $input->options as $option)
		  		<option value="{{ $option->value }}">{{ $option->name }}</option>	
		  	@endforeach
		</select>
	@elseif ( $input->type == 'toggle' )
		<input type="checkbox" name="{{ $input->name }}" checked data-toggle="toggle" data-size="small" {{ $required }} {{ $locked }}>
	@elseif ( $input->type == 'boolean' )
		<select class="form-control" name="{{ $input->name }}" {{ $required }} {{ $locked }}>
		  	<option value="0">0</option>
		  	<option value="1">1</option>	
		</select>
	@else
	    <input class="form-control" type="{{ $input->type }}" name="{{ $input->name }}" autocomplete="off" {{ $required }} {{ $locked }}>
	@endif
</div>

</div>
	
