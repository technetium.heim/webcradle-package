<?php

use Illuminate\Database\Seeder;

class DatasetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('datasets')->delete();
        DB::table('datasets')->insert(
            [
                ['id'=>'1', 'value'=>'1', 'icon'=>'', 'key'=>'gender' ,'sort' => '1'],
                ['id'=>'2', 'value'=>'2', 'icon'=>'', 'key'=>'gender' ,'sort' => '2'],
                ['id'=>'3', 'value'=>'1', 'icon'=>'', 'key'=>'dayofweek' ,'sort' => '1'],
                ['id'=>'4', 'value'=>'2', 'icon'=>'', 'key'=>'dayofweek' ,'sort' => '2'],
                ['id'=>'5', 'value'=>'3', 'icon'=>'', 'key'=>'dayofweek' ,'sort' => '3'],
                ['id'=>'6', 'value'=>'4', 'icon'=>'', 'key'=>'dayofweek' ,'sort' => '4'],
                ['id'=>'7', 'value'=>'5', 'icon'=>'', 'key'=>'dayofweek' ,'sort' => '5'],
                ['id'=>'8', 'value'=>'6', 'icon'=>'', 'key'=>'dayofweek' ,'sort' => '6'],
                ['id'=>'9', 'value'=>'7', 'icon'=>'', 'key'=>'dayofweek' ,'sort' => '7'],
                ['id'=>'10', 'value'=>'1', 'icon'=>'', 'key'=>'month' ,'sort' => '1'],
                ['id'=>'11', 'value'=>'2', 'icon'=>'', 'key'=>'month' ,'sort' => '2'],
                ['id'=>'12', 'value'=>'3', 'icon'=>'', 'key'=>'month' ,'sort' => '3'],
                ['id'=>'13', 'value'=>'4', 'icon'=>'', 'key'=>'month' ,'sort' => '4'],
                ['id'=>'14', 'value'=>'5', 'icon'=>'', 'key'=>'month' ,'sort' => '5'],
                ['id'=>'15', 'value'=>'6', 'icon'=>'', 'key'=>'month' ,'sort' => '6'],
                ['id'=>'16', 'value'=>'7', 'icon'=>'', 'key'=>'month' ,'sort' => '7'],
                ['id'=>'17', 'value'=>'8', 'icon'=>'', 'key'=>'month' ,'sort' => '8'],
                ['id'=>'18', 'value'=>'9', 'icon'=>'', 'key'=>'month' ,'sort' => '9'],
                ['id'=>'19', 'value'=>'10', 'icon'=>'', 'key'=>'month' ,'sort' => '10'],
                ['id'=>'20', 'value'=>'11', 'icon'=>'', 'key'=>'month' ,'sort' => '11'],
                ['id'=>'21', 'value'=>'12', 'icon'=>'', 'key'=>'month' ,'sort' => '12'],
            ]
        );

        DB::table('dataset_descriptions')->delete();
        DB::table('dataset_descriptions')->insert(
            [
                ['id'=>'1', 'dataset_id'=>'1', 'language_id'=>'1', 'name'=>'Male'],
                ['id'=>'2', 'dataset_id'=>'2', 'language_id'=>'1', 'name'=>'Female'],
                ['id'=>'3', 'dataset_id'=>'3', 'language_id'=>'1', 'name'=>'Monday'],
                ['id'=>'4', 'dataset_id'=>'4', 'language_id'=>'1', 'name'=>'Tuesday'],
                ['id'=>'5', 'dataset_id'=>'5', 'language_id'=>'1', 'name'=>'Wednesday'],
                ['id'=>'6', 'dataset_id'=>'6', 'language_id'=>'1', 'name'=>'Thursday'],
                ['id'=>'7', 'dataset_id'=>'7', 'language_id'=>'1', 'name'=>'Friday'],
                ['id'=>'8', 'dataset_id'=>'8', 'language_id'=>'1', 'name'=>'Saturday'],
                ['id'=>'9', 'dataset_id'=>'9', 'language_id'=>'1', 'name'=>'Sunday'],
                ['id'=>'10', 'dataset_id'=>'10', 'language_id'=>'1', 'name'=>'Jan'],
                ['id'=>'11', 'dataset_id'=>'11', 'language_id'=>'1', 'name'=>'Feb'],
                ['id'=>'12', 'dataset_id'=>'12', 'language_id'=>'1', 'name'=>'Mar'],
                ['id'=>'13', 'dataset_id'=>'13', 'language_id'=>'1', 'name'=>'Apr'],
                ['id'=>'14', 'dataset_id'=>'14', 'language_id'=>'1', 'name'=>'May'],
                ['id'=>'15', 'dataset_id'=>'15', 'language_id'=>'1', 'name'=>'Jun'],
                ['id'=>'16', 'dataset_id'=>'16', 'language_id'=>'1', 'name'=>'Jul'],
                ['id'=>'17', 'dataset_id'=>'17', 'language_id'=>'1', 'name'=>'Aug'],
                ['id'=>'18', 'dataset_id'=>'18', 'language_id'=>'1', 'name'=>'Sept'],
                ['id'=>'19', 'dataset_id'=>'19', 'language_id'=>'1', 'name'=>'Oct'],
                ['id'=>'20', 'dataset_id'=>'20', 'language_id'=>'1', 'name'=>'Nov'],
                ['id'=>'21', 'dataset_id'=>'21', 'language_id'=>'1', 'name'=>'Dec'],                
            ]
        );
    }
}
