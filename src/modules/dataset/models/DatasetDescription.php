<?php
namespace Cradle\modules\dataset\models;

use App\Models\AppCradleModel;

class DatasetDescription extends AppCradleModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'language_id', 'name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function dataset()
    {
        return $this->belongsTo('Cradle\modules\dataset\models\Dataset');
    }
}
