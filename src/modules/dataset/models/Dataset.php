<?php

namespace Cradle\modules\dataset\models;

use App\Models\AppCradleModel;

class Dataset extends AppCradleModel
{
    protected $cascadeDeletes = ['descriptions'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value', 'icon', 'sort' , 'key'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    public function descriptions()
    {   
        return $this->hasMany('Cradle\modules\dataset\models\DatasetDescription');
    }

    public function getName()
    {
        return \App\DatasetDescription::where('dataset_id', $this->id )->first()->name;
    }
}
