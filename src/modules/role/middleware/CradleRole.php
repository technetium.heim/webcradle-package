<?php 
namespace Cradle\modules\role\middleware;

/**
 * This file is part of Entrust,
 * a role & permission management solution for Laravel.
 *
 * @license MIT
 * @package Zizaco\Entrust
 */

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Cradle;
class CradleRole
{
	protected $auth;

	/**
	 * Creates a new instance of the middleware.
	 *
	 * @param Guard $auth
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  Closure $next
	 * @param  $roles
	 * @return mixed
	 */
	public function handle($request, Closure $next, $roles)
	{
		if ($this->auth->guest() || !$request->user()->hasRole(explode('|', $roles))) {
			if($roles == 'admin') {
				
				return redirect()->route(Cradle::config('project.redirect.admin_exception'));
			}
			return response()->view('general.errors.403');
			abort(403);
		}

		return $next($request);
	}
}
