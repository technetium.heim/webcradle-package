<?php 
namespace Cradle\modules\role\models;

/**
 * This file is part of Entrust,
 * a role & permission management solution for Laravel.
 *
 * @license MIT
 * @package Zizaco\Entrust
 */

use Cradle\modules\role\contracts\PermissionInterface;
use Cradle\modules\role\foundation\PermissionTrait;
use App\Models\AppCradleModel;
use Illuminate\Support\Facades\Config;


class Permission extends AppCradleModel implements PermissionInterface
{
    use PermissionTrait;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    protected $fillable = [
        'name', 'display_name', 'description',
    ];

    protected $hidden = ['pivot'];
    
    /**
     * Creates a new instance of the model.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = Config::get('role.permissions_table');
    }

}
