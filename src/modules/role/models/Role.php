<?php 
namespace Cradle\modules\role\models;

/**
 * This file is part of Entrust,
 * a role & permission management solution for Laravel.
 *
 * @license MIT
 * @package Zizaco\Entrust
 */

use Cradle\modules\role\contracts\RoleInterface;
use Cradle\modules\role\foundation\RoleTrait;
use App\Models\AppCradleModel;
use Illuminate\Support\Facades\Config;

class Role extends AppCradleModel implements RoleInterface
{
    use RoleTrait;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    protected $fillable = [
        'name', 'display_name', 'description',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['pivot'];

    /**
     * Creates a new instance of the model.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = Config::get('role.roles_table');
    }

}
