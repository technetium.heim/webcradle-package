<?php

namespace Cradle\modules;

use Illuminate\Support\ServiceProvider;
use Cradle\basic\supports\CradleServiceProviderTrait;

class ModuleServiceProvider extends ServiceProvider
{
    use CradleServiceProviderTrait;
    // protected $defer = true;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {   
        $this->publishModule();
        $this->loadViewsFrom(__DIR__.'/bread/views', 'BreadView'); 
        $this->loadViewsFrom(__DIR__.'/fform/views', 'FormView');  
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerAlias();       
        // Role
        $this->app['router']->aliasMiddleware('role', \Cradle\modules\role\middleware\CradleRole::class);
        $this->app['router']->aliasMiddleware('permission', \Cradle\modules\role\middleware\CradlePermission::class);
        $this->app['router']->aliasMiddleware('ability', \Cradle\modules\role\middleware\CradleAbility::class);

        // $this->app['router']->aliasMiddleware('setlocale', \Cradle\modules\translation\middleware\SetLocale::class);

    }

    public function publishModule() 
    {   
        //load migrations
        $this->loadMigrationsFrom(__DIR__.'/dataset/migrations');
        $this->loadMigrationsFrom(__DIR__.'/role/migrations');
        $this->loadMigrationsFrom(__DIR__.'/menu/migrations');
        $this->loadMigrationsFrom(__DIR__.'/settings/migrations');
        $this->loadMigrationsFrom(__DIR__.'/dashboard/migrations');

        //config File for all modules
        $this->publishes([
        __DIR__.'/role/config' => base_path('/config'),
        __DIR__.'/translation/config' => base_path('/config'),
        ],"module-once");

        // Dataset
        $this->publishes([
        __DIR__.'/dataset/seeds' => database_path('/seeds'),
        __DIR__.'/menu/seeds' => database_path('/seeds'),
        ],"module-update");

        // // Socialite
        // $this->publishes([
        // __DIR__.'/socialite/migrations' => database_path('/migrations'),
        // __DIR__.'/socialite/providers' => app_path('/Providers'),
        // __DIR__.'/socialite/models' => app_path('/'),
        // __DIR__.'/socialite/views' => resource_path('/views/general/modules/socialite'),
        // __DIR__.'/socialite/controllers' => app_path('/Http/Controllers/user/socialite'),
        // ],"add-socialite");

        $this->pushGroupsList('modules', ['module-once','module-update'] );
    }


    protected function registerAlias()
    {
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();

        // // Socialite
        // if (config('cradleConfig.socialite_fb') == 'enable') {
        //     $this->app->register(
        //        'Laravel\Socialite\SocialiteServiceProvider'
        //     );        
        //     $loader->alias('Socialite', 'Laravel\Socialite\Facades\Socialite');

        //     // Socialite
        //     $this->mergeConfigFrom(__DIR__.'/socialite/config/socialiteServices.php', 'services');
        // }

        // bind this
        $this->app->bind('fast-form',function() {
            return new fform\FForm();
        });

        $loader->alias('FForm', \Cradle\modules\fform\facades\FForm::class);
    }
}
