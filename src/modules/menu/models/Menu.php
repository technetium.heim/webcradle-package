<?php

namespace Cradle\modules\menu\models;


use App\Models\AppCradleModel;

class Menu extends AppCradleModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    
    public function item()
    {   
        return $this->hasMany('Cradle\modules\menu\models\MenuItem');
    }
}