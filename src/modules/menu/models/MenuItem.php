<?php
namespace Cradle\modules\menu\models;

use App\Models\AppCradleModel;

class MenuItem extends AppCradleModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    
    public function menu()
    {
        return $this->belongsTo('Cradle\modules\menu\models\Menu');
    }
}
