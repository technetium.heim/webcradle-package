<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu_id = DB::table("menus")->insertGetId([
            "name" => "admin",
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
        ]);

        //layout
            $menu_item_id = DB::table("menu_items")->insertGetId([
                "menu_id" => $menu_id,
                "icon" => "fa-columns",
                "text" => "Layout",
                "sort" => 30,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]);
            
            DB::table("menu_items")->insert([
                "menu_id" => $menu_id,
                "parent_id" => $menu_item_id,
                "text" => "Menus",
                "action" => "route",
                "target" => "bread.menu.index",
                "route" => "admin/bread/menu",
                "sort" => 1,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]);

        //resource
            $menu_item_id = DB::table("menu_items")->insertGetId([
                "menu_id" => $menu_id,
                "icon" => "fa-database",
                "text" => "Resource",
                "sort" => 50,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]);
            
            DB::table("menu_items")->insert([
                "menu_id" => $menu_id,
                "parent_id" => $menu_item_id,
                "text" => "Datasets",
                "action" => "route",
                "target" => "bread.dataset.index",
                "route" => "admin/bread/dataset",
                "sort" => 1,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]);

        //menu
            $menu_item_id = DB::table("menu_items")->insertGetId([
                "menu_id" => $menu_id,
                "icon" => "fa-users",
                "text" => "Accounts",
                "sort" => 90,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]);

            DB::table("menu_items")->insert([
                "menu_id" => $menu_id,
                "parent_id" => $menu_item_id,
                "text" => "Users",
                "action" => "route",
                "target" => "bread.user.index",
                "route" => "admin/bread/user",
                "sort" => 1,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]);
            DB::table("menu_items")->insert([
                "menu_id" => $menu_id,
                "parent_id" => $menu_item_id,
                "text" => "Roles",
                "action" => "route",
                "target" => "bread.role.index",
                "route" => "admin/bread/role",
                "sort" => 2,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]);
            DB::table("menu_items")->insert([
                "menu_id" => $menu_id,
                "parent_id" => $menu_item_id,
                "text" => "Permissions",
                "action" => "route",
                "target" => "bread.permission.index",
                "route" => "admin/bread/permission",
                "sort" => 3,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]);
    }
}
