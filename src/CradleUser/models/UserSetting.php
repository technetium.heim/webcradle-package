<?php

namespace App\Models;

use App\Models\AppCradleModel;
use Cradle\CradleUser\defaults\CradleUserSettingModel;

class UserSetting extends AppCradleModel
{
    use CradleUserSettingModel;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['locale'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
  
}
