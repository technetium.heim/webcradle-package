<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
// use Illuminate\Foundation\Auth\User as Authenticatable;
use Cradle\basic\foundation\auth\CradleUser as Authenticatable;

use Cradle\CradleUser\notifications\UserResetPassword;
use Cradle\CradleUser\defaults\CradleUserModel;

class User extends Authenticatable
{
    use Notifiable;
    use CradleUserModel;    

    protected $cascadeDeletes = [];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'pivot'
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new UserResetPassword($token));
    }
}
