<?php
namespace Cradle\CradleUser\defaults;


trait CradleUserSettingModel
{
  
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}