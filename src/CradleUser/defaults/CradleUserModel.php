<?php
namespace Cradle\CradleUser\defaults;

use Cradle\modules\role\foundation\UserRoleTrait;


trait CradleUserModel
{
 	use UserRoleTrait;

 	public function profile() 
    {
        return $this->hasOne('App\Models\UserProfile');
    }

    public function setting()
    {
    	return $this->hasOne('App\Models\UserSetting');
    }
}