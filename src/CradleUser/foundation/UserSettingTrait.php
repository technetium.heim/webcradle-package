<?php

namespace Cradle\CradleUser\foundation;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Auth;

trait UserSettingTrait
{
    public function updatePassword(Request $request)
    {
        $rules = $this->rulesUpdatePassword();
        $errormsg =  $this->errorMsgUpdatePassword();        

        $this->validate($request, $rules, $errormsg);

        $oldpassword = $request->input('password'); 
        $newpassword = $request->input('newpassword');

        $user = $this->guard()->user();

        if (Hash::check($oldpassword, $user->password)) {
           
            $user->password = Hash::make($newpassword);
            $user->save();
            $request->session()->flash('status', 'Password Updated'); 
        }else {
            $request->session()->flash('error', 'Incorrect Password');   
        }
        

        return redirect()->back();
    }

    protected function guard()
    {
        return Auth::guard();
    }

    // UPDATE PASSWORD
    protected function rulesUpdatePassword()
    {
        return [
            'password' =>  'required|min:6',
            'newpassword' => 'required|confirmed|min:6',
        ];
    }

    protected function errorMsgUpdatePassword()
    {
        return [
            'newpassword.confirmed'  => 'The new password confirmation does not match',
            'newpassword.min'  => 'The new password must be at least 6 characters.',
        ];
    }

    public function updateEmail(Request $request)
    {
        
        $rules= $this->rulesUpdateEmail();

        $errormsg = $this->errorMsgUpdateEmail();

        $this->validate($request,  $rules, $errormsg);

        $password = $request->input('password'); 
        $newemail = $request->input('email');

        $user = $this->guard()->user();

        if (Hash::check($password, $user->password)) {

            $user->email = $newemail;
            $user->save();
            
            $request->session()->flash('status', 'Email Updated'); 
        }else {
            $request->session()->flash('error', 'Incorrect Password');  
        }
        

        return redirect()->back();
    }

    public function updateLanguage(Request $request)
    {
        $language = $request->input('language');
        $settings = Auth::user()->setting;
        $settings->language = $language;
        $settings->save();

         return redirect()->back();
    }

    public function delete(Request $request)
    {
        $rules= $this->rulesDeleteAccount();

        $errormsg = $this->errorMsgDeleteAccount();

        $this->validate($request,  $rules, $errormsg);

        $password = $request->input('password'); 

        $user = $this->guard()->user();

        if (Hash::check($password, $user->password)) {
            $request->session()->flash('hint', 'Account Deleted'); 
            $user->delete();            
        }else {
            $request->session()->flash('error', 'Incorrect Password');  
        }
        

        return redirect()->back();
    }

}