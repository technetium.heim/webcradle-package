<?php

namespace Cradle\CradleUser\controllers\user\auth;

use App\Http\Controllers\CradleController;
use Cradle\basic\foundation\auth\CradleRegistersUsers;
use App\Models\User;

class RegisterController extends CradleController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use CradleRegistersUsers;
    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        $page = $this->setTitle('User Register Page');
        return view('UserView::user.units.auth.page.register', $page);
    }


    protected function cradleRedirect($condition) {
        switch ($condition) {
            case 'register':
                return Cradle::config('project.redirect.user_after_register'); 
                break;
            default:
                return 'cradle.home';
                break;
        }
        
    }

    protected function create(array $data)
    {
        $user= new User;
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->save();
        return $user;
        // return User::create([
        //     'name' => $data['name'],
        //     'email' => $data['email'],
        //     'password' => bcrypt($data['password']),
        // ]);
    }

    protected function registerRules()
    {
        return [
            'name' => 'required|max:30',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ];
    }
}
