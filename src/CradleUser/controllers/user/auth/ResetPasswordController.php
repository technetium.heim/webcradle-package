<?php

namespace Cradle\CradleUser\controllers\user\auth;


use App\Http\Controllers\CradleController;
use Cradle\basic\foundation\auth\CradleResetsPasswords;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use Auth;

class ResetPasswordController extends CradleController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use CradleResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm(Request $request, $token = null)
    {   
        $this->page['title'] = "Reset Password";
        $this->page['meta']['description'] = 'A project to create website faster';
        return view('UserView::user.units.auth.passwords.reset', $this->page)->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    protected function cradleRedirect($condition) {
        switch ($condition) {
            case 'reset':
                return 'cradle.home'; //empty for intended location.
                break;
            default:
                return 'cradle.home';
                break;
        }    
    }

    protected function guard()
    {
        return Auth::guard();
    }

    public function broker()
    {
        return Password::broker();
    }
}
