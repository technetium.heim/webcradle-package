<?php

namespace Cradle\CradleUser\controllers\user\auth;

use App\Http\Controllers\CradleController;
use Cradle\basic\foundation\auth\CradleAuthenticatesUsers;
use Auth;
use Cradle;

class LoginController extends CradleController
{
    use CradleAuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }


    // Settings for user group 
    // Parameters: username, redirect, guard, login form
    public function showLoginForm()
    {
        $page = $this->setTitle('User Login Page');
        return view('UserView::user.units.auth.page.login', $page);
    }

    protected function cradleRedirect($condition) {
        switch ($condition) {
            case 'login': 
                return Cradle::config('project.redirect.user_after_login'); 
                break;
            case 'logout': 
                return Cradle::config('project.redirect.user_after_logout'); 
                break;
            default:
                return 'cradle.home';
                break;
        }    
    }

    protected function loginRules()
    {   
        return [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ];
    }

    protected function guard()
    {
        return Auth::guard();
    }   

    public function username()
    {
        return 'email';
    }

}
