<?php

namespace Cradle\CradleUser\controllers\user\auth;

use App\Http\Controllers\CradleController;
use Cradle\basic\foundation\auth\CradleSendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends CradleController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use CradleSendsPasswordResetEmails;

    protected $redirectTo = '/home';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {
        $this->page['title'] = "Forgot Password";
        $this->page['meta']['description'] = 'A project to create website faster';
        return view('UserView::user.units.auth.passwords.email', $this->page);
    }

    public function broker()
    {
        return Password::broker();
    }
}