<?php

namespace Cradle\CradleUser\controllers\user;

use App\Http\Controllers\CradleController;

class CradlePageController extends CradleController
{
    protected $page;
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function index($folder, $page = '')
    {
        if($page != '') {
            $this->page['title'] = ucwords($page);
            $this->page['meta']['description'] = 'A project to create website faster';
            return view('UserView::user/pages/'.$folder.'/'.$page,$this->page);
        }
        else {
            $this->page['title'] = ucwords($folder);
            $this->page['meta']['description'] = 'A project to create website faster';
            return view('UserView::user/pages/'.$folder,$this->page);
        }
    }

}
