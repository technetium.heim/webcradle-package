<?php

namespace Cradle\CradleUser\controllers\user\setting;

use App\Http\Controllers\CradleController;
use Cradle\CradleUser\foundation\UserSettingTrait;
use Auth;

class UserSettingController extends CradleController
{
    use UserSettingTrait;

    protected $redirectTo = '/home';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
    *    Show Account Edit Page
    */
    public function showAccountEditPage() {
        $this->page['title'] = "Account Settings";
        $this->page['meta']['description'] = 'User Account Settings';
        return view('UserView::setting.account_edit', $this->page);         
    }

    /*
    *    Show Account Delete Page
    */
    public function showAccountDeletePage() {
        $this->page['title'] = "Account Delete";
        $this->page['meta']['description'] = 'User Account Delete';
        return view('UserView::setting.account_delete', $this->page);         
    }

    /*
    *    Show Account Social Page
    */
    public function showAccountConnectPage() {
        $connect= [];
        if(file_exists(app_path('/Models/SocialAccount.php'))){
            $connect = \App\Models\SocialAccount::where('user_id', $this->guard()->user()->id)->get();
        }

        $this->page['title'] = "Account Connected App";
        $this->page['meta']['description'] = 'User Connected App';
        return view('UserView::setting.account_connect', $this->page)->withConnects($connect);         
    }

    /*
    *    Show Account Language
    */
    public function showAccountLanguagePage() {
        $page = $this->setTitle('Account Language');
        $languages = config('translator.available_locales');
        return view('UserView::setting.account_language', $page)->withLanguages($languages); 
    }

    /*
    *   Guard for auth.
    */
    protected function guard()
    {
        return Auth::guard();
    }

    // UPDATE PASSWORD
    protected function rulesUpdatePassword()
    {
        return [
            'password' =>  'required|min:6',
            'newpassword' => 'required|confirmed|min:6',
        ];
    }

    protected function errorMsgUpdatePassword()
    {
        return [
            'newpassword.confirmed'  => 'The new password confirmation does not match',
            'newpassword.min'  => 'The new password must be at least 6 characters.',
        ];
    }


    // UPDATE EMAIL
    protected function rulesUpdateEmail()
    {
        return [
            'password' =>  'required|min:6',
            'email' => 'required|email|max:255|unique:users',
        ];
    }

    protected function errorMsgUpdateEmail()
    {
        return [];
    }

     // UPDATE EMAIL
    protected function rulesDeleteAccount()
    {
        return [
            'password' =>  'required',
            'confirmation' => 'in:DELETE',
        ];
    }

    protected function errorMsgDeleteAccount()
    {
        return [];
    }
}
