<?php

Route::group(['middleware' => 'web'], function() { // middleware web required for error share

	$PageController ='Cradle\CradleUser\controllers\user\CradlePageController';

	// Route::get('/home', $PageController.'@index')->name('cradle.home');
	Route::get('/home', function() { \Session::reflash(); return redirect('pages/home'); })->name('cradle.home');
	Route::get('/about', function() { \Session::reflash(); return redirect('pages/about/company'); })->name('cradle.about');
	Route::get('/contact', function() { \Session::reflash(); return redirect('pages/contact'); })->name('cradle.contact');
	Route::get('/pages/{folder}/{page?}', $PageController.'@index')->name('cradle.page');
	
	// AUTH ROUTE

	$LoginController = 'Cradle\CradleUser\controllers\user\auth\LoginController';
	$RegisterController = 'Cradle\CradleUser\controllers\user\auth\RegisterController';
	$ForgotController = 'Cradle\CradleUser\controllers\user\auth\ForgotPasswordController';
	$ResetController = 'Cradle\CradleUser\controllers\user\auth\ResetPasswordController';

	// User Auth Route
	Route::post('/signup', $RegisterController.'@register')->name('cradle.register.submit');
	Route::post('/login', $LoginController.'@login')->name('cradle.login.submit');
	Route::post('/logout', $LoginController.'@logout')->name('cradle.logout');
	// User reset password route
	Route::get('/password/forget', $ForgotController.'@showLinkRequestForm')->name('cradle.password.forget');
	Route::post('/password/email', $ForgotController.'@sendResetLinkEmail')->name('cradle.password.email');
	Route::get('/password/reset/{token}', $ResetController.'@showResetForm')->name('cradle.password.reset');
	Route::post('/password/reset', $ResetController.'@reset')->name('cradle.password.reset.submit');
	
	$AccountController =  Cradle::config('project.controller.usersetting');
	// User Account Route
	Route::get('/account/show', $AccountController.'@showAccountEditPage')->name('cradle.account.show');
	Route::get('/account/delete', $AccountController.'@showAccountDeletePage')->name('cradle.account.delete');
	Route::get('/account/connect', $AccountController.'@showAccountConnectPage')->name('cradle.account.connect');
	Route::post('/account/password/update', $AccountController.'@updatePassword')->name('cradle.account.password.update');
	Route::post('/account/email/update', $AccountController.'@updateEmail')->name('cradle.account.email.update');
	Route::post('/account/delete', $AccountController.'@delete')->name('cradle.account.delete.submit');
	Route::get('/account/language', $AccountController.'@showAccountLanguagePage')->name('cradle.account.language');
	Route::post('/account/language', $AccountController.'@updateLanguage')->name('cradle.account.language.submit');

	// User Page Login Route
	if (Cradle::config('project.user_auth_page') == 'enable')	{
		Route::get('/login', $LoginController.'@showLoginForm')->name('cradle.login');
		Route::get('/signup', $RegisterController.'@showRegistrationForm')->name('cradle.register');
	}

	// Socialite Login Route
	if (Cradle::config('project.socialite') == 'enable')	{
 		Route::get('/redirect/{provider}', $controller.'@redirect')->name('redirect.social');
		Route::get('/callback/{provider}', $controller.'@callback')->name('callback.social');
	}

	// testing page
	Route::get('/testing', function () {

		return view('UserView::user.testing',[ 'title'=> 'test', 'meta'=> [ 'description'=>'test' ] ]);

	});
});