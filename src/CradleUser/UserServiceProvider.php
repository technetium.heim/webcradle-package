<?php

namespace Cradle\CradleUser;

use Illuminate\Support\ServiceProvider;
use Cradle\basic\supports\CradleServiceProviderTrait;

class UserServiceProvider extends ServiceProvider
{
    use CradleServiceProviderTrait;
    // protected $defer = true;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {    
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadViewsFrom(__DIR__.'/views', 'UserView');       
        $this->publishCradle();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    public function publishCradle() 
    {
        //load migrations
        $this->loadMigrationsFrom(__DIR__.'/migrations');

        $this->publishes([
        __DIR__.'/models' => app_path('/Models'),
        ],"user-once");

        $this->pushGroupsList('users', ['user-once'] );
    }

}
