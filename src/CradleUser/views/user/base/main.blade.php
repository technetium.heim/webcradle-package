<!-- Layout -->
    @extends('WCView::general.base.main')

<!-- Page -->
    @section('main-sky')
        <!-- header -->
            @include('UserView::user.components.headers.main')

        <!-- tab -->
            @include('UserView::user.components.tabs.main')

        <!-- navbar -->
            @yield('page-navbar','')

        <!-- toolbar -->
    @endsection

    @section('main-ceiling')
        <div class="row">
            @if(session('status'))
                @php
                    switch (session('status')) {
                        case 'error':
                            $type = 'danger';
                            break;
                        default:
                            $type = 'success';
                            break;
                    }
                @endphp
                <div class="alert alert-{{ $type }}">
                    @if(count(session('message')) > 1)
                        @foreach(session('message') as $message)
                            <li>{!! $message !!}</li>
                        @endforeach
                    @else 
                        {!! session('message')[0] !!}
                    @endif
                </div>
            @endif
        </div>
    @endsection

    @section('main-column')
        <div class="row">
            @if($column_left == 1)
                <div class="col-left row">
                    @yield('page-column-left','')
                    @yield('layout-column-left','')
                </div>
            @endif
            @if($column_left == 1 && $column_right == 1)
                <div class="col-center col-center-2-column row">
            @elseif($column_left == 1 || $column_right == 1)
                <div class="col-center col-center-1-column row">
            @else
                <div class="col-center col-center-0-column row">
            @endif
                    @yield('page-column-center','')
                </div>
            @if($column_right == 1)
                <div class="col-right row">
                    @yield('page-column-right','')
                    @yield('layout-column-ight','')
                </div>
            @endif
        </div>
    @endsection

    @section('main-floor')
        <!-- footer -->
            @include('UserView::user.components.footers.main')
    @endsection

    @section('main-ground')
        <!-- mobile bottom navbar -->
            @yield('page-bottomnav','')
    @endsection

<!-- Modal -->
    @section('modal')
        <!-- Common--> 
            <!-- User Auth --> 
                @include('UserView::user.units.auth.login')
                @include('UserView::user.units.auth.register')
            <!-- Menu -->
                @include('UserView::user.components.menus.main')
        <!-- Specific--> 
            @yield('page-modal','')
    @endsection

<!-- Script -->
    @section('script-top')
        @yield('page-script-top','')
    @endsection

    @section('script-analytic')  
    @endsection

    @section('script-bottom') 
        @yield('page-script-bottom','')
    @endsection