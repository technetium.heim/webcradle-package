@php


@endphp

<!-- layout -->
    @extends('UserView::user.layouts.template.empty.basic')

<!-- navbar -->
    @section('page-navbar')
        @component('WCView::general.components.navbars.extend.primary')
            @slot('title',$title)
        @endcomponent
    @endsection

<!-- column left -->
    @section('page-column-left')
    @endsection

<!-- column center -->
    @section('page-column-center')
    	{{ trans('auth.failed') }}
        {{ trans('testtest.item1') }}

        <br>
        @php 
            $form = FForm::makeForm();
            $form->setURL( route('cradle.home') );
            $form->newInput( 'id' , 'text');
            $form->newInput( 'birthday' )->setType('date');
            $form->newInput( 'test' , 'multiple')->addOptions(['1'=>'happy', '2'=>'sad']);
            $form->newInput( 'textarea' )->setType('textarea');

            {{-- dd( $form->getInput( 'id' ) ); --}}
        @endphp

            {!! $form->render() !!}
    @endsection


<!-- column right -->
    @section('page-column-right')
    @endsection 

<!-- modal -->
    @section('page-modal')
    @endsection

<!-- script-top -->
    @section('page-script-top')
    @endsection

<!-- script-bottom -->
    @section('page-script-bottom')
    @endsection
