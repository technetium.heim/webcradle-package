@extends('WCView::general.components.links.main')

@php
    $links = array
    (
        (object) array(
            "type" => "section",
            "account" => "user",
        ),
        (object) array(
            "type" => "item",
            "icon" => "fa-home",
            "text" => "Home",
            "action" => "uri",
            "target" => route('cradle.home'),
            "route" => "home",
        ),
          {{-- <!--
        (object) array(
            "type" => "section",
            "account" => "user",
        ),
        (object) array(
            "type" => "item",
            "account" => "user",
            "icon" => "fa-address-card-o",
            "text" => "My Dashboard",
            "action" => "uri",
            "target" => route('cradle.dashboard'),
            "route" => "",
        ),
         (object) array(
            "type" => "item",
            "account" => "user",
            "icon" => "fa-user-circle",
            "text" => "My Profile",
            "action" => "uri",
            "target" => "",
            "route" => "",
        ),
        (object) array(
            "type" => "item",
            "account" => "user",
            "icon" => "fa-key",
            "text" => "My Account",
            "action" => "uri",
            "target" => "",
            "route" => "",
        ), --> --}}
        (object) array(
            "type" => "section"
        ),
        (object) array(
            "type" => "item",
            "account" => "guest",
            "icon" => "fa-lock",
            "text" => "Log In",
            "action" => "modal",
            "target" =>"#modal-user-login",
            "route" => "",
        ),
        (object) array(
            "type" => "item",
            "account" => "guest",
            "icon" => "fa-sign-in",
            "text" => "Sign Up",
            "action" => "modal",
            "target" =>"#modal-user-register", 
            "route" => "",
        ),
        (object) array(
            "type" => "item",
            "account" => "user",
            "icon" => "fa-unlock-alt",
            "text" => "Log Out",
            "action" => "script",
            "target" => "$('#logout-form').submit();",
            "route" => "",
        ),
    );
@endphp

@if(Auth::check())
    @php
        array_unshift($links,
            (object) array(
                "type" => "item",
                "account" => "user",
                "icon" => "fa-user-circle-o",
                "text" => Auth::user()->name,
                "action" => "uri",
                "target" => "",
                "route" => "",
            )
        );
    @endphp
@endif