@extends('WCView::general.components.links.main')

@php
    $links = array
    (
        (object) array(
            "type" => "item",
            "icon" => "fa-home",
            "text" => "Home",
            "action" => "uri",
            "target" => route('cradle.home'),
            "route" => "home",
        ),
        (object) array(
            "type" => "item",
            "icon" => "",
            "text" => "About",
            "action" => "uri",
            "target" => route('cradle.about'),
            "route" => "about",
        ),
        (object) array(
            "type" => "item",
            "icon" => "",
            "text" => "Contact",
            "action" => "uri",
            "target" => route('cradle.contact'),
            "route" => "contact",
        ), 
    );
@endphp