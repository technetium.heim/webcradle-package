@extends('WCView::general.components.links.main')

@php
    $links = array
    (
        (object) array(
            "type" => "item",
            "icon" => "",
            "text" => config('app.name'),
            "action" => "uri",
            "target" => route('cradle.page',['folder'=>'about','page'=>'company']),
            "route" => "about/company",
        ),
        (object) array(
            "type" => "item",
            "icon" => "",
            "text" => "Team",
            "action" => "uri",
            "target" => route('cradle.page',['folder'=>'about','page'=>'team']),
            "route" => "about/team",
        ),
        (object) array(
            "type" => "item",
            "icon" => "",
            "text" => "Portfolio",
            "action" => "uri",
            "target" => route('cradle.page',['folder'=>'about','page'=>'portfolio']),
            "route" => "about/portfolio",
        ),
        (object) array(
            "type" => "item",
            "icon" => "",
            "text" => "Testimonials",
            "action" => "uri",
            "target" => route('cradle.page',['folder'=>'about','page'=>'testimonials']),
            "route" => "about/testimonials",
        ),
        (object) array(
            "type" => "item",
            "icon" => "",
            "text" => "Awards",
            "action" => "uri",
            "target" => route('cradle.page',['folder'=>'about','page'=>'awards']),
            "route" => "about/awards",
        ),
    );
@endphp