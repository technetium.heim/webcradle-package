@component('WCView::general.components.tabs.main')
    @slot('content')
        @include('UserView::user.components.links.tab')
    @endslot
@endcomponent