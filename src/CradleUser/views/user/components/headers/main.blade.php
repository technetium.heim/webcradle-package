@extends('WCView::general.components.headers.main')

@section('header-content')
	<div class="row">
		<div class="col-xs-4 text-left">
			<div class="header-btn pull-left">
				<a class="btn" data-toggle="modal" data-target="#menu">
					<i class="fa fa-fw fa-lg fa-bars"></i>
				</a>
			</div>
			<div class="header-title pull-left">
				<span class="text-primary">{{ config('app.name') }}</span>
			</div>
		</div>
		<div class="col-xs-4 text-center">
		</div>
		<div class="col-xs-4 pull-right text-right">
			@if(Auth::check())
				<div class="header-btn">
					<a class="btn">
						<i class="fa fa-fw fa-lg fa-user-circle-o"></i>
					</a>
				</div>
			@else
				<a class="btn btn-link" data-toggle="modal" data-target="#modal-user-login">
					<span>Log In</span>
				</a>
				<a class="btn btn-default" data-toggle="modal" data-target="#modal-user-register">
					<span>Sign Up</span>
				</a>
			@endif
		</div>
	</div>
@endsection