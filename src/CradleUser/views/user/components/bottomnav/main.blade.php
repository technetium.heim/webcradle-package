@extends('WCView::general.components.bottomnav.main')

@php
    $links = array
    (
        (object) array(
            "type" => "item",
            "icon" => "fa-bullhorn",
            "text" => "News",
            "action" => "uri",
            "target" => route('cradle.page',['folder'=>'about','page'=>'company']),
            "route" => "home",
        ),
        (object) array(
            "type" => "item",
            "icon" => "fa-clipboard",
            "text" => "Notices",
            "action" => "uri",
            "target" => route('cradle.page',['folder'=>'about','page'=>'team']),
            "route" => "about/team",
        ),
        (object) array(
            "type" => "item",
            "icon" => "fa-th",
            "text" => "More",
            "action" => "uri",
            "target" => route('cradle.page',['folder'=>'about','page'=>'portfolio']),
            "route" => "about/portfolio",
        ),
        (object) array(
            "type" => "item",
            "icon" => "fa-users",
            "text" => "Visitors",
            "action" => "uri",
            "target" => route('cradle.page',['folder'=>'about','page'=>'testimonials']),
            "route" => "about/testimonials",
        ),
        (object) array(
            "type" => "item",
            "icon" => "fa-building",
            "text" => "Facilities",
            "action" => "uri",
            "target" => route('cradle.page',['folder'=>'about','page'=>'awards']),
            "route" => "about/awards",
        ),
    );
@endphp