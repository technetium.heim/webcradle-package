@extends('WCView::general.components.footers.main')

@section('footer_content')
	<div class="row">
		<div class="col-xs-12">
			@include('UserView::user.components.links.tab')
		</div>
	</div>
	<div class="row">
        <div class="col-xs-12">
            <div class="trademark">
                <div class="small">Designed by</div>
                <div><b>{{ config('app.author') }}</b></div>
            </div>
        </div>
    </div>
@endsection