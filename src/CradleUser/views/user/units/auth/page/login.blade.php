<!-- layout -->
	@extends('UserView::user.layouts.template.white.basic')

<!-- navbar -->
	@section('page-navbar')
		@component('WCView::general.components.navbars.extend.primary')
			@slot('title',$title)
	    @endcomponent
	@endsection

<!-- column left -->
	@section('page-column-left')
	@endsection

<!-- column center -->
	@section('page-column-center')
		@component('WCView::general.components.contents.extend.blank')
            @slot('title','')
            @slot('content')
            	@component('WCView::general.components.panels.main')
                @slot('title','User Login')
                @slot('content')
                    <div class="row">
		                <div class="max-width-ls">

		                <form class="form-horizontal" role="form" method="POST" action="{{ route('cradle.login.submit') }}">
		                    {{ csrf_field() }}
		                    <div class="row padding no-padding-bottom">
		                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
		                            <div class="col-xs-12 col-sm-3 text-left">
		                                <label for="email" class="control-label">Email</label>
		                            </div>
		                            <div class="col-xs-12 col-sm-9">
		                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
		                                @if ($errors->has('email'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('email') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>
		                    </div>
		                    <div class="row padding no-padding-top">
		                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
		                            <div class="col-xs-12 col-sm-3 text-left">
		                                <label for="password" class="control-label">Password</label>
		                            </div>
		                            <div class="col-xs-12 col-sm-9">
		                                <input id="password" type="password" class="form-control" name="password" required>
		                                @if ($errors->has('password'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('password') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>
		                    </div>
		                    <div class="row">
		                        <div class="col-xs-6 text-left">
		                            <div class="checkbox">
		                                <label>
		                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
		                                </label>
		                            </div>
		                        </div>
		                        <div class="col-xs-6 text-right">
		                            <a class="btn btn-link" href="{{ route('cradle.password.forget') }}">
		                                Forgot Password?
		                            </a>
		                        </div>
		                    </div>
		                    <div class="row">
		                        <div class="col-xs-12 text-left">
		                            <button type="submit" class="btn btn-primary btn-block">
		                                Login
		                            </button>
		                        </div>
		                    </div>
		                    <hr>
		                    <div class="row">
		                        <div class="col-xs-12 text-left">
		                            <span>Don't have an account? <a class="btn-link" href="{{ route('cradle.register') }}">
		                                Sign Up
		                            </a></span>
		                        </div>
		                    </div>
		            
		                </form>
		                </div>
			        </div>
                @endslot
                @endcomponent
            @endslot
        @endcomponent
	@endsection


<!-- column right -->
	@section('page-column-right')
	@endsection	

<!-- modal -->
	@section('page-modal')
	@endsection

<!-- script-top -->
	@section('page-script-top')
	@endsection

<!-- script-bottom -->
	@section('page-script-bottom')
	@endsection