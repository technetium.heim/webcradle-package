<!-- layout -->
	@extends('UserView::user.layouts.template.white.basic')

<!-- navbar -->
	@section('page-navbar')
		@component('WCView::general.components.navbars.extend.primary')
			@slot('title',$title)
	    @endcomponent
	@endsection

<!-- column left -->
	@section('page-column-left')
	@endsection

<!-- column center -->
	@section('page-column-center')
		@component('WCView::general.components.contents.extend.blank')
            @slot('title','')
            @slot('content')
            	@component('WCView::general.components.panels.main')
                @slot('title','User Login')
                @slot('content')
                    <div class="row">
				        <div class="max-width-ls">
		                <form class="form-horizontal" role="form" method="POST" action="{{ route('cradle.register.submit') }}">
		                    {{ csrf_field() }}
		                    <div class="row padding no-padding-bottom">
		                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
		                            <div class="col-xs-12 col-sm-3 text-left">
		                                <label for="name" class="control-label">Name</label>
		                            </div>
		                            <div class="col-xs-12 col-sm-9">
		                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
		                                @if ($errors->has('name'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('name') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>
		                    </div>
		                    <div class="row padding no-padding-v">
		                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
		                            <div class="col-xs-12 col-sm-3 text-left">
		                                <label for="email" class="control-label">Email</label>
		                            </div>
		                            <div class="col-xs-12 col-sm-9">
		                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
		                                @if ($errors->has('email'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('email') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>
		                    </div>
		                    <div class="row padding no-padding-v">
		                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
		                            <div class="col-xs-12 col-sm-3 text-left">
		                                <label for="password" class="control-label">Password</label>
		                            </div>
		                            <div class="col-xs-12 col-sm-9">
		                                <input id="password" type="password" class="form-control" name="password" required>
		                                @if ($errors->has('password'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('password') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>
		                    </div>
		                    <div class="row padding no-padding-top">
		                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
		                            <div class="col-xs-12 col-sm-3 text-left">
		                                <label for="password_confirmation" class="control-label">Confirm Password</label>
		                            </div>
		                            <div class="col-xs-12 col-sm-9">
		                                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" required>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="row">
		                        <div class="col-xs-12 text-left">
		                            <button type="submit" class="btn btn-primary btn-block">
		                                Sign Up
		                            </button>
		                        </div>
		                    </div>
		                    <hr>
		                    <div class="row">
		                        <div class="col-xs-12 text-left">
		                            <span>Own an account?<a class="btn-link" href="{{ route('cradle.login') }}" >
		                                Log In
		                            </a></span>
		                        </div>
		                    </div>
		                </form>
		                </div>
			        </div>
                @endslot
                @endcomponent
            @endslot
        @endcomponent
	@endsection


<!-- column right -->
	@section('page-column-right')
	@endsection	

<!-- modal -->
	@section('page-modal')
	@endsection

<!-- script-top -->
	@section('page-script-top')
	@endsection

<!-- script-bottom -->
	@section('page-script-bottom')
	@endsection