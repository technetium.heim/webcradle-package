@component('WCView::general.components.modals.extend.center') 
    @slot('modal_id', 'modal-user-register')
    @slot('modal_content_class', '')
    @slot('modal_header')
        @component('WCView::general.components.navbars.extend.modal.primary')
            @slot('title','Sign Up')
            @slot('left_action','dismiss')
            @slot('left_icon','fa-times')
        @endcomponent
    @endslot
    @slot('modal_body')
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 padding">
                <div class="max-width-ls">
                <form class="form-horizontal" role="form" method="POST" action="">
                    {{ csrf_field() }}
                    <div class="row padding no-padding-bottom">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <div class="col-xs-12 col-sm-3 text-left">
                                <label for="name" class="control-label">Name</label>
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row padding no-padding-v">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-xs-12 col-sm-3 text-left">
                                <label for="email" class="control-label">Email</label>
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row padding no-padding-v">
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-xs-12 col-sm-3 text-left">
                                <label for="password" class="control-label">Password</label>
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <input id="password" type="password" class="form-control" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row padding no-padding-top">
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-xs-12 col-sm-3 text-left">
                                <label for="password_confirmation" class="control-label">Confirm Password</label>
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 text-left">
                            <button id="submit-register" class="btn btn-primary btn-block">
                                Sign Up
                            </button>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-12 text-left">
                            <span>Own an account?<a class="btn-link" data-dismiss="modal" data-toggle="modal" data-target="#modal-user-login" >
                                Log In
                            </a></span>
                        </div>
                    </div>
                    @if ( config('cradleConfig.socialite') == 'enable' )
                        @include('WCView::general.modules.socialite.socialite-login')
                    @endif
                </form>
                </div>
            </div>
        </div>
    @endslot
    @slot('modal_footer')
    @endslot

    @slot('modal_script')
        <script>
            // ajax_validate(submit_selector, target_route );
            ajax_validate('#submit-register', "{!! URL::route('cradle.register.submit') !!}" );
        </script>
    @endslot
@endcomponent

