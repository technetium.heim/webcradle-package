<!-- layout -->
    @extends('UserView::user.layouts.template.white.basic')

<!-- navbar -->
    @section('page-navbar')
        @component('WCView::general.components.navbars.extend.primary')
            @slot('title',$title)
        @endcomponent
    @endsection

<!-- column left -->
    @section('page-column-left')
    @endsection

<!-- column center -->
    @section('page-column-center')
        @component('WCView::general.components.contents.extend.blank')
            @slot('title','')
            @slot('content')
                <div class="row padding">
                    <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                        <div class="panel panel-default">
                            <div class = "panel-heading">
                              Forget Password
                            </div>
                            <div class="panel-body">
                                <div class="padding text-left">
                                    <form class="form-horizontal" role="form" method="POST" action="{{ route('cradle.password.email') }}">
                                        {{ csrf_field() }}

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="control-label">E-Mail Address</label>

                                            <div class="">
                                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="">
                                                <button type="submit" class="btn btn-primary pull-right">
                                                    Send Password Reset Link
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>             
                    </div>
                </div>
            @endslot
        @endcomponent
    @endsection


<!-- column right -->
    @section('page-column-right')
    @endsection 

<!-- modal -->
    @section('page-modal')
    @endsection

<!-- script-top -->
    @section('page-script-top')
    @endsection

<!-- script-bottom -->
    @section('page-script-bottom')
    @endsection

