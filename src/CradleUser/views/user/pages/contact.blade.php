<!-- layout -->
	@extends('UserView::user.layouts.template.empty.left_column')

<!-- navbar -->
	@section('page-navbar')
		@component('WCView::general.components.navbars.extend.primary')
			@slot('title',$title)
	    @endcomponent
	@endsection

<!-- column left -->
	@section('page-column-left')
		@include('UserView::user.components.sidebars.contact')
	@endsection

<!-- column center -->
	@section('page-column-center')
		@component('WCView::general.components.contents.extend.card')
            @slot('title', $title)
            @slot('content')
            	<a name="info"></a>
				<div class="row text-center">
					<div class="col-xs-12">
						<div class="max-width-sm text-left">
							<div class="row">
								<div class="col-xs-12">
									<h2>General Info</h2>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="list-group">
										<div class="list-group-item">
											<i class="fa fa-fw fa-lg fa-building"></i>
											<i class="fa fa-fw"></i>
											<span class="text-b">Technetium Heim Sdn. Bhd.</span>
										</div>
										<div class="list-group-item ">
											<i class="fa fa-fw fa-lg fa-phone"></i>
											<i class="fa fa-fw"></i>
											<span>(+6012)-3989-864</span>
										</div>
										<div class="list-group-item ">
											<i class="fa fa-fw fa-lg fa-envelope"></i>
											<i class="fa fa-fw"></i>
											<span>technetium.heim@gmail.com</span>
										</div>
										<div class="list-group-item">
											<i class="fa fa-fw fa-lg fa-map-marker"></i>
											<i class="fa fa-fw"></i>
											<span>Kuala Lumpur, Malaysia</span>
										</div>
									</div>
								</div>
							</div>
							<a name="follow"></a>
							<hr>
							<div class="row">
								<div class="col-xs-12">
									<h2>Follow Us</h2>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<a class="btn btn-facebook"><i class="fa fa-fw fa-lg fa-facebook"></i></a>
									<a class="btn btn-google-plus"><i class="fa fa-fw fa-lg fa-google-plus"></i></a>
									<a class="btn btn-linkedin"><i class="fa fa-fw fa-lg fa-linkedin"></i></a>
									<a class="btn btn-twitter"><i class="fa fa-fw fa-lg fa-twitter"></i></a>
									<a class="btn btn-youtube"><i class="fa fa-fw fa-lg fa-youtube-play"></i></a>
								</div>
							</div>
							<a name="message"></a>
							<hr>
							<div class="row">
								<div class="col-xs-12">
									<h2>Message Us</h2>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<form class="form-horizontal margin-top padding">
										<div class="form-group">
							                <div class="col-xs-12 text-left col-sm-3">
							                    <label class="control-label" for="email" >Email</label>
							                </div>
							                <div class="col-xs-12 col-sm-9">
							                    <input class="form-control" id="email" name="email" type="email" value="" required="">
							                </div>
							            </div>
							            <div class="form-group">
							                <div class="col-xs-12 text-left col-sm-3">
							                    <label class="control-label" for="name" >Name</label>
							                </div>
							                <div class="col-xs-12 col-sm-9">
							                    <input class="form-control" id="name" name="name" type="text" value="" required="">
							                </div>
							            </div>
							            <div class="form-group">
							                <div class="col-xs-12 text-left col-sm-3">
							                    <label class="control-label" for="message" >Message</label>
							                </div>
							                <div class="col-xs-12 col-sm-9">
							                    <textarea class="form-control" id="message" name="message" rows="5"></textarea>
							                </div>
							            </div>
									</form>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-4 col-sm-push-8">
					            	<a class="btn btn-block btn-primary">Send</a>
								</div>
							</div>
						</div>
					</div>
				</div>
            @endslot
        @endcomponent
	@endsection


<!-- column right -->
	@section('page-column-right')
	@endsection	

<!-- modal -->
	@section('page-modal')
	@endsection

<!-- script-top -->
	@section('page-script-top')
	@endsection

<!-- script-bottom -->
	@section('page-script-bottom')
	@endsection