<!-- layout -->
	@extends('UserView::user.layouts.template.white.left_column')

<!-- navbar -->
	@section('page-navbar')
		@component('WCView::general.components.navbars.extend.primary')
			@slot('title',$title)
	    @endcomponent
	@endsection

<!-- column left -->
	@section('page-column-left')
		@include('UserView::user.components.sidebars.about')
	@endsection

<!-- column center -->
	@section('page-column-center')
		@component('WCView::general.components.contents.extend.blank')
            @slot('title', $title)
            @slot('content')
            @endslot
        @endcomponent
	@endsection


<!-- column right -->
	@section('page-column-right')
	@endsection	

<!-- modal -->
	@section('page-modal')
	@endsection

<!-- script-top -->
	@section('page-script-top')
	@endsection

<!-- script-bottom -->
	@section('page-script-bottom')
	@endsection