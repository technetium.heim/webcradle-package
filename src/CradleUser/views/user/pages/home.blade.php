<!-- layout -->
	@extends('UserView::user.layouts.template.white.basic')

<!-- navbar -->
	@section('page-navbar')
		@component('WCView::general.components.navbars.extend.primary')
			@slot('title',$title)
	    @endcomponent
	@endsection

<!-- column left -->
	@section('page-column-left')
	@endsection

<!-- column center -->
	@section('page-column-center')
		@component('WCView::general.components.contents.extend.blank')
            @slot('title','')
            @slot('content')
            	<div class="text-center">
					<div class="row">
				    	<div class="col-xs-12">
				    		<h2>Who We Are</h2>
						</div>
				    </div>
				    <div class="row">
				    	<div class="col-xs-12">
				    		<div class="max-width-sm">
					    		<div class="row">
							    	<div class="col-xs-4">
							    		<div><i class="fa fa-fw fa-5x fa-mobile"></i></div>
							    		<div>Mobile</div>
							    		<div>Mobile</div>
							    	</div>
							    	<div class="col-xs-4">
							    		<div><i class="fa fa-fw fa-5x fa-table"></i></div>
							    		<div>Database</div>
							    		<div>Mobile</div>
							    	</div>
							    	<div class="col-xs-4">
							    		<div><i class="fa fa-fw fa-5x fa-cloud-upload"></i></div>
							    		<div>Cloud</div>
							    		<div>Mobile</div>
							    	</div>
						    	</div>
					    	</div>
				    	</div>
				    </div>
				    <div class="row">
				    	<div class="col-xs-12">
						    <?php
						    	for($i=0;$i<100;$i++) {
						    		echo $i.'<br>';
						    	}
						    ?>
						</div>
				    </div>
				</div>
            @endslot
        @endcomponent
	@endsection


<!-- column right -->
	@section('page-column-right')
	@endsection	

<!-- modal -->
	@section('page-modal')
	@endsection

<!-- script-top -->
	@section('page-script-top')
	@endsection

<!-- script-bottom -->
	@section('page-script-bottom')
	@endsection