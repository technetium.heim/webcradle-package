    @extends('UserView::setting.account_base')

    @section('account-setting-content')
        @component('WCView::general.components.contents.extend.blank')
            @slot('title','')
            @slot('content')
            
                @component('WCView::general.components.panels.main')
                @slot('title','Delete Account')
                @slot('content')
                    <div class="alert alert-danger">
                        Are you sure?
                        Your account will be DELETED permanantly.
                    </div>
                    <div class="padding"> 
                        <form class="form-horizontal padding" role="form" method="POST" action="{{ route('cradle.account.delete.submit') }}">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="current_email" class="col-md-3 col-xs-4 col-form-label">Current Email</label>
                                <div class="col-md-9 col-xs-8">
                                    <span class="form-control">{{ Auth::user()->email }}</span>
                                </div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-3 col-xs-4 col-form-label">Password</label>

                                <div class="col-md-9 col-xs-8">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('confirmation') ? ' has-error' : '' }}">
                                <label for="confirmation" class="col-md-3 col-xs-4 col-form-label">Enter "DELETE"</label>

                                <div class="col-md-9 col-xs-8">
                                    <input id="confirmation"  type ="text" class="form-control" name="confirmation" required>

                                    @if ($errors->has('confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <hr>
                            <div class="form-group">
                                <button type="submit" class="btn btn-danger">
                                    Delete
                                </button>
                            </div>
                        </form>
                    </div>

                @endslot
                @endcomponent

            @endslot
        @endcomponent
    @endsection
