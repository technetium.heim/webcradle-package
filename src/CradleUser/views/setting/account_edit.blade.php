<!-- layout -->
    @extends('UserView::setting.account_base')

    @section('account-setting-content')
        @component('WCView::general.components.contents.extend.blank')
            @slot('title','')
            @slot('content')

                @component('WCView::general.components.panels.main')
                @slot('title','Update Password')
                @slot('content')
                    
                    <div class="padding"> 

                        <form class="form-horizontal padding" role="form" method="POST" action="{{ route('cradle.account.password.update') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-3 col-xs-4 col-form-label">Current Password</label>

                                <div class="col-md-9 col-xs-8">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('newpassword') ? ' has-error' : '' }}">
                                <label for="newpassword" class="col-md-3 col-xs-4 col-form-label">New Password</label>

                                <div class="col-md-9 col-xs-8">
                                    <input id="newpassword" type="password" class="form-control" name="newpassword" required>

                                    @if ($errors->has('newpassword'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('newpassword') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('newpassword_confirmation') ? ' has-error' : '' }}">
                                <label for="newpassword-confirm" class="col-md-3 col-xs-4 col-form-label">Confirm New Password</label>
                                <div class="col-md-9 col-xs-8">
                                    <input id="newpassword-confirm" type="password" class="form-control" name="newpassword_confirmation" required>

                                    @if ($errors->has('newpassword_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('newpassword_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <hr>
                            <div class="form-group">
                   
                                <button type="submit" class="btn btn-primary">
                                    Update Password
                                </button>

                            </div>
                        </form>
       
                    </div>
             

                @endslot
                @endcomponent
                
                {{-- <!-- Change Email --> --}}
                @component('WCView::general.components.panels.main')
                @slot('title','Update Email')
                @slot('content')
                    <div class="padding">
                        <form class="form-horizontal padding" role="form" method="POST" action="{{ route('cradle.account.email.update') }}">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="current_email" class="col-md-3 col-xs-4 col-form-label">Current Email</label>
                                <div class="col-md-9 col-xs-8">
                                    <span class="form-control">{{ Auth::user()->email }}</span>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-3 col-xs-4 col-form-label">Password</label>

                                <div class="col-md-9 col-xs-8">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-3 col-xs-4 col-form-label">New Email-Address</label>

                                <div class="col-md-9 col-xs-8">
                                    <input id="email"  class="form-control" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <hr>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    Update Email
                                </button>
                            </div>
                        </form>
                    </div>
                @endslot
                @endcomponent

                {{-- <!-- Logout --> --}}
                @component('WCView::general.components.panels.main')
                @slot('title','Logout')
                @slot('content')
                    <div class="padding">
                        <form class="form-horizontal padding" role="form" method="POST" action="{{ route('cradle.logout') }}">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <div class="col-md-12">
                                   <span class="form-control">{{ Auth::user()->email }}</span>
                                </div>
                            </div>

                            <hr>
                            <div class="form-group">
                                    <button type="submit" class="btn btn-primary">
                                        Logout
                                    </button>
                            </div>
                        </form>
                    </div>
                @endslot
                @endcomponent

            @endslot
        @endcomponent
    @endsection