@extends('UserView::setting.account_base')

@section('account-setting-content')
    @component('WCView::general.components.contents.extend.blank')
        @slot('title','')
        @slot('content')
        
            @component('WCView::general.components.panels.main')
            @slot('title','Connected Applications')
            @slot('content')
                    <div class="padding"> 
                        <form class="form-horizontal padding" role="form" method="POST" action="">
                            {{ csrf_field() }}
                            <div class="col-md-12">
                                @if ( empty($connects) || $connects->isEmpty())
                                    <span>"You haven’t connected any apps to your account."</span>
                                @else       
                                    @foreach ($connects as $connect)
                                    <div class="row text-left">  
                                        <div class="col-xs-12 col-md-5 bold">{{ $connect->getProviderName() }}</div>
                                        <div class="col-xs-12 col-md-7">{{ Auth::user()->email }}</div>
                                    </div>                           
                                    @endforeach
                            
                                @endif
                            </div>
                        </form>
                    </div>
         

            @endslot
            @endcomponent

        @endslot
    @endcomponent
@endsection

