<!-- layout -->
    @extends('UserView::user.layouts.template.empty.left_column')

<!-- navbar -->
    @section('page-navbar')
        @component('WCView::general.components.navbars.extend.primary')
            @slot('title',$title)
        @endcomponent
    @endsection

<!-- column left -->
    @section('page-column-left')
        
        @include ('UserView::setting.account_menu') 

    @endsection

<!-- column center -->
    @section('page-column-center')
        
        @yield('account-setting-content')
        
    @endsection


<!-- column right -->
    @section('page-column-right')
    @endsection 

<!-- modal -->
    @section('page-modal')
    @endsection

<!-- script-top -->
    @section('page-script-top')
    @endsection

<!-- script-bottom -->
    @section('page-script-bottom')
    @endsection

