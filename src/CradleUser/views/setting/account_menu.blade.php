@component('WCView::general.components.panels.main')
    @slot('title','')
    @slot('content')
        <ul class="list-group text-left">
            @if ( Route::has('cradle.dashboard')) 
                <a class="list-group-item" href="{{ route( 'cradle.dashboard' ) }}">Back To DashBoard</a>
            @else
                <a class="list-group-item" href="{{ route( Cradle::config('project.home_page') ) }}">Back To Home</a>
            @endif
        </ul>
    @endslot
@endcomponent

@php
    $links = array
    (
        (object) array(
            "type" => "item",
            "icon" => "",
            "text" => "Account Settings",
            "action" => "uri",
            "target" => route('cradle.account.show'),
            "route" => "account/show",
        ),
        (object) array(
            "type" => "item",
            "icon" => "",
            "text" => "Delete Account",
            "action" => "uri",
            "target" => route('cradle.account.delete'),
            "route" => "account/delete",
        ),
        (object) array(
            "type" => "item",
            "icon" => "",
            "text" => "Connected App",
            "action" => "uri",
            "target" => route('cradle.account.connect'),
            "route" => "account/connect",
        ),
        (object) array(
            "type" => "item",
            "icon" => "",
            "text" => "Set Language",
            "action" => "uri",
            "target" => route('cradle.account.language'),
            "route" => "account/language",
        ),
    );
@endphp

@component('WCView::general.components.panels.main')
    @slot('title','')
    @slot('content')
        @component('WCView::general.components.links.main', ['links' => $links ])
        @endcomponent
    @endslot
@endcomponent