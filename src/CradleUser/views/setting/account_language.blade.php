@extends('UserView::setting.account_base')

@section('account-setting-content')
    @component('WCView::general.components.contents.extend.blank')
        @slot('title','')
        @slot('content')
        
            @component('WCView::general.components.panels.main')
            @slot('title','Set Language')
            @slot('content')
                <div class="padding"> 
                    <form class="form-horizontal padding" role="form" method="POST" action="{{ route('cradle.account.language.submit') }}">
                        {{ csrf_field() }}
                        
                        <div class="form-group{{ $errors->has('language') ? ' has-error' : '' }}">
                            <label for="langauge" class="col-md-3 col-xs-4 col-form-label">Langauge</label>

                            <div class="col-md-9 col-xs-8">
                                <select id="language" class="form-control" name="language" required>
                                    @foreach($languages as $language)
                                        
                                        <option value="{{ $language }}"
                                        @if( Auth::user()->setting->language == $language)
                                        selected
                                        @endif
                                        >{{ strtoupper($language) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <hr>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                Save
                            </button>
                        </div>
                    </form>
                </div>

            @endslot
            @endcomponent

        @endslot
    @endcomponent
@endsection

