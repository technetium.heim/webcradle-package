<?php

Route::group(['middleware' => 'web'], function() { // middleware web required for error share
	Route::prefix(Cradle::config('project.doorbell.admin'))->group(function(){

		$PageController = 'Cradle\CradleAdmin\controllers\admin\CradlePageController';
		$LoginController = 'Cradle\CradleAdmin\controllers\admin\auth\LoginController';
		
		Route::get('/', function() {
			return redirect()->route('cradle.admin.dashboard');
		});

		//Dashboard
		Route::get('/dashboard', $PageController.'@index')->name('cradle.admin.dashboard');

		// Admin Auth Route
		Route::get('/login', $LoginController.'@showLoginForm')->name('cradle.admin.login');
		Route::post('/login', $LoginController.'@login')->name('cradle.admin.login.submit');
		Route::post('/logout', $LoginController.'@logout')->name('cradle.admin.logout');

		// Default Bread User & Set Role
		$bread_dir = 'Cradle\CradleAdmin\controllers\admin\bread';
		Cradle::routesBread( 'user' , $bread_dir.'\UserBreadController');
		Cradle::routesBread( 'role' , $bread_dir.'\RoleBreadController');
		Cradle::routesBread( 'permission' , $bread_dir.'\PermissionBreadController');
		Cradle::routesBread( 'dataset' , $bread_dir.'\DatasetBreadController');
		Cradle::routesBread( 'menu' , $bread_dir.'\MenuBreadController');
		Cradle::routesBread( 'menu_item' , $bread_dir.'\MenuItemBreadController');
		Cradle::routesBread( 'setting' , $bread_dir.'\SettingBreadController');

		// //Addon Passport : Web Service
		// Cradle::routesBread('oauth_client','App\Http\Controllers\admin\bread\OauthClientBreadController');
	
		// //Addon Passport-client: Api
		// Route::get('/api/test','App\Http\Controllers\admin\api\ApiTestController@index')->name('admin.api.test');
		// Route::post('/api/token/test','App\Http\Controllers\admin\api\ApiTestController@request')->name('admin.api.test.request');
		// Route::get('/api/token','App\Http\Controllers\admin\api\ApiTokenController@index')->name('admin.api.token');
		// Route::post('/api/token/request','App\Http\Controllers\admin\api\ApiTokenController@request')->name('admin.api.token.request');
		// Cradle::routesBread('api', "App\Http\Controllers\admin\bread\ApiBreadController");

	});
});
