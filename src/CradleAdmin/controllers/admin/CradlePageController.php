<?php

namespace Cradle\CradleAdmin\controllers\admin;

use App\Http\Controllers\CradleController;

class CradlePageController extends CradleController
{

    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $this->page['title'] = "Admin Dashboard";
        $this->page['meta']['description'] = "Admin Dashboard";
        return view('AdminView::admin.pages.dashboard', $this->page);
    }

}
