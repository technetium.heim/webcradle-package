<?php

namespace Cradle\CradleAdmin\controllers\admin\auth;

use App\Http\Controllers\CradleController;
use Cradle\basic\foundation\auth\CradleAuthenticatesUsers;
use Auth;
use View;
use Cradle;

class LoginController extends CradleController
{
    use CradleAuthenticatesUsers;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['logout','showLoginForm' ]]);
    }


    // Settings for user group 
    // Parameters: username, redirect, guard, login form
    public function showLoginForm()
    {
        if (Auth::check() && Auth::user()->hasRole('admin'))
            return redirect()->route('cradle.admin.dashboard');
        
        $this->page['title'] = "Admin Login";
        $this->page['meta']['description'] = 'Admin Login';
        return view('AdminView::admin.units.auth.login', $this->page);
    }

    protected function cradleRedirect($condition) {
        switch ($condition) {
            case 'login':
                return Cradle::config('project.redirect.admin_after_login'); 
                break;
            case 'logout':
                return Cradle::config('project.redirect.admin_after_logout'); 
                break;
            default:
                return 'cradle.admin.dashboard';
                break;
        }    
    }

    protected function loginRules()
    {   
        return [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ];
    }

    protected function guard()
    {
        return Auth::guard();
    }   

    public function username()
    {
        return 'email';
    }

}
