<?php

namespace Cradle\CradleAdmin\controllers\admin\bread;

use App\Http\Controllers\CradleController;
use Cradle\modules\bread\contracts\BreadInterface;
use Cradle\modules\bread\defaults\defaultTranslationBread;

class TranslationBreadController extends CradleController implements BreadInterface
{
   use defaultTranslationBread;
   
}
