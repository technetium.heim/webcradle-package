<?php

namespace Cradle\CradleAdmin\controllers\admin\bread;

use App\Http\Controllers\CradleController;
use Cradle\modules\bread\foundation\breadTrait;
use Cradle\modules\bread\contracts\BreadInterface;
use Cradle\modules\bread\defaults\defaultMenuItemBread;
use Cradle; 

class MenuItemBreadController extends CradleController implements BreadInterface
{
   use defaultMenuItemBread;
}