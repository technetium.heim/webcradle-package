<div class="sidebar">
    <div class="row">
        <div class="col-xs-12">
            @component('WCView::general.components.panels.main')
                @slot('title','Content')
                @slot('content')
                    <div class="list-group">
                        <a href="#info" class="list-group-item">General Info</a>
                        <a href="#follow" class="list-group-item">Follow Us</a>
                        <a href="#message" class="list-group-item">Message Us</a>

                    </div>
                @endslot
            @endcomponent
        </div>
    </div>
</div>