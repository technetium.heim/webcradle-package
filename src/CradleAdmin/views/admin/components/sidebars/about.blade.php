<div class="sidebar">
    <div class="row">
        <div class="col-xs-12">
            @component('WCView::general.components.panels.main')
                @slot('title','About')
                @slot('content')
                    @include('AdminView::admin.components.links.about')
                @endslot
            @endcomponent
        </div>
    </div>
</div>