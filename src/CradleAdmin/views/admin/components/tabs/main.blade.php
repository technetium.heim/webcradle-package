@component('WCView::general.components.tabs.main')
    @slot('content')
        @include('AdminView::admin.components.links.tab')
    @endslot
@endcomponent