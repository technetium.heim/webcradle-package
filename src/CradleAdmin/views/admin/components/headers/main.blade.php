@extends('WCView::general.components.headers.main')

@section('off-response','')

@section('header-content')
	<div class="row bg-admin">
		<div class="visible-xs header-btn pull-left">
			<a class="btn btn-header" onclick="openSidebar();">          
            	<i class="fa fa-fw fa-lg fa-bars text-white" aria-hidden="true"></i>
            </a>
		</div>
		<div class="col-xs-4 text-left">
			<div class="header-title pull-left padding-left">
				<span class="text-white text-nowrap">{{ config('app.name') }} Admin</span>
			</div>
		</div>
		<div class="col-xs-4 text-center">
		</div>
		<div class="col-xs-4 pull-right">
			<div class="pull-right">
			@if(Auth::check() && Auth::user()->hasRole('admin') )
				<form id="logout-form" action="{{ route('cradle.admin.logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}    
                </form>
				<ul class="la la-nav la-nav-h height-header" >
			      	<li class="dropdown">
			        	<a class="dropdown-toggle text-white" data-toggle="dropdown" href="#">
			        		<i class="fa fa-user fa-lg"></i>
			        		<span class="caret"></span>
			        	</a>
				        <ul class="dropdown-menu dropdown-menu-right">
				          	<li>
				          		<a class="btn btn-link" onclick="$('#logout-form').submit();">				    
			          				<i class="fa fa-sign-out fa-lg" aria-hidden="true"></i>
			         				Logout
				          		</a>
				          	</li>
				        </ul>
				    </li>
				</ul>
			@endif
			</div>
		</div>
	</div>
@endsection