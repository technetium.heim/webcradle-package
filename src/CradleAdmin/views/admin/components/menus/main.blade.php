@component('WCView::general.components.menus.main')
    @slot('content')
        @include('AdminView::admin.components.links.menu')

        @if(Auth::check())
            <form id="logout-form" action="{{ route('cradle.logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}    
            </form>
        @endif
 
    <!-- END -->
    @endslot
@endcomponent

