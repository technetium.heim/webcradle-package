@php
    if(!isset($menu_name)) { $menu_name = 'admin'; }
    
    use Cradle\modules\menu\models\Menu;
    use Cradle\modules\menu\models\MenuItem;

    $menu = Menu::where('name',$menu_name)->get();
    $menu_id = $menu[0]->id;

    $menu_items = MenuItem::where('menu_id',$menu_id)->orderBy('parent_id', 'asc')->orderBy('sort', 'asc')->get();

    foreach($menu_items as $i) {
        if($i['action'] == 'route') {
            $link = route($i['target']);
        }
        else {
            $link = $i['target'];
        }

        $route = $i['route'];
        $active = '';
        if(isset($route) && $route != '') {
            if (Request::is($route.'/*') || Request::is('pages/'.$route.'/*') || Request::is($route) || Request::is('pages/'.$route)) {
                if(isset($i['parent_id'])) {
                    $menus[$i['parent_id']]->collapse = '';
                }
                $active = 'text-primary';
            }
        }

        if(isset($i['parent_id'])) {
            $menus[$i['parent_id']]->sub[] = (object) array(
                'name' => $i['text'],
                'link' => $link,
                'active' => $active,
            );

        }
        else {
            $menus[$i['id']] = (object) array(
                'id' => $i['id'],
                'name' => $i['text'],
                'icon' => $i['icon'],
                'link' => $link,
                'sub' => [],
                'collapse' => 'collapse',
                'active' => $active,
            );
        }
    }

@endphp

<div class="sidebar-main responsive">
    <ul class="nav">
    @foreach($menus as $menu)
            <li>
                @if(!empty($menu->sub)) 
                    <a data-toggle="collapse" data-target="#{{ $menu->id }}">
                        <i class="fa fa-fw {{ $menu->icon }}"></i>
                        &nbsp;&nbsp;{{ $menu->name }}
                        @if (!empty($menu->sub)) 
                        <i class="fa fa-fw fa-angle-down pull-right"></i>
                        @endif
                    </a>
                @else
                    <a class="{{ $menu->active }}" href="{!! $menu->link !!}">
                        <i class="fa fa-fw {{ $menu->icon }}"></i>
                        &nbsp;&nbsp;{{ $menu->name }}
                    </a>
                @endif
                @if (!empty($menu->sub)) 
                    <ul id="{{ $menu->id }}" class="{{ $menu->collapse }}">
                        @foreach($menu->sub as $submenu)
                            <li>
                                <a class="{{ $submenu->active }}" href="{!! $submenu->link !!}">
                                    <i class="fa fa-fw fa-arrow-circle-right"></i> 
                                     &nbsp;&nbsp;
                                    {{ $submenu->name }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </li>
        @endforeach
        </li>
    </ul>
</div>