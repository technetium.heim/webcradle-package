<!-- layout -->
    @extends('AdminView::admin.layouts.template.white.left_column')

<!-- navbar -->
    @section('page-navbar')
    @endsection

<!-- column left -->
    @section('page-column-left')

    @endsection

<!-- column center -->
    @section('page-column-center')
        <div class="row">
            <div class="col-xs-12">
                <div class="padding border-bottom">
                    <span class="text-b text-lg text-black">Dashboard</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="padding">
                    @php
                        if(!isset($menu_name)) { $menu_name = 'admin'; }
                        use Cradle\modules\menu\models\Menu;
                        use Cradle\modules\menu\models\MenuItem;
                        $item = Menu::where('name',$menu_name)->first()->item()->get();
                        $button = [];
                        $tab = [];

                        foreach($item as $i) 
                        {
                            if(!isset($i->parent_id))
                            {
                                if($i->text != "Dashboard") {
                                    $button[$i->id] = (object) [
                                        "id" => $i->id,
                                        "text" => $i->text,
                                        "icon" => $i->icon,
                                        "action" =>  $i->action,
                                        "target" =>  $i->target,
                                        "sort" =>  $i->sort,
                                    ];
                                }
                                if(!isset($i->action))
                                {
                                    if(!isset($tab[$i->id])) 
                                    { 
                                        $tab[$i->id] = (object) [
                                            "id" => $i->id,
                                            "child" => [],
                                        ];
                                    }
                                }
                            }
                            else 
                            {
                                if(!isset($tab[$i->parent_id])) 
                                { 
                                    $tab[$i->parent_id] = (object) [
                                        "id" => $i->parent_id,
                                        "child" => [],
                                    ]; 
                                }
                                $tab[$i->parent_id]->child[] = (object) [
                                    "id" => $i->id,
                                    "text" => $i->text,
                                    "target" => $i->target,
                                ];
                            }    
                        }

                        function cmp($a, $b)
                        {
                            return $a->sort - $b->sort;
                        }
                        usort($button, "cmp");

                    @endphp
                    <div class="tab-content">
                        <div id="tab-dashboard" class="tab-pane fade in active">
                            @foreach($button as $b)
                                @if($b->action == "route")
                                    <div class="col-xs-4 col-sm-3 col-md-2 padding-xs">
                                        <a class="btn-block " href="{!! route($b->target) !!}">
                                            <div class="bg-primary rounded-sm padding text-center border">
                                                <i class="fa fa-fw fa-3x {{ $b->icon }}"></i>
                                                <br>{{ $b->text }}
                                            </div>
                                        </a>
                                    </div>
                                @else
                                    <div class="col-xs-4 col-sm-3 col-md-2 padding-xs">
                                        <a class="btn-block" data-toggle="tab" href="#tab-dashboard-{{ $b->id }}">
                                            <div class="bg-primary rounded-sm padding text-center border">
                                                <i class="fa fa-fw fa-3x {{ $b->icon }}"></i>
                                                <br>{{ $b->text }}
                                            </div>
                                        </a>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        @foreach($tab as $t)
                            <div id="tab-dashboard-{{ $t->id }}" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <a class="btn btn-link" data-toggle="tab" href="#tab-dashboard">< Back</a>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="panel border">
                                            <div class="panel-body">
                                                @foreach($t->child as $c)
                                                    <div>
                                                        <a href="{!! route($c->target) !!}" class="btn btn-link">{{ $c->text }}</a>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endsection

<!-- column right -->
    @section('page-column-right')
    @endsection 

<!-- modal -->
    @section('page-modal')
    @endsection

<!-- script-top -->
    @section('page-script-top')
    @endsection

<!-- script-bottom -->
    @section('page-script-bottom')
    @endsection