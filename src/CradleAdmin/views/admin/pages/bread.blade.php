<!-- layout -->
    @extends('AdminView::admin.layouts.template.white.left_column')

<!-- navbar -->
    @section('page-navbar')
    @endsection

<!-- column left -->
    @section('page-column-left')

    @endsection

<!-- column center -->
    @section('page-column-center')
        @component('WCView::general.components.contents.extend.blank-maxwidth')
            @slot('content')
                
                @include('BreadView::main')

            @endslot
        @endcomponent
    @endsection


<!-- column right -->
    @section('page-column-right')
    @endsection 

<!-- modal -->
    @section('page-modal')
    @endsection

<!-- script-top -->
    @section('page-script-top')
    @endsection

<!-- script-bottom -->
    @section('page-script-bottom')
    @endsection

