<!-- layout -->
    @extends('AdminView::admin.layouts.template.white.basic')

<!-- navbar -->
    @section('page-navbar')
    @endsection

<!-- column left -->
    @section('page-column-left')
    @endsection

<!-- column center -->
    @section('page-column-center')
        @component('WCView::general.components.contents.extend.blank')
            @slot('title','')
            @slot('content')
                @component('WCView::general.components.panels.main')
                @slot('title','Admin Login')
                @slot('content')
                    <div class="row padding text-left">
                        <form class="form-horizontal padding" role="form" method="POST" action="{{ route('cradle.admin.login.submit') }}">
                        {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-3 col-xs-4 col-form-label">Email</label>

                                <div class="col-md-9 col-xs-8">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-3 col-xs-4 col-form-label">Password</label>

                                <div class="col-md-9 col-xs-8">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-3">
                                    <div class="row">
                                        <div class="checkbox col-xs-6 col-md-6">
                                            <label>
                                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                            </label>
                                        </div>
                                        {{-- 
                                        <div class="col-xs-6 col-md-6 text-right">
                                            <a class="btn btn-link" href="{{ route('cradle.password.forget') }}">
                                                Forgot Password?
                                            </a>
                                        </div>
                                        --}}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-3">
                                    <button id="submit-login" type="submit" class="btn btn-primary btn-block">
                                        Login
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                @endslot
                @endcomponent
            @endslot
        @endcomponent
    @endsection


<!-- column right -->
    @section('page-column-right')
    @endsection 

<!-- modal -->
    @section('page-modal')
    @endsection

<!-- script-top -->
    @section('page-script-top')
    @endsection

<!-- script-bottom -->
    @section('page-script-bottom')
    @endsection


