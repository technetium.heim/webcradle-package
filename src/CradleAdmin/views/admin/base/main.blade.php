<!-- Layout -->
    @extends('WCView::general.base.main')

<!-- Page -->
    @section('main-sky')
        <!-- header -->
            @include('AdminView::admin.components.headers.main')

        <!-- tab -->
            {{--@include('AdminView::admin.components.tabs.main') --}}

        <!-- navbar -->
            @yield('page-navbar','')

        <!-- toolbar -->
    @endsection

    @section('main-ceiling')
        <div class="row">
            @if(session('status'))
                @php
                    switch (session('status')) {
                        case 'error':
                            $type = 'danger';
                            break;
                        default:
                            $type = 'success';
                            break;
                    }
                @endphp
                <div class="alert alert-{{ $type }}">
                    @if(count(session('message')) > 1)
                        @foreach(session('message') as $message)
                            <li>{!! $message !!}</li>
                        @endforeach
                    @else 
                        {!! session('message')[0] !!}
                    @endif
                </div>
            @endif
        </div>
    @endsection

    @section('main-column')
        
        <div class="row attach-sidebar master-admin">
            @if($column_left == 1)
                <div class="col-left row">                   
                    @yield('page-column-left','')
                    @yield('layout-column-left','')
                </div>
            @endif
            @if($column_left == 1 && $column_right == 1)
                <div class="col-center col-center-2-column row">
            @elseif($column_left == 1 || $column_right == 1)
                <div class="col-center col-center-1-column row">
            @else
                <div class="col-center col-center-0-column row">
            @endif
                    @yield('page-column-center','')
                </div>
            @if($column_right == 1)
                <div class="col-right row">
                    @yield('page-column-right','')
                    @yield('layout-column-right','')
                </div>
            @endif
        </div>
    @endsection

    @section('main-floor')
        <!-- footer -->
            {{--@include('AdminView::admin.components.footers.main')--}}
    @endsection

    @section('main-ground')
        <!-- mobile bottom navbar -->
    @endsection

<!-- Modal -->
    @section('modal')
        <!-- Specific--> 
            @yield('page-modal','')
    @endsection

<!-- Script -->
    @section('script-top')
        @yield('page-script-top','')
    @endsection

    @section('script-analytic')  
    @endsection

    @section('script-bottom') 
        @yield('page-script-bottom','')
    @endsection