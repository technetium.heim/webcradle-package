<!-- Base > Layout > View -->
<!-- This is admin layouts. -->

<!-- extended from base (include all the basic div only) -->
@extends('WCView::general.common.base')

<!-- Set up Page Sizing Class -->
<!-- mobile-sizing, pc-sizing  -->
@section('page-size','')

@section('main-middle-class','with-sidebar')

<!-- START CONTENT -->
@section('main-top')
<!-- This is where nav bar should be -->
    @component('WCView::general.components.navbars.nav-for-sidebar') <!-- use component  -->
        @slot('title', 'Admin DashBoard')
    @endcomponent

@endsection

@section('main-middle')
    @parent
    @component('AdminView::admin.menus.default-menu')
    @endcomponent
@endsection

@section('main-bottom')
<!-- This is where footer should go -->
@endsection


@section('child-modal')
<!-- All Modal Page is include here-->
    @yield('unique-child-modal')
@endsection
