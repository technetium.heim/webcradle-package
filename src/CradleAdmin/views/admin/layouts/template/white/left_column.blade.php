<!-- Layout -->
    @extends('AdminView::admin.base.main')

    @php
        $column_left = 1; // boolean
        $column_right = 0; // boolean
        $body_bg_color = 'white'; // any css color code / (null)
    @endphp

    @section('layout-column-left')
        @if ( Auth::check() && Auth::user()->hasRole('admin'))
            @include('AdminView::admin.components.menus.side')
        @endif     
    @endsection

    @section('layout-column-right')
    @endsection