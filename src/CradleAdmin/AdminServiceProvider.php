<?php

namespace Cradle\CradleAdmin;

use Illuminate\Support\ServiceProvider;
use Cradle\basic\supports\CradleServiceProviderTrait;

class AdminServiceProvider extends ServiceProvider
{
    use CradleServiceProviderTrait;
    // protected $defer = true;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {   
       
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadViewsFrom(__DIR__.'/views', 'AdminView');
        $this->publishCradle();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      
    }

    public function publishCradle() 
    {

    }

}
