<?php
namespace Cradle;

use Illuminate\Support\Facades\File;
use Crabon\Carbon;

class Version {
	
	protected $content;
	protected $path;

	public function setUpdatedToNow(){
		$this->content->last_updated = Carbon::now()->utc; 
	}

	public function checkAndCreateLocalVersion( $path )
	{
		if( ! File::exists( $path ) ){
			$json = json_encode( [
	            "name" => "WebCradle",
	            "version" =>  "",
	            "build" => "",
	            "published" => [
	            ],
	            "addons" =>[
	            ],
	        ], JSON_PRETTY_PRINT );
	          
	        File::put( $path , $json );
	    }
	}

	public function loadCradleVersion( $path = "" )
	{
		$path = $path? $path: $this->path = __DIR__.'/WCVersion.json';

		$this->content = json_decode( File::get( $path ) );
		$this->path = $path;
		return $this;
	}

	public function loadLocalVersion( $path )
	{		
		$this->content = json_decode( File::get( $path ) );
		$this->path = $path;
		return $this;
	}	

	public function getVersion()
	{
		return $this->content->{'version'};		
	}

	public function addVersion()
	{
		$this->setBuild( 0 );
		$this->setVersion( $this->getVersion() + 0.1 );
		$this->updateFile();
	}

	// For Cradle Version
	public function pushPublishTags( $group , array $tags )
	{
		foreach ($tags as $tag) {
    		$this->pushPublishTag( $group, $tag );
    	}
	}

	public function pushPublishTag( $group , $tag )
	{
		$this->addPublishGroup($group);

    	if ( !in_array($tag, $this->content->publish->$group ) ){
    		array_push( $this->content->publish->$group , $tag );
    	}
	}

	protected function addPublishGroup( $group )
	{
    	if (! array_key_exists( $group , $this->content->publish ) ){
            $this->content->publish->$group = [];
        }
    }

    public function getPublishList( $group ="" )
	{
		if ($group){
			return $this->content->publish->$group;
		}else{
			return $this->content->publish;					
		} 
	}
	// For Local
	public function getPublishedList( $group ="" )
	{
		if ($group){
			return $this->content->published->$group;
		}else{
			return $this->content->published;					
		} 
	}

	public function getPublishedAddons()
	{
		return $this->content->addons;		
	}

	public function pushPublishedAddon( $tag )
	{
		if(!in_array($tag , $this->content->addons)) {
			array_push($this->content->addons, $tag);
		}
	}

	public function setPublishedList (array $published)
	{
		$this->content->published =  $published;
	}

	// Shared Function
	public function setVersion( $version_input )
	{	
		$this->content->{'version'} = $version_input;
	}

	public function getBuild()
	{
		return $this->content->{'build'};
	}

	public function addBuild()
	{
		$this->setBuild( $this->getBuild() + 1 );
		$this->updateFile();
	}

	public function setBuild( $build_input )
	{	
		$this->content->{'build'} = $build_input;
	}

	public function updateFile()
	{
		File::put( $this->path , json_encode($this->content, JSON_PRETTY_PRINT ) );
		return 'Success Updated';
	}
}	