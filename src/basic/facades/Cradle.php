<?php 

namespace Cradle\basic\facades;

use Illuminate\Support\Facades\Facade;


class Cradle extends Facade 
{
	protected static function getFacadeAccessor()
	{
		return "cradle-basic";
	}

}