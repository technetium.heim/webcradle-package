<?php
namespace Cradle\basic\supports;

trait CradleServiceProviderTrait
{
	protected static $groups=[];
    protected static $localVersionPath;

    public static function getPublishTags() {
        return static::$groups;
    }

    public static function getLocalVersionPath() {
        return static::$localVersionPath;
    }

    protected function setLocalVersionPath( $path ){
    	static::$localVersionPath = $path;
    }

    protected function pushGroupList( $master, $group ){
    	
    	$this->addGroup($master);

    	if ( !in_array($group, static::$groups[$master] ) ){
    		array_push( static::$groups[$master] , $group );
    	}
    }

    protected function pushGroupsList( $master, array $groups_array ){
    	foreach ($groups_array as $group) {
    		$this->pushGroupList( $master, $group );
    	}
    }

    protected function addGroup( $master){
    	if (! array_key_exists( $master , static::$groups)) {
            static::$groups[$master] = [];
        }
    }

}