<?php
namespace Cradle\basic\supports;

use Cradle\Version; 
use Cradle\basic\CradleServiceProvider;
use Artisan;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Filesystem\Filesystem;
use League\Flysystem\Filesystem as Flysystem;
use League\Flysystem\Adapter\Local as LocalAdapter;
use League\Flysystem\MountManager;

trait CradleCommandsTrait
{
	protected $cradle_version;
	protected $local_version;	

	protected function getLocalVersionFile()
	{	
		$file = new Version;
		$path = CradleServiceProvider::getLocalVersionPath();

		// Create Local Version is not exist
		$file->checkAndCreateLocalVersion($path);

		// Load Current Version instance to Local Version 
		$file->loadLocalVersion( $path );
		return $file;
	}

	protected function getCradleVersionFile()
	{	
		$file = new Version;
		// Load Current Version instance to Local Version 
		$file->loadCradleVersion();
		return $file;
	}

	protected function runPublishGroups( array $groups, array $excludes=[], $force = false )
	{
		//get from list from version
		$published_groups = [];
		foreach($groups as $group) {
			$published_groups[$group] = $this->runPublishGroup( $group, $excludes , $force );
		}

		return $published_groups;
	}

	protected function runPublishGroup( $group, array $excludes=[], $force = false )
	{
		//get from list from version
		$group_tags = $this->cradle_version->getPublishList( $group );
		
		$published_tags = [];
		foreach($group_tags as $tag) {
			array_push( $published_tags , $this->runPublishTag( $tag , $excludes , $force) );
		}

		return $published_tags;
	}

	protected function runPublishTags( array $tags, array $excludes=[], $force = false )
	{
		//get from list from version		
		$published_tags = [];
		foreach($tags as $tag) {
			array_push( $published_tags , $this->runPublishTag( $tag , $excludes , $force) );		
		}

		return $published_tags;
	}

	protected function runPublishTag( $tag, array $excludes=[] , $force = false)
	{
		if( in_array( $tag , $excludes) ) return;
		
		Artisan::call('vendor:publish', [
        	'--force' => $force,
        	'--tag' => $tag,
        ]);	

		return $tag;
	}

	protected function askMigrate()
    {
        if ($this->confirm('Do you wish to RUN Migration?')) {  
            Artisan::call('migrate');
            $this->info('Complete Migration.');
        }   
    }

    protected function checkPublishFiles($tag)
    {
    	foreach ($this->pathsToPublish($tag) as $from => $to) {
            return $this->checkItem($from, $to);
        }
    }

    protected function checkItem($from, $to)
    {
        if ( File::isFile($from) ) {
            return $this->checkFile($from, $to);
        }elseif ( File::isDirectory($from)) {
            return $this->checkDirectory($from, $to);
        }

        $this->error("Can't locate path: <{$from}>");
    }

    protected function checkFile($file)
    {
        if ( File::exists($file) ) {
            return File::getTimestamp($file);
        }
    }

    protected function checkDirectory($from, $to)
    {
        return $this->checkManagedFiles(new MountManager([
            'from' => new Flysystem(new LocalAdapter($from)),
            'to' => new Flysystem(new LocalAdapter($to)),
        ]));
    }

    protected function checkManagedFiles($manager)
    {
        foreach ($manager->listContents('from://', true) as $file) {
            // if ($file['type'] === 'file' && (! $manager->has('to://'.$file['path']) )) {
            	// return checkFile('to://'.$file['path']);
        	return \Carbon\Carbon::createFromTimestamp($file['timestamp']);
                // $manager->put('to://'.$file['path'], $manager->read('from://'.$file['path']));
            // }
        }
    }






    protected function publishTag($tag)
    {
        foreach ($this->pathsToPublish($tag) as $from => $to) {
            $this->publishItem($from, $to);
        }

        $this->info('Publishing complete.');
    }

    protected function pathsToPublish($tag)
    {
        return CradleServiceProvider::pathsToPublish(
            "", $tag
        );
    }

}