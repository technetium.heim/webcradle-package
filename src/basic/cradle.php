<?php
namespace Cradle\basic;

use Auth;
use Route;
use Cradle\modules\dataset\models\Dataset;
use Cradle\modules\dataset\models\DatasetDescription;

class Cradle 

{

	public function display ()
	{
		return public_path();
	}


	public function assetPath($path)
	{
		$path = trim($path, '/');

		if ( file_exists(public_path($path) ) )
            return asset($path);
        else 
        {
			return url("../webcradle/src/basic/public/".$path);		
        }
 	}

 	public function controller_path($file, $group, $units ) 
 	{	
 		$file = trim($file, '/');
 		$group = trim($group, '/');
 		$units = trim($units, '/');

 		// if (is_dir(app_path('/Http/Controllers/vendor/cradle/'.$group.'/'.$component))) 
 		// {
 		// 	return 'App\Http\Controllers\vendor\cradle\\'.$group.'\\'.$component.'\\'.$file;
 		// }
 		// else 
 		// {
 		// 	if ($group == 'admin')
 		// 		return 'Cradle\\'.$group.'\controllers\\'.$file;
 		// 	else		
 		// 	return 'Cradle\basic\controllers\\'.$component.'\\'.$file;			
 		// }	

 		return 'App\Http\Controllers\\'.$group.'\\'.$units.'\\'.$file;
 	}

 	public function avatarPath($filename)
	{
		if (!Auth::check()) $filename = 'default.png';
		return url("uploads/avatars/".$filename);
 	}

 	public function avatarThumbPath($filename)
	{	
		if (Auth::check()) $filename = 'default.png';
		return url("uploads/avatars/thumb/".$filename);
 	}

 	public function uploadPath($filename)
	{	
		$filename = trim($filename, '/');
		return url("uploads/".$filename);
 	}

 	public function routesBread($item, $controller, $doorbell="", $group_key = "" ) {	
 		
 		if( $group_key =="" ){
 			$urlpath = 	$doorbell.'/bread/'.$item;
 			Route::get($urlpath.'/index', $controller.'@index')->name('bread.'.$item.'.index');
			Route::get($urlpath.'/browse', $controller.'@browse')->name('bread.'.$item.'.browse');
			Route::get($urlpath.'/read/{id?}', $controller.'@read')->name('bread.'.$item.'.read');
			Route::post($urlpath.'/edit', $controller.'@edit')->name('bread.'.$item.'.edit');
			Route::post($urlpath.'/add', $controller.'@add')->name('bread.'.$item.'.add');
			Route::post($urlpath.'/delete', $controller.'@delete')->name('bread.'.$item.'.delete');
 		}else{
 			$urlpath = 	$doorbell.'/'.$group_key.'/bread/'.$item;
 			Route::get($urlpath.'/index', $controller.'@index')->name($group_key.'.bread.'.$item.'.index');
			Route::get($urlpath.'/browse', $controller.'@browse')->name($group_key.'.bread.'.$item.'.browse');
			Route::get($urlpath.'/read/{id?}', $controller.'@read')->name($group_key.'.bread.'.$item.'.read');
			Route::post($urlpath.'/edit', $controller.'@edit')->name($group_key.'.bread.'.$item.'.edit');
			Route::post($urlpath.'/add', $controller.'@add')->name($group_key.'.bread.'.$item.'.add');
			Route::post($urlpath.'/delete', $controller.'@delete')->name($group_key.'.bread.'.$item.'.delete');
 		}

 	}

 	public function routesSocialite($controller) {	
 		if (config('cradleConfig.socialite') == 'enable')	{
 			Route::get('/redirect/{provider}', $controller.'@redirect')->name('redirect.social');
			Route::get('/callback/{provider}', $controller.'@callback')->name('callback.social');
		}
 	}

 	public function routesUserDashboard() {	
 		// PROFILE ROUTE
		$DashboardController =  'user\dashboard\DashboardController';
		$ProfileController =  'user\dashboard\ProfileController';
		$AccountController =  'user\dashboard\AccountController';

		Route::get('/dashboard', $DashboardController.'@index')->name('cradle.dashboard');

		Route::get('/profile/show', $ProfileController.'@showProfilePage')->name('cradle.profile.show');
		Route::get('/profile/edit', $ProfileController.'@showProfileEditPage')->name('cradle.profile.edit');
		Route::post('/profile/save', $ProfileController.'@saveProfile')->name('cradle.profile.save');
		Route::post('/profile/avatar/upload', $ProfileController.'@uploadAvatar')->name('cradle.profile.avatar.upload');
		Route::post('/profile/avatar/remove', $ProfileController.'@removeAvatar')->name('cradle.profile.avatar.remove');
		Route::get('/profile/avatar', $ProfileController.'@showProfileAvatarPage')->name('cradle.profile.avatar');	

		Route::get('/account/edit', $AccountController.'@showAccountEditPage')->name('cradle.account.edit');
		Route::get('/account/delete', $AccountController.'@showAccountDeletePage')->name('cradle.account.delete');
		Route::get('/account/connect', $AccountController.'@showAccountConnectPage')->name('cradle.account.connect');
		Route::post('/account/password/update', $AccountController.'@updatePassword')->name('cradle.account.password.update');
		Route::post('/account/email/update', $AccountController.'@updateEmail')->name('cradle.account.email.update');
		Route::post('/account/delete', $AccountController.'@delete')->name('cradle.account.delete.submit');
 	}

 	//return array with value and name
 	public function datasets($key) {

  		// to add language filter after
 		$datasets = Dataset::where('key', $key)->orderBy('sort')->get();

 		$data = [];
 		foreach ($datasets as $dataset) {
 			$row  = (object)[
 				'value' => $dataset->value,
 				'name'	=> DatasetDescription::where('dataset_id',$dataset->id)->first()->name,
 			];

 			array_push($data, $row);
 		}

 		return $data;

 	}

 	public function datasetName($key, $value= null) {

  		// to add language filter after
  		if($value != null) {	
 			$data = Dataset::where('key', $key)
 						->where('value', $value)->first()
 						->descriptions()->where('language_id', 1)->first()
 						->name;

 			return $data;
 		}
 	}

 	public function config( $key ){
 		if( config($key) != "") {
 			return config($key);
 		}

 		$array = explode('.', $key );
	 	unset($array[0]);
	 	$key = implode('.', $array );
		return config( 'cradleConfig.'.$key );
 	}
}
