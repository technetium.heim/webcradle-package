<?php 
namespace Cradle\basic\console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Artisan;
use Cradle\basic\supports\CradleCommandsTrait;


class UpdateCradleFontCommand extends Command {

    use CradleCommandsTrait;

    protected $signature = 'cradle:wcfont-update';
    protected $description = 'Force update wcfont files from package to project';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        if ($this->confirm('Do you wish to UPDATE wcfont?')) { 
            
            // run force update for update file
            $force_tags = ['wcfont-update'];
            $this->runPublishTags( $force_tags, [] , true );

            $this->info('wcfont Updated.');
        }
    }
}