<?php 
namespace Cradle\basic\console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Artisan;

class FixErrorCommand extends Command {

    protected $name = 'cradle:fix';
    protected $description = 'Fix SQL error';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $this->info('This will force replace App/Providers/AppServiceProvider.php');
        if ($this->confirm('Do you wish to continue?')) {   
            Artisan::call('vendor:publish', [
                '--force' => true,
                '--tag' => 'fix-error',
                ]);

            $this->info('SQL error fixed.');
        }        
    }

}