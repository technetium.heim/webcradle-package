<?php 
namespace Cradle\basic\console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Artisan;
use Cradle\basic\supports\CradleCommandsTrait;


class UpdateCradleScssUpdateCommand extends Command {

    use CradleCommandsTrait;

    protected $signature = 'cradle:scss-update';
    protected $description = 'Force update WebCradle SCSS file into Project > resources > assets > sass.';
    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        if ($this->confirm('This will replace the SCSS file in Project Folder. Do you wish to UPDATE?')) { 
            $tags = ['scss-update'];
            // run force publish
            $this->runPublishTags( $tags, [], true );
            $this->info('SCSS files is Replaced.');
        }
    }
}