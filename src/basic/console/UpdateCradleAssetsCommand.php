<?php 
namespace Cradle\basic\console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Artisan;
use Cradle\basic\supports\CradleCommandsTrait;


class UpdateCradleAssetsCommand extends Command {

    use CradleCommandsTrait;

    protected $signature = 'cradle:assets-update';
    protected $description = 'Force update js & css files from package to project';
    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $this->info( '[0] New - Copy only new js files from package to project.');
        $this->info( '[1] Replace - Force replace all js files from package to project. (not recommended)');

        $selected = $this->choice('Select options:', [
            'New', 'Replace'
        ]);

        $tags = ['assets-update'];
        if( $selected == 'New'){
            // run normal publish 
            $this->runPublishTags( $tags, [] );
            $this->info('New js and css files is Published.');
        }else{
            // run force publish
            $this->runPublishTags( $tags, [], true );
            $this->info('js & css files is Replaced.');
        }
    }
}