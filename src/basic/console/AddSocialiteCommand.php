<?php 
namespace Cradle\basic\console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Artisan;

class AddSocialiteCommand extends Command {

    protected $name = 'cradle:add-socialite';
    protected $description = 'Add Additional Files for socialite';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
 
        Artisan::call('vendor:publish', [
            '--tag' => 'add-socialite',
            ]);
        
       
        $this->info('Migration File => create_social_accounts_table.php');
        $this->info('Model File => SocialAccount.php');
        $this->info('Provider File => SocialAccountServiceProvider.php');
        $this->info('Additional Files for Socialite created.');
         $this->info('Please run-> composer require laravel/socialite');
    }

}