<?php 
namespace Cradle\basic\console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Artisan;
use Cradle\basic\supports\CradleCommandsTrait;


class UpdateCradleCommand extends Command {

    use CradleCommandsTrait;

    protected $signature = 'cradle:update';
    protected $description = 'Update Web Cradle Basic & Modules';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        Artisan::call('cradle:update-publish');
        
        if ($this->confirm('Do you wish to UPDATE?')) { 
            
            // run normal publish for init file
            $tags = ['basic-init', 'user-once', 'module-once'];
            $this->runPublishTags( $tags, [] );
            
            // run force update for update file
            $force_tags = ['basic-update', 'module-update'];
            $this->runPublishTags( $force_tags, [] , true );

            $this->info('Web Cradle Updated.');
        }

        if ($this->confirm('Do you wish to RUN Migration for cradle:init?')) {  
            Artisan::call('migrate');
            $this->info('Web Cradle Init Complete Migration.');
        } 


        if ($this->confirm('Do you wish to RUN Seeder(Dataset) for cradle:init?')) {  
            Artisan::call('db:seed', [
                '--class' => 'DatasetTableSeeder',
                ]);
            $this->info('Web Cradle Init Complete Seeder.');
        }

    }

  
}