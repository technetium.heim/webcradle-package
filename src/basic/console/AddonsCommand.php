<?php 
namespace Cradle\basic\console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Artisan;
use Illuminate\Support\Facades\File;
use Cradle\basic\supports\CradleCommandsTrait;

class AddonsCommand extends Command {

    use CradleCommandsTrait;
    
    protected $signature = 'cradle:addon {addon_name?}';
    protected $description = 'Add Web Cradle Addons';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {   
        Artisan::call('cradle:update-publish');
        
        $this->cradle_version =  $this->getCradleVersionFile();
        $this->local_version = $this->getLocalVersionFile();

        $addon_name = $this->argument('addon_name');

        if( !isset($addon_name) ) {

            $this->info( 'Addons Available:');
            print_r( $this->cradle_version->getPublishList('addons') );

            $this->info( '  Type \'cancel\' to cancel.');

            $addon_name = $this->ask('Please Enter Addons Package Name'); 
        }


        // if 1st time, use force.
        

       
        switch ($addon_name) {
            case 'cancel':
                
                break;
            case 'addons-user-profile':
                $published = $this->runPublishTag( $addon_name, [] , true);
                $this->askMigrate();
                $this->local_version->pushPublishedAddon( $published );
                $this->info( 'Addon->'.$addon_name.' Added');             
                $this->info('Addons list updated');             
                break;
            default:
                if( in_array($addon_name, $this->cradle_version->getPublishList('addons'))){
                    $published = $this->runPublishTag( $addon_name, [], true );
                    $this->local_version->pushPushiledAddon( $published );     
                }else {
                    $this->info('Error: Addons ['.$addon_name.'] Not Available.');
                    $this->info( 'Addon->'.$addon_name.' Added');   
                    $this->info('Addons list updated');         
                }
                break;
        }

        $this->local_version->updateFile();
    }
}