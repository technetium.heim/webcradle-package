<?php 
namespace Cradle\basic\console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Artisan;
use Cradle\basic\supports\CradleCommandsTrait;

class ForceUpdateCradleCommand extends Command {

    use CradleCommandsTrait;

    protected $signature = 'cradle:force-update {tag?}';
    protected $description = 'Force Update by tag.';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $tag = $this->argument('tag');
        
        if(!isset($tag)){
            $this->info( 'Force Updates Tags Available:');
            $this->info( '  once    -> Init files, should only copy once. DO NOT run this if not necessary.');   
            $this->info( '  view    -> View files for WebCradle Basic, Users , Modules.');    
            $this->info( '  asset   -> Asset files for CSS, JS compilation');
            $this->info( '  public  -> Public files for Init (CSS, JS)');
            $this->info( '  cancel  -> Cancel.');

            $tag = $this->ask('Please Enter Tag Name'); 
        } 

        switch ($tag) {
            case 'once':
                $runForceUpdate = ['basic-once', 'user-once', 'admin-once', 'module-once'];

                break;
            case 'view':
                $runForceUpdate = ['basic-view', 'user-view', 'admin-view', 'module-view'];

                break;
            case 'asset':
                $runForceUpdate = ['basic-asset'];

                break;

            case 'public':
                $runForceUpdate = ['basic-public'];

                break;
            case 'cancel':
                $runForceUpdate = [];

                break;
            
            default:
                $runForceUpdate = [];
                $this->info('Error: Tag ['.$tag.'] Not Available.');     
                break;
        }


        if (!empty( $runForceUpdate) ) {
            print_r( $this->runPublishTags( $runForceUpdate, [], true ) );
        }        
    }

}