<?php 
namespace Cradle\basic\console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Artisan;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use Cradle\basic\supports\CradleCommandsTrait;

class InstallCommand extends Command {

    use CradleCommandsTrait;

    protected $name = 'cradle:init';
    protected $description = 'Initilize Web Cradle Basic';

    public function __construct()
    {
        parent::__construct();
    }


    public function fire()
    {
        Artisan::call('cradle:update-publish');

        $published = [];
        $this->cradle_version =  $this->getCradleVersionFile();
        $is_initialized = false;

        $this->info('This will re-initialize all web cradle files.');
        if ($this->confirm('Do you wish to continue?')) { 

            $init_groups = ['basic','users','modules'];
            $excludes = [];
            
            $published = $this->runPublishGroups( $init_groups, $excludes, true );
 
            $this->info('Web Cradle Basic Initialized.');
            $this->info('Web Cradle Module: Role Initialized.');
            $this->info('Web Cradle Module: Translation Initialized.');
            $this->info('Web Cradle User: user Initialized.');
            $this->info('Web Cradle User: admin Initialized.');
            $this->info('Web Cradle Module: BREAD Initialized.');
            $this->info('Web Cradle Module: Dataset Initialized.');
            $is_initialized = true;
        }

        if ($this->confirm('Do you wish to RUN Migration for cradle:init?')) {  
            Artisan::call('migrate');
            $this->info('Web Cradle Init Complete Migration.');
        } 


        if ($this->confirm('Do you wish to RUN Seeder(Dataset) for cradle:init?')) {  
            Artisan::call('db:seed', [
                '--class' => 'DatasetTableSeeder',
                ]);
            $this->info('Web Cradle Init Complete Seeder.');
        }
    }
}