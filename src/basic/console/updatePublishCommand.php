<?php 
namespace Cradle\basic\console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Artisan;
use Cradle\basic\supports\CradleCommandsTrait;

class UpdatePublishCommand extends Command {

    use CradleCommandsTrait;

    protected $name = 'cradle:update-publish';
    protected $description = 'Update all publish list tags into Cradle Version file';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $this->cradle_version =  $this->getCradleVersionFile();

        $CradleServiceProviders = [
            \Cradle\basic\CradleServiceProvider::class, 
            \Cradle\CradleUser\UserServiceProvider::class,
            \Cradle\CradleAdmin\AdminServiceProvider::class,
            \Cradle\modules\ModuleServiceProvider::class,
        ];

        foreach( $CradleServiceProviders as $sp) { 
            foreach( $sp::getPublishTags() as $key => $value ) {
                $this->cradle_version->pushPublishTags( $key , $value );
            }
        }
        $this->cradle_version->updateFile();
        $this->info( 'Cradle Version Publish List Updated.' );
    }
}