<?php 
namespace Cradle\basic\console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Artisan;
use App\Models\User;
use Hash;

class MakeAdminCommand extends Command {

    protected $name = 'cradle:make-admin';
    protected $description = 'Make New Admin User for Web Cradle';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $email = $this->ask('Please Enter Admin Login Email:');        
        $password = $this->secret('Please Enter Admin Login Password');
        $name = $this->ask('Please Enter Admin Name:');

        if ($this->confirm('Do you wish to Create A new Admin User?')) {   
            
            $admin = new User;
            $admin->name = $name;
            $admin->email = $email;
            $admin->password = Hash::make($password);
            $success = $admin->save();


            $admin->attachRole( $admin->roles()->getModel()->where('name','admin')->first() );


            if($success){
                $this->info('New Admin created.');
                $this->info('Email:'. $email);
            } else {
                $this->error('Failed to create new Admin!');
            }
        }
    }

}