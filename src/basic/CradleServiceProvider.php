<?php

namespace Cradle\basic;

use Illuminate\Support\ServiceProvider;
use Cradle\basic\CradleCommands;
use Cradle\basic\CradleMiddlewares;
use Cradle\basic\supports\CradleServiceProviderTrait;

class CradleServiceProvider extends ServiceProvider
{
    use CradleCommands;
    use CradleMiddlewares;
    use CradleServiceProviderTrait;

    public function registerCradleUserGroup () {
        
        $this->app->register(
            'Cradle\CradleUser\UserServiceProvider'
        );

        $this->app->register(
            'Cradle\CradleAdmin\AdminServiceProvider'
        );

    }

    public function registerCradleComponent () {

        $loader = \Illuminate\Foundation\AliasLoader::getInstance();

        // Image Intervention
        $this->app->register(
               \Intervention\Image\ImageServiceProvider::class
            );

        $loader->alias('Image', \Intervention\Image\Facades\Image::class);



        // Socialite
        // if (config('cradleConfig.socialite_fb') == 'enable') {
        //     $this->app->register(
        //        'Laravel\Socialite\SocialiteServiceProvider'
        //     );        
        //     $loader->alias('Socialite', 'Laravel\Socialite\Facades\Socialite');
        // }

    }

    // protected $defer = true;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {      
        // $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadViewsFrom(__DIR__.'/views', 'WCView');
        
        $this->publishCradle();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {   
        // register WC command line (in Cradle\basic\CradleCommands.php)
        $this->registerCommands();

        // register WC middlewares line (in Cradle\basic\CradleMiddlewares.php)
        $this->registerMiddlewares();

        // bind this
        $this->app->bind('cradle-basic',function() {
            return new Cradle();
        });


        $this->registerCradleUserGroup();
        $this->registerCradleComponent();
        $this->registerCradleModule();
    }

    protected function publishCradle() 
    {   
        // load basic migration instead of publish
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        
        $this->publishes([
        __DIR__.'/init/config' => base_path('/config'),       
        __DIR__.'/init/models' => app_path('/Models'),          // Copy AppCradleModel.php to Project
        __DIR__.'/init/controllers' => app_path('/Http/Controllers'),  // Copy CradleController to Project
        __DIR__.'/init/exceptions' => app_path('/Exceptions'),  // Copy & replace Exception (Force)
        __DIR__.'/init/public' => public_path(''),
        ],"basic-init");

        $this->publishes([        
        __DIR__.'/init-update/assets' => resource_path('/assets'),        // Copy general base to Project 
        __DIR__.'/init-update/config' => base_path('/config'),
        ],"basic-update");

        // Only use for update WC FONTS
        $this->publishes([        
        __DIR__.'/init/public/fonts/wcfont.eot' =>  public_path('/fonts/wcfont.eot'),
        __DIR__.'/init/public/fonts/wcfont.ttf' =>  public_path('/fonts/wcfont.ttf'), 
        __DIR__.'/init/public/fonts/wcfont.woff' =>  public_path('/fonts/wcfont.woff'), 
        __DIR__.'/init/public/fonts/wcfont.svg' =>  public_path('/fonts/wcfont.svg'), 
        __DIR__.'/init/public/css/wcfont.css' => base_path('/css/wcfont.css'),
        ],"wcfont-update");
        
        // Update assets
        $this->publishes([        
        __DIR__.'/init/public/js' => public_path('/js'),
        __DIR__.'/init/public/css' => public_path('/css'),
        ],"assets-update");

        $this->publishes([        
        __DIR__.'/init-update/assets/sass/_10_wc_library_variables.scss' =>  resource_path('/assets/sass/_10_wc_library_variables.scss'),
        __DIR__.'/init-update/assets/sass/_20_wc_website_variables.scss' =>  resource_path('/assets/sass/_20_wc_website_variables.scss'),
        __DIR__.'/init-update/assets/sass/_30_wc_other_variables.scss' =>  resource_path('/assets/sass/_30_wc_other_variables.scss'),
        __DIR__.'/init-update/assets/sass/wc.scss' =>  resource_path('/assets/sass/wc.scss'),
        __DIR__.'/init-update/assets/sass/app.scss' =>  resource_path('/assets/sass/app.scss'),
        ],"scss-update");

        // Publish fix SQL error
        $this->publishes([
        __DIR__.'/fix' => app_path('/Providers'),
        ],"fix-error");

        // For adding into Web Cradle Version File
        $this->pushGroupsList('basic', ['basic-init','basic-update'] );

        $this->setLocalVersionPath( base_path('/cradle.json') );
    }

    protected function registerCradleModule () {

        $this->app->register(
            'Cradle\modules\ModuleServiceProvider'
        );

        // $this->app->register(
        //     'Cradle\addons\AddonServiceProvider'
        // );

    }

}

