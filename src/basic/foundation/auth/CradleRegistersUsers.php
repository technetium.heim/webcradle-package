<?php

namespace Cradle\basic\foundation\auth;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

trait CradleRegistersUsers
{
    use RegistersUsers;

    protected function validator ($request)
    {
        return Validator::make($request, $this->registerRules());
    }

    public function register(Request $request)
    {
        $v = $this->validator($request->all()); 
        if ($request->expectsJson()) {
            //Validate using ajax 
            if ($v->fails()) return response()->json($v->errors());
        }else {
            $v->validate();
        }

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        $this->cradleHint()->authMessage($request, 'register', $this->guard()->user()->name);

        if ($request->expectsJson()) {
            $response['status'] = "success";
            if ($this->cradleRedirect('register') != '') 
                $response['redirect'] = route($this->cradleRedirect('register'));    

            return response()->json($response);
        }

        if ($this->cradleRedirect('register') != '') 
            return redirect()->route($this->cradleRedirect('register'));

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }
}

