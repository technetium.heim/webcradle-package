<?php

namespace Cradle\basic\foundation\auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use Illuminate\Support\Facades\Lang;

trait CradleAuthenticatesUsers
{
    // This trait used Laravel default auth trait.
    use AuthenticatesUsers;

    protected function validator($request)
    {   
        return Validator::make($request, $this->loginRules());        
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();
        $this->clearLoginAttempts($request);

        $this->cradleHint()->authMessage($request, 'login', $this->guard()->user()->name);

        if ($request->expectsJson()) {
             $response['status'] = "success";

            if ($this->cradleRedirect('login') != '') 
                $response['redirect'] = route($this->cradleRedirect('login'));    

            return response()->json($response);
            // return response()->json($errors, 200);
        }
        
        if ($this->cradleRedirect('login') != '') 
            return redirect()->route($this->cradleRedirect('login'));

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());    
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed'), 'password' => ""];

        if ($request->expectsJson()) {
            return response()->json($errors, 200);
        }

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
        $this->cradleHint()->authMessage($request, 'logout');   

        if ($this->cradleRedirect('logout') != '') {
            
            return redirect()->route($this->cradleRedirect('logout'));
        }

        return redirect()->route('cradle.home');
    }

    protected function sendLockoutResponse(Request $request)
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        $message = Lang::get('auth.throttle', ['seconds' => $seconds]);

        $errors = [$this->username() => $message];

        if ($request->expectsJson()) {
            return response()->json($errors, 200);
        }

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

     public function login(Request $request)
    {

        $v = $this->validator($request->all()); 
        if ($request->expectsJson()) {
            //Validate using ajax 
            if ($v->fails()) return response()->json($v->errors());
        }else {
            $v->validate();
        }

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
}