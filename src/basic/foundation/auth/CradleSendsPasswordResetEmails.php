<?php

namespace Cradle\basic\foundation\auth;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

trait CradleSendsPasswordResetEmails
{
    use SendsPasswordResetEmails;
}