<?php

namespace Cradle\basic\foundation\auth;

use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Auth;

trait CradleResetsPasswords
{
    use ResetsPasswords;

    protected function sendResetResponse($response)
    {

        $this->cradleHint()->authMessage(request(), 'login', $this->guard()->user()->name); 
        return redirect()->route($this->cradleRedirect('reset'));
    }
}