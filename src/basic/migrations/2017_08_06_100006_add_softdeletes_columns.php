<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddSoftdeletesColumns extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('roles', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('permissions', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('datasets', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('dataset_descriptions', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });

        Schema::table('roles', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });

        Schema::table('permissions', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });

        Schema::table('datasets', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });

        Schema::table('dataset_descriptions', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
    }

}
