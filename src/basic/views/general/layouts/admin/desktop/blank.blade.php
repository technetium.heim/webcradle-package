@extends('WCView::general.grounds.primary')

@push('styles')
  <style>
    html, body {
      overflow: hidden;
    }
    .background {
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      height: calc(100vh);
      width: calc(100vw);
      opacity: 0.7;
    }
    .content {
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      z-index: 2;
      padding-bottom: 70px; /* to solve iphone browser issue */
    }
  </style>
@endpush

@section('header')
@endsection

@section('body')
  <div id="body">
    <div id="subheader">@yield('subheader','')</div>
    <div class="width-full scrollable-y hidden-scrollbar @yield('content_class','')" style="height: @yield('content_height','calc(100vh)');">
      @yield('content','')
    </div>
  </div>
@endsection

@push('scripts')
@endpush