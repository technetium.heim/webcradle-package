@extends('WCView::general.grounds.primary')

@push('styles')
  <style>
    html, body {
      overflow: hidden;
    }
    #header {
      height: 50px;
    }
    #menu-content, #modal-menu-content {
      height: calc(100vh - 50px);
      width: 280px;
      background-color: #fafafa;
      padding-top:0; /* to nullify .scrollable-y */
      margin-top:0; /* to nullify .scrollable-y */
      padding-bottom:100px;
    }
    #modal-menu {
      position: absolute;
      left:0;
      z-index:2;
    }
    #modal-menu-content {
      position: absolute;
      top:0;
      left:0;
      z-index:2;
    }
    #modal-menu-backdrop {
      height: calc(100vh - 50px);
      width: calc(100vw);
      position: absolute;
      top:0;
      left:0;
      background-color: black;
      opacity:0.5;
      z-index:1;
    }
    #body {
      width: calc(100vw);
      display: inline-block;
    }
    @media only screen and (min-width: 768px) {
      #body.with-menu {
        max-width: calc(100vw - 280px);
      }
    }
  </style>
@endpush

@section('header')
  <div id="header" class="row hidden-print bg-primary-darkest">
    <div class="col-xs-12">
      <div class="pull-left">
        <a class="text-white hidden-xs" onclick="toggleMenu();"><i class="icon-50 material-icons">menu</i></a>
        <a class="text-white visible-xs" onclick="toggleModalMenu();"><i class="icon-50 material-icons">menu</i></a>
      </div>
      <div class="pull-left">
        <a class="text-white" href="@if(isset($group_key)) {{route( $group_key.'.user.dashboard')}} @else {{route('dashboard')}} @endif">
          <div class="text-white text-helvetica text-bold line-height-50 text-18 text-line-1 padding-h">{{ env('APP_NAME') }}</div>
        </a>
      </div>
    </div>
  </div>
@endsection

@section('body')
  <!-- BOC: menu -->
    <div id="modal-menu" class="hidden-sm hidden-md hidden-lg hidden-xl" style="display: none;">
      <div id="modal-menu-content" class="border-right scrollable-y hidden-scrollbar">@yield('menu','')</div>
      <div id="modal-menu-backdrop" onclick="toggleModalMenu();"></div>
    </div>
    <div id="menu" class="pull-left">
      <div id="menu-content" class="hidden-xs border-right scrollable-y hidden-scrollbar">@yield('menu','')</div>
    </div>
  <!-- EOC -->
  <div id="body" class="with-menu pull-right">
    <div id="subheader">@yield('subheader','')</div>
    <div class="width-full scrollable-y hidden-scrollbar @yield('content_class','')" style="height: @yield('content_height','calc(100vh - 150px)');">
      @yield('content','')
    </div>
  </div>
@endsection

@push('scripts')
  <script>
    function toggleMenu() {
      $('#menu').toggle();
      $('#body').toggleClass('with-menu');
    }
    function toggleModalMenu() {
      $('#modal-menu').toggle();
    }
  </script>
@endpush