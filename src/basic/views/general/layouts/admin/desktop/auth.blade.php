@extends('WCView::general.grounds.primary')

@push('styles')
  <style>
    html, body {
      overflow: hidden;
    }
    body {
      background-image: url("{{ asset('images/background-auth.jpg') }}");
      background-size: cover;
      background-position: center; 
      background-repeat: no-repeat;
    }
    .background {
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      height: calc(100vh);
      width: calc(100vw);
      opacity: 0.7;
    }
    .content {
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      z-index: 2;
      padding-bottom: 70px; /* to solve iphone browser issue */
    }
  </style>
@endpush

@section('header')
@endsection

@section('body')
  <div>
    <div class="bg-primary-darkest background"></div>
    <div class="content height-screen scrollable-y hidden-scrollbar">
      <!-- BOC: header -->
        <div class='padding-lg-top text-bold text-white text-24 text-helvetica'>{{ config('app.name') }}</div>
        <div class="margin-lg-bottom text-white">{{ config('app.description') }}</div>
      <!-- EOC -->
      <!-- BOC: content -->
        @yield('content','')
      <!-- EOC -->
      <!-- BOC: copyright -->
        <div class="padding-lg-top line-height-50 text-white text-12 opacity-80">Powered by <b>{{config('app.author')}}</b></div>
      <!-- EOC -->
    </div>
  </div>
@endsection

@push('scripts')
@endpush