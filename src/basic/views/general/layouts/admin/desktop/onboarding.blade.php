@extends('WCView::general.grounds.primary')

@push('styles')
  <style>
    html, body {
      overflow: hidden;
    }
    .background {
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      height: calc(100vh);
      width: calc(100vw);
      opacity: 0.7;
    }
    .content {
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      z-index: 2;
      padding-bottom: 70px; /* to solve iphone browser issue */
    }
  </style>
@endpush

@section('header')
  <div id="header" class="row hidden-print bg-primary-darkest">
    <div class="col-xs-12 text-left">
      <a class="text-white" href="{{URL::to('/dashboard')}}">
        <div class="text-white text-helvetica text-bold line-height-50 text-18 text-line-1 padding-h">{{ env('APP_NAME') }}</div>
      </a>
    </div>
  </div>
@endsection

@section('body')
  <div id="body">
    <div id="subheader">@yield('subheader','')</div>
    <div class="width-full bg-null scrollable-y hidden-scrollbar @yield('content_class','')" style="height: @yield('content_height','calc(100vh - 50px)');">
      @yield('content','')
    </div>
  </div>
@endsection

@push('scripts')
@endpush