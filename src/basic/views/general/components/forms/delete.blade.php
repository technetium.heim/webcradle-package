<div class="modal modal-bottom" role="dialog" id="modal-form-delete">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<span class="modal-title">Delete {{ $item_name }}</span>
			</div>
			<div class="modal-body">
				<form class="hidden" method="POST">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}
					<input type="hidden" name="id"/>
					<input type="hidden" name="name"/>
				</form>
				Are you sure you want to delete <b class="target"></b>?
			</div>
			<div class="modal-footer">
				<button onclick="deleteDB();" type="button" class="btn btn-danger" data-dismiss="modal">Delete</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

<script>
	function deleteDB() {
		var id = $('#modal-form-delete input[name=id]').val();
		$('#modal-form-delete form').attr('action', '{{ $item_name_sc_plural }}/'+id);
		$('#modal-form-delete form').submit();
	}

	$('#modal-form-delete').on('show.bs.modal', function(e) {
		$('#modal-form-delete .target').html($('#modal-form-delete input[name=name]').val());
	})
</script>