<div class="modal modal-bottom" role="dialog" id="modal-form-add">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<span class="modal-title">New {{ $item_name }}</span>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" method="POST">
					{{ csrf_field() }}
					{{ method_field('POST') }}
					@yield('modal_form')
				</form>
			</div>
			<div class="modal-footer">
				<button onclick="addDB();" type="button" class="btn btn-primary" data-dismiss="modal">Create</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script>
	function addDB() {
		$('#modal-form-add form').attr('action', '{{ $item_name_sc_plural }}');
		$('#modal-form-add form').submit();
	}
</script>