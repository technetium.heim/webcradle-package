<div class="modal modal-bottom" role="dialog" id="modal-form-edit">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<span class="modal-title">Edit {{ $item_name }}</span>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" method="POST">
					{{ csrf_field() }}
					{{ method_field('PATCH') }}
					@yield('modal_form')
				</form>
			</div>
			<div class="modal-footer">
				<button onclick="updateDB();" type="button" class="btn btn-primary" data-dismiss="modal">Update</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script>
	function readDB() {
		var id = $('#modal-form-edit input[name=id]').val();

		$.ajax({
			type: "GET",
			url: "{{ $item_name_sc_plural }}/"+id+"/read",
			dataType: "json",
			success: function(response) {
				//fill in form input
				$('#modal-form-edit input').each(function(index) {
					if(typeof response[$(this).attr('name')] !== 'undefined') {
						$(this).val(response[$(this).attr('name')]);
					}
				});
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(xhr.status);
			}
		});
	}

	function updateDB() {
		var id = $('#modal-form-edit input[name=id]').val();
		$('#modal-form-edit form').attr('action', '{{ $item_name_sc_plural }}/'+id);
		$('#modal-form-edit form').submit();
	}

	$('#modal-form-edit').on('show.bs.modal', function(e) {
		readDB();
	})
</script>