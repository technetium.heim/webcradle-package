<style>
	.btn-submit > .btn-submit-loop {
		display: none;
	}
	.btn-submit > .btn-submit-text {
		display: inline-block;
	}
	.btn-submit.loading {
		pointer-events: none;
	}
	.btn-submit.loading > .btn-submit-loop {
		display: inline-block;
	}
	.btn-submit.loading > .btn-submit-text {
		display: none;
	}
</style>
<!-- BOC: prepare params -->
	@php
		if(!isset($id)) { $id = ''; }
		if(!isset($class)) { $class = ''; }
		if(!isset($text)) { $text = ''; }
		if(!isset($action)) { $action = ''; }
		if(!isset($target)) { $target = ''; }
		if(!isset($route)) { $route = ''; }
		if(!isset($disabled)) { $disabled = false; }
		if(!isset($height)) { $height = 50; }
	@endphp
<!-- EOC -->
<!-- BOC: action -->
	@php
		switch($action) {
			case 'uri':
				$action = 'href="'.$target.'"';
				break;
			case 'modal':
				$action = ' data-toggle="modal" data-dismiss="modal" data-target="'.$target.'"';
				break;
			case 'script':
				$action = 'onclick="$(this).addClass(\'loading\'); '.$target.'"';
				break;
			case 'dropdown':
				$action = 'data-toggle="collapse" data-target=".menu-sub-'.$text.'" onclick="$(\'.collapse.in\').collapse(\'hide\')"';
				break;
			default:
				$action = '';
		}
	@endphp
<!-- EOC -->
<!-- BOC: print -->
	<a
    id="{{$id}}" 
    class='btn-submit {{$class}} line-height-{{$height}} height-{{$height}}' 
    {!! $action !!}
  >
  	<span class="btn-submit-text">{{$text}}</span>
    <span class="btn-submit-loop"><i class="fa fa-spin"><i class="icon-{{$height}} material-icons fa-flip-horizontal">loop</i></i></span>
	</a>
<!-- EOC -->