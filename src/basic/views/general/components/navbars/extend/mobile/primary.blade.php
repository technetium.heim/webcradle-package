@extends('WCView::general.components.navbars.main')

@php
	$position = 1;
	$type = 21;
	$color = 'primary';
	$class = 'navbar-mobile'; 
	$center_align = 'center';
@endphp