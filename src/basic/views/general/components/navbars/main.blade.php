@php
	//default
		if(!isset($color)) { $color = 'white'; }
		if(!isset($class)) { $class = ''; }
		if(!isset($position)) { $position = 1; }
	//overall
		if(!isset($custom)) { $custom = ''; }
	//left
		if(!isset($left_icon)) { $left_icon = ''; }
		if(!isset($left_text)) { $left_text = ''; }
		if(!isset($left_action)) { $left_action = ''; }
		if(!isset($left_target)) { $left_target = ''; }
	//center
		if(!isset($center_align)) { $center_align = 'left'; }

		switch($center_align) {
			case 'left':
				$center_align = 'text-left';
				break;

			case 'right':
				$center_align = 'text-right';
				break;

			case 'center':
				$center_align = 'text-center';
				break;

			case 'centre':
				$center_align = 'text-center';
				break;

			default:
				$center_align = '';
		}

		if(!isset($center_text)) { $center_text = $title; }
	//right
		if(!isset($right_icon)) { $right_icon = ''; }
		if(!isset($right_text)) { $right_text = ''; }
		if(!isset($right_action)) { $right_action = ''; }
		if(!isset($right_target)) { $right_target = ''; }
	
	//type
		if(!isset($type)) {
			if($custom != '') {
				$type = 0;
			}
			else if($left_icon != '' && $right_icon != '') {
				$type = 21;
			}
			else if($left_icon != '' || $right_icon != '') {
				$type = 20;
			}
			else if($left_text != '' || $right_text != '') {
				$type = 30;
			}
			else {
				$type = 10;
			}
		}

	//code
		switch($left_action) {
			case 'script':
				$left_code = 'onclick="'.$left_target.'"';
				break;

			case 'back':
				$left_code = 'onclick="window.history.go(-1); return false;"';
				break;

			case 'uri':
				$left_code = 'href="'.$left_target.'"';
				break;

			case 'modal':
				$left_code = 'data-toggle="modal" data-target="'.$left_target.'"';
				break;

			case 'tab':
				$left_code = 'data-toggle="tab" data-target="'.$left_target.'"';
				break;

			case 'dismiss':
				if($left_target != '') {
					$left_code = 'data-dismiss="modal" data-toggle="modal" data-target="'.$left_target.'"';
				}
				else {
					$left_code = 'data-dismiss="modal"';
				}
				break;

			default:
				$left_code = '';
		}

		switch($right_action) {
			case 'script':
				$right_code = 'onclick="'.$right_target.'"';
				break;

			case 'uri':
				$right_code = 'href="'.$right_target.'"';
				break;

			case 'modal':
				$right_code = 'data-toggle="modal" data-target="'.$right_target.'"';
				break;

			case 'dismiss':
				if($right_target != '') {
					$right_code = 'data-dismiss="modal" data-toggle="modal" data-target="'.$right_target.'"';
				}
				else {
					$right_code = 'data-dismiss="modal"';
				}
				break;

			default:
				$right_code = '';
		}

	//content
		//left
			if($left_icon != '') {
				$left_content = '<a class="btn" '.$left_code.'><i class="fa fa-fw fa-lg '.$left_icon.'"></i></a>';
			}
			else if($left_text != '') {
				$left_content = '<a class="btn" '.$left_code.'>'.$left_text.'</a>';
			}
			else if($type >= 20 && $type < 30) {
				if($left_action == 'back') {
					$left_content = '<a class="btn" '.$left_code.'><i class="fa fa-fw fa-lg fa-long-arrow-left"></i></a>';
				}
				else {
					$left_content = '<a class="btn" data-dismiss="modal" data-toggle="modal" data-target="#menu"><i class="fa fa-fw fa-lg fa-bars"></i></a>';
				}
			}
			else {
				$left_content = '';
			}
		//center
			$center_content = $center_text;

		//right
			if($right_icon != '') {
				$right_content = '<a class="btn" '.$right_code.'><i class="fa fa-fw fa-lg '.$right_icon.'"></i></a>';
			}
			else if($right_text != '') {
				$right_content = '<a class="btn" '.$right_code.'>'.$right_text.'</a>';
			}
			else {
				$right_content = '';
			}
@endphp

<div class="navbar {{ $class }} navbar-{{ $color }} navbar-position-{{ $position }} navbar-type-{{ $type }}">
	<div class="row">
		<div class="col-xs-12">
			@if($type == 0)
				{{ $custom }}
			@elseif($type == 10)
				<div class="row">
					<div class="col-xs-12">
						<div class="navbar-title text-center">
							<span class="text-line-1">{!! $center_content !!}</span>
						</div>
					</div>
				</div>
			@else
				@if($type == 30)
					<div class="row">
				@endif

				<!-- START: left -->
					@if($type >= 20 && $type < 30)
						<div class="navbar-btn pull-left">
							{!! $left_content !!}
						</div>
					@elseif($type == 30)
						<div class="col-xs-3 text-left">
							<div class="navbar-link">
								{!! $left_content !!}
							</div>
						</div>
					@endif
				<!-- END -->

				<!-- START: center -->
					@if($type >= 20 && $type < 30)
						<div class="navbar-title pull-left {{ $center_align }}">
							<span class="text-line-1">{!! $center_content !!}</span>
						</div>
					@elseif($type == 30)
						<div class="col-xs-6 text-center">
							<div class="navbar-title">
								<span class="text-line-1">{!! $center_content !!}</span>
							</div>
						</div>
					@endif
				<!-- END -->
				
				<!-- START: right -->
					@if($type >= 20 && $type < 30)
						<div class="navbar-btn pull-right">
							{!! $right_content !!}
						</div>
					@elseif($type == 30)
						<div class="col-xs-3 text-right">
							<div class="navbar-link">
								{!! $right_content !!}
							</div>
						</div>
					@endif
				<!-- END -->

				@if($type == 30)
					</div>
				@endif
			@endif
		</div>
	</div>
</div>
<div class="navbar-backwall {{ $class }}">
</div>