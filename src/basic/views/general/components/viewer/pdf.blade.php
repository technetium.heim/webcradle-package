@php
	if(!isset($width)) { $width = '100%'; }
	if(!isset($height)) { $height = '500px'; }
@endphp

<iframe src="{{ $file_src }}" width="{{ $width }}" height="{{ $height }}"></iframe>