<div class="list-group">
	@foreach($links as $link)
		<!-- default -->
			@php
				if(!isset($link->type)) { $link->type = ''; }
				if(!isset($link->account)) { $link->account = ''; }
				if(!isset($link->icon)) { $link->icon = ''; }
				if(!isset($link->text)) { $link->text = ''; }
				if(!isset($link->action)) { $link->action = ''; }
				if(!isset($link->target)) { $link->target = ''; }
				if(!isset($link->route)) { $link->route = ''; }
			@endphp

		<!-- active -->
			@php
				if(isset($link->route) && $link->route != '') {
					if (Request::is($link->route.'/*') || Request::is('pages/'.$link->route.'/*') || Request::is($link->route) || Request::is('pages/'.$link->route)) {
						$active = 'active';
					}
					else {
						$active = '';
					}
				}
				else {
					$active = '';
				}
			@endphp

		<!-- account -->
			@if(isset($link->account))
				@if($link->account == 'guest')
	            	@php $print = Auth::guest()?  true:false;  @endphp
				@elseif($link->account == 'user')
					@php $print = Auth::check()?  true:false;  @endphp
				@elseif($link->account != '')
					@php $print = Auth::guard('$link->account')->check()?  true:false;  @endphp
				@else
		            @php $print = true; @endphp
				@endif
			@else
	            @php $print = true; @endphp
			@endif

		<!-- type -->
			@if($print == true && $link->type == 'section')
	            <hr>
	            @php $print = false; @endphp
	        @endif

        <!-- print -->
			@if($print == true)
				@if($link->action == 'uri')
					<a href="{{ $link->target }}" class="list-group-item {{ $active}}">
				@elseif($link->action == 'modal')
					<a class="list-group-item {{ $active}}" data-toggle="modal" data-dismiss="modal" data-target="{{ $link->target }}">
				@elseif($link->action == 'script')
					<a class="list-group-item {{ $active}}" onclick="{{ $link->target }}">
				@else
					<a class="list-group-item {{ $active}}">
				@endif
					@if(isset($link->icon) && $link->icon != '')
						<span class="icon">
							<i class="fa fa-fw fa-lg {{ $link->icon }}"></i>
							<i class="fa fa-fw"></i>
						</span>
					@endif
					<span>{{ $link->text }}</span>
				</a>
			@endif
	@endforeach
</div>