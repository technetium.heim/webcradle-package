@extends('WCView::general.components.contents.main')

@php
	$content_group_type = '';
	$content_type = 'card';
	$content_max_width = 'sm';
	$content_bg_color = 'transparent';
@endphp