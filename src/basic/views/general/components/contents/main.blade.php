@php
	if(!isset($content_group_type)) { $content_group_type = ''; }
	if(!isset($content_max_width)) { $content_max_width = ''; }
	if(!isset($content_bg_color)) { $content_bg_color = 'transparent'; }
	if(!isset($content_type)) { $content_type = ''; }
	if(!isset($content_title)) { if(!isset($title)) { $content_title = ''; } else { $content_title = $title; } }
	if(!isset($content_action)) { $content_action = ''; }
	if(!isset($content_top)) { $content_top = ''; }
	if(!isset($content_middle)) { if(!isset($content)) { $content_middle = ''; } else { $content_middle = $content; } }
	if(!isset($content_bottom)) { $content_bottom = ''; }
	if(!isset($content_height)) { $content_height = ''; }
	if(!isset($content_padding)) { $content_padding = false; }
	if(!isset($content_class)) { $content_class = ''; }
@endphp
@php
	if($content_padding) { $class_padding = 'padding'; } else { $class_padding = ''; }
@endphp

<div class="content-group scrollable-y hidden-scrollbar" style="height:{{ $content_height }}; background-color:{{ $content_bg_color }};">
	@if($content_group_type == 'advance')
		<div class="row">
			<div class="col-xs-12">
				<div class="content-loading-wrapper hidden">
		            <div class="row">
		                <div class="col-xs-12">
		                	<div class="content-loading">
								<div class="row">
									<div class="col-xs-12">
					                    <i class="content-loading-icon fa fa-fw fa-5x fa-spin fa-spinner"></i>
					                </div>
					                <div class="col-xs-12">
					                	<span class="content-loading-message">Loading ...</span>
					                </div>
					            </div>
				        	</div>
						</div>  
		            </div>
	        	</div>
			</div>
			<div class="col-xs-12">
		        <div class="content-error-wrapper hidden">
		            <div class="row">
		                <div class="col-xs-12">
		                	<div class="content-error">
					            <div class="row">
					                <div class="col-xs-12 margin-bottom">
					                    <i class="content-error-icon fa fa-fw fa-5x fa-frown-o text-danger"></i>
					                </div>
					                <div class="col-xs-12">
					                    <span class="content-error-title text-b">Something Went Wrong</span>
					                </div>
					                <div class="col-xs-12 margin-bottom">
					                    <span class="content-error-message">Try again. If it still doesn't work, let us know.</span>
					                </div>
					                <div class="col-xs-12 margin-xs-bottom">
					                    <a class="btn btn-block btn-block-fixed btn-primary" onclick="location.reload();">Refresh page</a>
					                </div>
					                <div class="col-xs-12">
					                    <a class="btn btn-block btn-block-fixed btn-default">Let Us Know</a>
					                </div>
					            </div>
					        </div>
		                </div>
		            </div>
		        </div>
			</div>
			<div class="col-xs-12">
		        <div class="content-empty-wrapper hidden">
		            <div class="row">
		                <div class="col-xs-12">
		                	<div class="content-empty">
					            <div class="row">
					                <div class="col-xs-12 margin-bottom">
					                    <i class="content-error-icon fa fa-fw fa-5x fa-meh-o text-warning"></i>
					                </div>
					                <div class="col-xs-12">
					                    <span class="content-error-title text-b">Oops, no result</span>
					                </div>
					                <div class="col-xs-12 margin-bottom">
					                    <span class="content-error-message"></span>
					                </div>
					            </div>
					        </div>
		                </div>
		            </div>
		        </div>
			</div>
		</div>
	@endif
	<div class="col-xs-12 full-screen-height">
        <div class="content-wrapper content-max-width-{{ $content_max_width }}">
    		<div class="row">
				<div class="col-xs-12">
		            <div class="content content-{{ $content_type }}">
		        		<div class="row">
		        			@if($content_title != '')
								<div class="col-xs-12">
						            <div class="content-top hidden-xs">
						            	<div class="row">
											<div class="col-xs-6">
												<h1>{{ $content_title }}</h1>
											</div>
											<div class="col-xs-6">
												{{ $content_action }}
											</div>
						            	</div>
						            	<div class="row">
											<div class="col-xs-12">
												{{ $content_top }}
											</div>
						            	</div>
										<hr>
						            </div>
								</div>
							@endif
							<div class="col-xs-12">
					            <div class="content-middle {{ $content_class }} {{ $class_padding }}">
					            	<div class="row">
										<div class="col-xs-12">
											{{ $content_middle }}
										</div>
					            	</div>
					            </div>
							</div>
							<div class="col-xs-12">
					            <div class="content-bottom">
					            	<div class="row">
										<div class="col-xs-12">
											{{ $content_bottom }}
										</div>
					            	</div>
					            </div>
							</div>
		            	</div>
		            </div>
				</div>
        	</div>
        </div>
	</div>
</div>