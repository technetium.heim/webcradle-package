<!-- BOC: style -->
	<style>
		.menu-list-item:hover {
	 		background-color: #f1f1f1;
		}
		.menu-list-item.active {
	    pointer-events: none;
	    background-color: #ccc;
	  }
		.menu-list-item.active a {
	    color: #333 !important;
	  }
	  .menu-list-item.menu-list-item-dropdown.active {
	    pointer-events: auto; /* to nullify active class effect */
	  }
		.menu-list-item.disabled {
	    pointer-events: none;
	  }
		.menu-list-item.disabled a {
	    color: #ddd !important;
	  }
	</style>
<!-- EOC -->
<!-- BOC: list -->
	@foreach($links as $link)
		<!-- BOC: prepare params -->
			@php
				if(!isset($link->class)) { $link->class = ''; }
				if(!isset($link->account)) { $link->account = ''; }
				if(!isset($link->icon)) { $link->icon = ''; }
				if(!isset($link->text)) { $link->text = ''; }
				if(!isset($link->action)) { $link->action = ''; }
				if(!isset($link->target)) { $link->target = ''; }
				if(!isset($link->route)) { $link->route = ''; }
				if(!isset($link->subs)) { $link->subs = []; }
				if(!isset($link->disabled)) { $link->disabled = false; }
			@endphp
		<!-- EOC -->
		<!-- BOC: check if active -->
			@php
				if(isset($link->route) && $link->route != '') {
					if(is_array($link->route)){
						$active = '';
						foreach( $link->route as $r){
							if (Request::is($r.'/*') || Request::is('pages/'.$r.'/*') || Request::is($r) || Request::is('pages/'.$r)) {
								$active = 'active';
							}
						}
					}else{
						if (Request::is($link->route.'/*') || Request::is('pages/'.$link->route.'/*') || Request::is($link->route) || Request::is('pages/'.$link->route)) {
							$active = 'active';
						}
						else {
							$active = '';
						}
					}
				}
				else {
					$active = '';
				}
			@endphp
		<!-- EOC -->
		<!-- BOC: check if disabled -->
			@php
				(isset($link->disabled) && $link->disabled == true) ? $disabled = 'disabled' : $disabled = '';
			@endphp
		<!-- EOC -->
		<!-- BOC: check sub if selected for collapse option -->
			@if (!empty($link->subs) ) 
				@php
					$have_selected = false; 
				@endphp
				@foreach($link->subs as $sub)
					@php
						if(isset($sub->route) && $sub->route != '') {
							if(is_array($sub->route)){
								foreach( $sub->route as $r){
									if (Request::is($r.'/*') || Request::is('pages/'.$r.'/*') || Request::is($r) || Request::is('pages/'.$r)) {
										$have_selected = 'active';
									}
								}
							}else{

								if (Request::is($sub->route.'/*') || Request::is('pages/'.$sub->route.'/*') || Request::is($sub->route) || Request::is('pages/'.$sub->route)) {
									$have_selected = true; 
								}
							}
						}
					@endphp
				@endforeach
				@php
					if($have_selected == true){
						$collapse = "in";
						$active = 'active';
					}
					else {
						$collapse = '';
					}
				@endphp
			@endif
		<!-- EOC -->
		<!-- BOC: check account if to be displayed -->
			@if(isset($link->account))
				@if($link->account == 'guest')
					@php $print = Auth::guest()?  true:false;  @endphp
				@elseif($link->account == 'user')
					@php $print = Auth::check()?  true:false;  @endphp
				@elseif($link->account != '')
					@php $print = Auth::guard('$link->account')->check()?  true:false;  @endphp
				@else
					@php $print = true; @endphp
				@endif
			@else
				@php $print = true; @endphp
			@endif
		<!-- EOC -->
		<!-- BOC: action -->
			@php
				switch($link->action) {
					case 'uri':
						$action = 'href="'.$link->target.'"';
						break;
					case 'modal':
						$action = ' data-toggle="modal" data-dismiss="modal" data-target="'.$link->target.'"';
						break;
					case 'script':
						$action = 'onclick="'.$link->target.'"';
						break;
					case 'dropdown':
						$action = 'data-toggle="collapse" data-target=".menu-sub-'.$link->text.'" onclick="$(\'.collapse.in\').collapse(\'hide\')"';
						break;
					default:
						$action = '';
				}
			@endphp
		<!-- EOC -->
		<!-- BOC: print -->
			@if($print == true)
				<div class="menu-list-item row height-50 menu-list-item-{{$link->action}} {{ $link->class }} {{$disabled}} {{$active}}">
					<a class="row text-primary" {!! $action !!}>
						<div class="pull-left"><i class="icon-50 material-icons">{{ $link->icon }}</i></div>
						<div class="pull-left textbox-50">{{ $link-> text}}</div>
						@if($link->action == 'dropdown')
							<div class="pull-right"><i class="icon-50 material-icons">arrow_drop_down</i></div>
						@endif
					</a>
				</div>
			@endif
		<!-- EOC -->
		<!-- BOC: sub -->
			@if(!empty($link->subs))
				<div id="" class="menu-sub-{{ $link->text }} collapse {{$collapse}}">
					@foreach($link->subs as $sub)
						<!-- BOC: prepare params -->
							@php
								if(!isset($sub->icon)) { $sub->icon = ''; }
								if(!isset($sub->text)) { $sub->text = ''; }
								if(!isset($sub->action)) { $sub->action = ''; }
								if(!isset($sub->target)) { $sub->target = ''; }
								if(!isset($sub->route)) { $sub->route = ''; }
								if(!isset($sub->disabled)) { $sub->disabled = false; }
							@endphp
						<!-- EOC -->
						<!-- BOC: check if active -->
							@php
								if(isset($sub->route) && $sub->route != '') {
									$active = '';
									if(is_array($sub->route)) {
										foreach( $sub->route as $r) {
											if (Request::is($r.'/*') || Request::is('pages/'.$r.'/*') || Request::is($r) || Request::is('pages/'.$r)) {
												$active = 'active';
											}
										}
									}
									else{
										if (Request::is($sub->route.'/*') || Request::is('pages/'.$sub->route.'/*') || Request::is($sub->route) || Request::is('pages/'.$sub->route)) {
											$active = 'active';
										}
										else {
											$active = '';
										}
									}
								}
								else {
									$active = '';
								}
							@endphp
						<!-- EOC -->
						<!-- BOC: check if disabled -->
							@php
								(isset($sub->disabled) && $sub->disabled == true) ? $disabled = 'disabled' : $disabled = '';
							@endphp
						<!-- EOC -->
						<!-- BOC: action -->
							@php
								switch($sub->action) {
									case 'uri':
										$action = 'href="'.$sub->target.'"';
										break;
									case 'modal':
										$action = ' data-toggle="modal" data-dismiss="modal" data-target="'.$sub->target.'"';
										break;
									case 'script':
										$action = 'onclick="'.$sub->target.'"';
										break;
									default:
										$action = '';
								}
							@endphp
						<!-- EOC -->
						<!-- BOC: print -->
							<div class="menu-list-item row height-50 {{$disabled}} {{$active}}">
								<a class="row text-primary" {!! $action !!}>
									<div class="pull-left"><i class="icon-50 material-icons">subdirectory_arrow_right</i></div>
									<div class="pull-left textbox-50">{{ $sub->text }}</div>
								</a>
							</div>
						<!-- EOC -->
					@endforeach
				</div>
			@endif
		<!-- EOC -->
	@endforeach
<!-- EOC -->