<div class="footer hidden-xs">
	<div class="row">
		<div class="col-xs-12">
			@yield('footer_content','')
		</div>
	</div>
</div>
<div class="footer-backwall hidden-xs">
</div>