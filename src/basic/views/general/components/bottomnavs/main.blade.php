<div class="bottomnav">
	<div class="row">
		<div class="col-xs-12">
			@foreach($links as $link)
                <!-- active -->
                @php
                	$action = $link->action;
                	$target = $link->target;

                    if(isset($link->route) && $link->route != '') {
                        if (Request::is($link->route.'/*') || Request::is('pages/'.$link->route.'/*') || Request::is($link->route) || Request::is('pages/'.$link->route)) {
                            $active = 'active';
                        }
                        else {
                            $active = '';
                        }
                    }
                    else {
                        $active = '';
                    }

                    switch($action) {
						case 'script':
							$code = 'onclick="'.$target.'"';
							break;

						case 'back':
							$code = 'onclick="window.history.go(-1); return false;"';
							break;

						case 'uri':
							$code = 'href="'.$target.'"';
							break;

						case 'modal':
							$code = 'data-toggle="modal" data-target="'.$target.'"';
							break;

						case 'dismiss':
							if($target != '') {
								$code = 'data-dismiss="modal" data-toggle="modal" data-target="'.$target.'"';
							}
							else {
								$code = 'data-dismiss="modal"';
							}
							break;

						default:
							$code = '';
					}

					if(count($links) == 1) {
						$col = 'col-xs-12';
					}
					else if(count($links) == 2) {
						$col = 'col-xs-6';
					}
					else if(count($links) == 3) {
						$col = 'col-xs-4';
					}
					else if(count($links) == 4) {
						$col = 'col-xs-3';
					}
					else if(count($links) == 5) {
						$col = 'col-xs-5ths';
					}
					else if(count($links) >= 6) {
						$col = 'col-xs-2';
					}
				@endphp
				<div class="{{ $col }}">
					<a class="{{ $active }}" {!! $code !!}>
                        <i class="fa fa-fw fa-lg {{ $link->icon }}"></i>
						<br>
                        <span>{{ $link->text }}</span>
					</a>
				</div>
			@endforeach
		</div>
	</div>
</div>