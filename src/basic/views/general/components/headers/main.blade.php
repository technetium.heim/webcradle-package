<div class="header border-bottom hidden-print">
	<div class="row">
		<div class="col-xs-12">
			@yield('header-content','')
		</div>
	</div>
</div>
<div class="header-backwall hidden-print">
</div>