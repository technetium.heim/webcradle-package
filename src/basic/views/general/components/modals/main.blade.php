@php
	if(!isset($modal_class)) { $modal_class = ''; }
	if(!isset($modal_type)) { $modal_type = ''; }
	if(!isset($modal_content_class)) { $modal_content_class = ''; }
	if(!isset($modal_header)) { $modal_header = ''; }
	if(!isset($modal_body)) { $modal_body = ''; }
	if(!isset($modal_footer)) { $modal_footer = ''; }
	if(!isset($modal_script)) { $modal_script = ''; }
@endphp

<!-- Modal -->
<div id="{{ $modal_id }}" class="modal {{ $modal_class }} {{ $modal_type }} " role="dialog" data-backdrop="false">
  	<div class="modal-dialog">
  		<div class="modal-back-dismiss" data-dismiss="modal"></div>
	    <div class="modal-content {{ $modal_content_class }}">
	    	@if($modal_header != '')
		        <div class="modal-header">
			         {{ $modal_header }}
		        </div>
		    @endif

	        <div class="modal-body">
		         {{ $modal_body }}
	        </div>
	        
	        @if($modal_footer != '')
		        <div class="modal-footer">
			         {{ $modal_footer }}
		        </div>
	        @endif
	   	</div>
	</div>
</div>

{{ $modal_script }}