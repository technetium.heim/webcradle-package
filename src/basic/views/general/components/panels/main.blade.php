@php
	if(!isset($title)) { $title = ''; }
	if(!isset($content)) { $content = ''; }
@endphp

<div class="padding">
	<div class="panel panel-default">
		<!-- header -->
			@if($title != '') 
				<div class="panel-heading">
					{{ $title }}
				</div>
			@endif
			
		<!-- body -->
			<div class="panel-body">
				{{ $content }}
			</div>
	</div>
</div>