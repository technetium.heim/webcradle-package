<!-- Base > Layout > View -->
<!-- This is base, whole website using the same base -->

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">        
    <!-- BOC: general -->
      <title>@if(isset($title) && $title != config('app.name')) {{$title}} - @endif{{ config('app.name') }}</title>
      <meta name="title" content="@if(isset($title) && $title != config('app.name')) {{$title}} - @endif{{ config('app.name') }}">
      <meta name="description" content="{{ config('app.description') }}">
      <meta name="keywords" content="{{ config('app.name') }}">
      <meta name="author" content="{{ config('app.author') }}">
    <!-- EOC -->
    <!-- BOC: icon -->
      <meta name="mobile-web-app-capable" content="yes">
      <link rel="shortcut icon" href="{{ asset('images/icon-favicon.png') }}">
      <link rel="icon" sizes="192x192" href="{{ asset('icon-mobile.png') }}">
      <link rel="icon" sizes="128x128" href="{{ asset('icon-mobile.png') }}">
      <link rel="apple-touch-icon" href="{{ asset('icon-mobile.png') }}"/>
      <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('icon-mobile.png') }}"/>
      <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('icon-mobile.png') }}"/>
    <!-- EOC -->
    <!-- BOC: CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- EOC -->

    <!-- Private Styles -->
    <link href="{{ Cradle::assetPath('css/app.css') }}" rel="stylesheet">
    <link href="{{ Cradle::assetPath('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ Cradle::assetPath('css/wcfont.css') }}" rel="stylesheet">
    <link href="{{ Cradle::assetPath('css/wc.css') }}" rel="stylesheet">
    <link href="{{ Cradle::assetPath('css/jquery.bootgrid.min.css') }}" rel="stylesheet">
    <link href="{{ Cradle::assetPath('css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ Cradle::assetPath('css/bootstrap-select.min.css') }}" rel="stylesheet">
    <link href="{{ Cradle::assetPath('css/bootstrap-toggle.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Scripts -->
    <script>
      window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
      ]) !!};
    </script>

    <script src="{{ Cradle::assetPath('js/app.js') }}"></script>  
    <script src="{{ Cradle::assetPath('js/moment-with-locales.min.js') }}"></script>
    <script src="{{ Cradle::assetPath('js/jquery.bootgrid.min.js') }}"></script>
    <script src="{{ Cradle::assetPath('js/jquery.bootgrid.fa.min.js') }}"></script>
    <script src="{{ Cradle::assetPath('js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ Cradle::assetPath('js/bootstrap-select.min.js') }}"></script>
    <script src="{{ Cradle::assetPath('js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ Cradle::assetPath('js/wc.js') }}"></script>
    <script src="{{ asset('js/project.js') }}"></script>
    
    @yield('script-top','')    
    @yield('script-analytic','')  
  </head>
  <body style="background-color:{{ $body_bg_color }};">
    <div class="alert" hidden>
      @yield('alert','')
    </div>

    @if(isset($errors) && $errors->any())
      <div class="pop pop-hint" onclick="$(this).hide();" hidden>
        <div>{{ $errors->first() }}</div>
      </div>
    @endif

    <div id="hint" class="pop pop-hint" onclick="$(this).hide();" hidden>
    </div>

    <div class="container-fluid">
      <!-- Placeholder for Page Design-->
      <div class="main-wrapper">
        <div class="main-sky">@yield('main-sky','')</div>
        <div class="main-ceiling">@yield('main-ceiling','')</div>
        <div class="main-column">@yield('main-column','')</div>
        <div class="main-floor">@yield('main-floor','')</div>
        <div class="main-ground">@yield('main-ground','')</div>
      </div>
      <!-- Placeholder for modal include -->
    </div>
    <div class="modal-container">
      @yield('modal','')
      @stack('addmodals')  
    </div>  
  </body>
</html>

@yield('script-bottom','')
@stack('addscripts')
<script> 
  $(document).ready(function() {
    @if(session('hint'))
      showHint("{{ session('hint') }}");
    @endif      
  });
</script>

@if(session('open-modal'))
  <script> 
    $('{{ session('open-modal') }}').modal('show');
  </script>
@endif