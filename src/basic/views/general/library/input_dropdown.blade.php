@php
	// default value
	
	if( !isset($id) ) $id = "";
	if( !isset($name) ) $name = "";
	if( !isset($show_label) ) $show_label = false;
	if( !isset($label) ) $label = $name;
	if( !isset($required) ) $required = false;
	if( !isset($locked) ) $locked = false;
	if( !isset($no_select) ) $no_select = true;
	if( !isset($no_select_text) ) $no_select_text = 'Select your option';	
	if( !isset($selected) ) $selected = "";					
	if( !isset($select_class) ) $select_class = "";			
	if( !isset($option_class) ) $option_class = "";			
	if( !isset($options_data) ) $options_data = [];			
	if( !isset($show_error) ) $show_error = true;		
	if( !isset($data_arrangement) ) $data_arrangement = 'value_name_as_key'; // 'value_name_as_key', 'value_as_key'
	if( !isset($info) ) $info = "";

	$this_errors = [];
	// error (only for important)
	if( $name == null ) array_push($this_errors , "No Input Name (name)" );
	if( empty($options_data) )  array_push($this_errors , "No Data (options_data[])" );
	
	// Process Data
	$required = $required? "required": "";
	$locked = $locked? "locked": "";

	$id = ($id != null ) ? "id=".$id: ""; 
	$option_class = ($option_class != null ) ? "class=".$option_class: ""; 

	// Process Structure
	$label_class = ( $show_label ) ? "col-xs-12 col-sm-3 text-left": ""; 
	$input_container_class = ( $show_label ) ? "col-xs-12 col-sm-9": ""; 

@endphp
<div class="row margin-bottom">
@if( $show_label )
	<label class="{{ $label_class }}"> 
	    {{ ucwords($label) }} 
		@if ( $required == 'required'  )
		 *
		@endif
	</label>
@endif 
	<div class="{{ $input_container_class }}"> 
		<select {{ $id }} class="form-control {{ $select_class }}" name="{{ $name }}" {{ $required }} {{ $locked }}>
			@if( $no_select == true )
				<option {{ $option_class }} disabled 
					@if( $selected == ""  )
		  				selected
		  			@endif
				>{{ $no_select_text }}</option>
			@endif

			@if( $data_arrangement == 'value_name_as_key')
				@foreach( $options_data as $option)
			  		@php
						if ( is_array( $option ) ){
			  				$option = (object)$option;
						}
			  		@endphp
			  		<option {{ $option_class }} value="{{ $option->value }}" 
			  			@if( $selected == $option->value )
			  				selected
			  			@endif
			  		>{{ $option->name }}
			  		</option>	
			  	@endforeach
			@else
				@foreach( $options_data as $key => $option)
			  		<option {{ $option_class }} value="{{ $key }}" 
			  			@if( $selected == $key)
			  				selected
			  			@endif
			  		>{{ $option }}</option>	
			  	@endforeach
		  	@endif
		</select>

		@if (!empty($this_errors) && $show_error)
			Input Setup Error: 
			@foreach( $this_errors as $error)
				<span class="text-i">
					{{ $error }}	
				</span>.
			@endforeach
		@endif
		{{ $info }}
		@if ($errors->has($name))
	    <span class="help-block">
	        <strong>{{ $errors->first($name) }}</strong>
	    </span>
		@endif
	</div>
</div>

{{--
// Usage //

@component('general.library.input_dropdown', [
	'show_label' => true/false*,
	'label' => 'label_name',
	'id' => 'id',
	'name' => 'input_name',
	'required' => true/false*,
	'locked' => true/false*,
	'no_select' => true*/false, 
	'no_select_text' => 'Select your option',
	'selected'=> "selected->value",
	'select_class' => 'class_name',
	'option_class' => 'class_name',
	'data_arrangement' => 'value_name_as_key* / value_as_key',
	'options_data' => [	'value' => 'value1',
			'name' => 'name1',
		],
		[	'value' => 'value2',
			'name' => 'name2',
		],
	'show_error' => true*/false,
])
@slot('info')
	
@endslot
@endcomponent
--}}

