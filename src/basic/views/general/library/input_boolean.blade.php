@php
	// default value
	if( !isset($toggle) ) $toggle = false;
	if( !isset($id) ) $id = "";
	if( !isset($name) ) $name = "";
	if( !isset($show_label) ) $show_label = false;
	if( !isset($label) ) $label = $name;
	if( !isset($required) ) $required = false;
	if( !isset($locked) ) $locked = false;	
	if( !isset($default) ) $default = 1;	
	if( !isset($selected) ) $selected = "";
	if( !isset($true_display) ) $true_display = "";
	if( !isset($false_display) ) $false_display = "";						
	if( !isset($select_class) ) $select_class = "";			
	if( !isset($option_class) ) $option_class = "";
	if( !isset($toggle_class) ) $toggle_class = "";
	if( !isset($toggle_size) ) $toggle_size = "small";
	if( !isset($toggle_onstyle) ) $toggle_onstyle = "";
	if( !isset($toggle_offstyle) ) $toggle_offstyle = "";					
	if( !isset($show_error) ) $show_error = true;	
	if( !isset($info) ) $info = "";

	$this_errors = [];
	// error (only for important)
	if( $name == null ) array_push($this_errors , "No Input Name (name)" );
	
	// Process Data
	$required = $required? "required": "";
	$locked = $locked? "locked": "";

	$id = ($id != null ) ? "id=".$id: ""; 
	$option_class = ($option_class != null ) ? "class=".$option_class: ""; 
	$toggle_class = ($toggle_class != null ) ? "class=".$toggle_class: ""; 

	// Process Structure
	$label_class = ( $show_label ) ? "col-xs-12 col-sm-3 text-left": ""; 
	$input_container_class = ( $show_label ) ? "col-xs-12 col-sm-9": ""; 

	if($selected != null){
		$default = $selected;
	}

@endphp
<div class="row margin-bottom">
@if( $show_label )
	<label class="{{ $label_class }}"> 
	    {{ ucwords($label) }} 
		@if ( $required == 'required'  )
		 *
		@endif
	</label>
@endif 
<div class="{{ $input_container_class }}"> 
@if($toggle) 
	@php
		if($true_display == null ) $true_display = "On";
		if($false_display == null ) $false_display = "Off";
	@endphp
	<input type="checkbox" {{ $toggle_class }}  
		@if( $default )
			checked
		@endif
	data-toggle="toggle" data-size="{{$toggle_size}}" data-on="{{$true_display}}" data-off="{{$false_display}}"
		@if($toggle_onstyle != null)
			data-onstyle="{{$toggle_onstyle}}"
		@endif 
		@if($toggle_offstyle != null)
			data-offstyle="{{$toggle_offstyle}}"
		@endif 
	onchange="( $(this).is(':checked'))? $('input[name={{$name}}]').val(1): $('input[name={{$name}}]').val(0)"
	>
	<input type="hidden" name="{{ $name }}" value="{{ $default }}">
@else
	<select {{ $id }} class="form-control {{ $select_class }}" name="{{ $name }}" {{ $required }} {{ $locked }}>
			@php
				if($true_display == null ) $true_display = "True - 1";
				if($false_display == null ) $false_display = "False - 0";
			@endphp
			@foreach( ["1" => $true_display,  "0" =>  $false_display ] as $key => $option)
		  		<option {{ $option_class }} value="{{ $key }}" 
		  			@if( $default == $key)
		  				selected
		  			@endif
		  		>{{ $option }}</option>	
		  	@endforeach

		</select>

		@if (!empty($this_errors) && $show_error)
			Input Setup Error: 
			@foreach( $this_errors as $error)
				<span class="text-i">
					{{ $error }}	
				</span>.
			@endforeach
		@endif
@endif
</div>
{{ $info }}
@if ($errors->has($name))
<span class="help-block">
    <strong>{{ $errors->first($name) }}</strong>
</span>
@endif
</div>

{{--
// Usage //

@component('general.library.input_boolean', [
	'show_label' => true/false*,
	'label' => 'label_name',
	'id' => 'id',
	'name' => 'input_name(use in controller)',
	'true_display' => 'On*/True*',
	'false_display' => 'Off*/False*',
	'required' => true/false*,
	'locked' => true/false*,
	'default'=> true*/false,
	'selected' => '',
	'select_class' => 'class_name',
	'option_class' => 'class_name',
	'toggle' => true/false*,
	'toggle_class' => 'class_name',
	'toggle_size' => 'large/normal/small*/mini',
	'toggle_onstyle' => 'primary*/success/info/warning/danger/default',
	'toggle_offstyle' => 'primary*/success/info/warning/danger/default',
	'show_error' => true*/false,
])
@slot('info')
	
@endslot
@endcomponent
--}}

