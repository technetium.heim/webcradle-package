@php
	// default value

	if( !isset($btn_id) ) $btn_id = "";
	if( !isset($btn_block) ) $btn_block = true;
	if( !isset($btn_class) ) $btn_class = "";
	if( !isset($container_class) ) $container_class = "";
	if( !isset($text) ) $text = "";
	if( !isset($info) ) $info = "";

	$btn_block = $btn_block? "btn-block": "";

	$btn_id = ($btn_id != null ) ? "id=".$btn_id: ""; 

@endphp
<div class='{{ $container_class }}'>
	<button {{ $btn_id }} class='btn btn-load {{ $btn_block }} {{ $btn_class }}'>
		<span class="btn-load-text">{{ $text }}</span>
		<span class="btn-load-icon hidden"><i class="fa fa-spinner fa-spin"></i></span>
	</button>
</div>

<script type="text/javascript">
	$('.btn-load').parents('form').submit(function( event ) {
	  $(this).find(".btn-load").addClass('disabled');
	  $(this).find(".btn-load-text").addClass('hidden');
	  $(this).find(".btn-load-icon").removeClass('hidden');
	});

	function resetBtnLoad(){
		event.preventDefault();
		$(".btn-load").removeClass('disabled');
	  	$(".btn-load-text").removeClass('hidden');
	  	$(".btn-load-icon").addClass('hidden');
	}
</script>
{{--
// Usage //
// LIMITATION: 
	- Use for button in forms only (on form submit)
	- Manually set back using jq (resetBtnLoad();)

@component('general.library.btn_load', [ 
	'btn_id' => 'btn_id',
	'text' => 'input_name',
	'btn_block' => true/false*,
	'btn_class' => "",
	'container_class' => "",
])
@slot('info')
	
@endslot
@endcomponent
--}}