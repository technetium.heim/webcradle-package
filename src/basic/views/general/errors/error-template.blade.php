@php
	if(!isset($title)) $title = "Errors";
	if(!isset($meta)) $meta['description'] = "Errors";
	$body_bg_color = 'white';
@endphp
@extends('WCView::general.base.main')

	@component('WCView::general.components.panels.main')
	    @slot('title')
	    	<div class="text-lg">Errors</div>
	    @endslot
	    @slot('content')
		
			<div>Sorry.</div>

			@yield('error-message')
			
			<div> 
				<a href="{{ route('cradle.home') }}">
					Return to Home.
				</a>	
			</div>
		@endslot
	@endcomponent

