<?php
namespace Cradle\basic;

use Illuminate\Console\Command;

use Cradle\basic\console\FixErrorCommand;
use Cradle\basic\console\UpdatePublishCommand;
use Cradle\basic\console\MakeAdminCommand;

use Cradle\basic\console\InstallCommand;
use Cradle\basic\console\ForceUpdateCradleCommand;
use Cradle\basic\console\UpdateCradleCommand;
use Cradle\basic\console\UpdateCradleFontCommand;
use Cradle\basic\console\UpdateCradleAssetsCommand;
use Cradle\basic\console\UpdateCradleScssUpdateCommand;

// This file register new artisan commands for Web Cradle

trait CradleCommands
{
	protected function registerCommands() 
	{	
		// Fix Error Command
		$this->registerCommand( 'command.cradle.fixError' , new FixErrorCommand );
		
		$this->registerCommand( 'command.cradle.updatePublish' , new UpdatePublishCommand );

		// Make Admin Command
		$this->registerCommand( 'command.cradle.makeAdmin' , new MakeAdminCommand );

		// Basic Commands
		$this->registerCommand( 'command.cradle.install' , new InstallCommand );
		$this->registerCommand( 'command.cradle.forceUpdate' , new ForceUpdateCradleCommand );
		$this->registerCommand( 'command.cradle.update' , new UpdateCradleCommand );
		$this->registerCommand( 'command.cradle.updatefont' , new UpdateCradleFontCommand );
		$this->registerCommand( 'command.cradle.updateAssets' , new UpdateCradleAssetsCommand );
		$this->registerCommand( 'command.cradle.updateScss' , new UpdateCradleScssUpdateCommand );
		
	}


	protected function registerCommand( $name , Command $command)
	{
		$this->app->singleton( $name , function($app) use ($command) 
		{
            return $command;
        });

		$this->commands( $name );

	}







}