<?php
namespace Cradle\basic\defaults;

use App\Http\Controllers\component\CradleHint;

trait CradleController
{	
  	protected function cradleHint()
    {
    	return new CradleHint;
    }

    protected function setTitle ($title, $meta="") 
    {
    	$meta = (isset($meta))? $meta: $title; 
    	return [
    		'title'=> $title,
    		'meta' => [ 
    			'description'=> $meta
    		]
    	];
    }
}