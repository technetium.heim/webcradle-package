<?php

namespace Cradle\basic\defaults;

use Closure;
// use Illuminate\Support\Facades\Auth;
use Session;
use App;
use Auth;
use Cradle;

class CradleGlobalMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
    	$this->setLocale($request, $next, $guard );
        $this->checkViewLinks();
        return $next($request);
    }

    protected function setLocale( $request, $next, $guard = null ) 
    {
    	if ( Auth::check() ) {
            // If login, check it pref language. 

            if( Auth::user()->setting ) {
                // if have, setLocale to the language
                if( Auth::user()->setting->language != null) {
                    App::setLocale( Auth::user()->setting->language ); 
                }
            }   
             
        }else { 
            if ( Session::has('locale') ) {
                App::setLocale( Session::get('locale') ); 
            }
        }        
    }

    protected function checkViewLinks()
    {
        if( Cradle::config('project.loadview.custom_general_view') ) {
            $path = Cradle::config('project.loadview.custom_general_view_path');
            $path = $path != ''?  $path : resource_path('/views');
            app('view')->replaceNamespace('WCView', $path) ;
        }
        if( Cradle::config('project.loadview.custom_user_view') ) {
            $path = Cradle::config('project.loadview.custom_user_view_path');
            $path = $path != ''?  $path : resource_path('/views');
            app('view')->replaceNamespace('UserView', $path) ;
        }
        if( Cradle::config('project.loadview.custom_admin_view') ) {
            $path = Cradle::config('project.loadview.custom_admin_view_path');
            $path = $path != ''?  $path : resource_path('/views');
            app('view')->replaceNamespace('AdminView', $path) ;
        }
    }

}
