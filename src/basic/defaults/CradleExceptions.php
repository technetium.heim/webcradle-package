<?php
namespace Cradle\basic\defaults;

use Illuminate\Session\TokenMismatchException;

trait CradleExceptions
{
 	protected function defaultRenderPage($exception)
    {
        if ($exception instanceof TokenMismatchException) {
            return response()->view('WCView::general.errors.TokenMismatched', [], 500);
        }

        return false;
    }

}