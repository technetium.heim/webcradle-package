<?php
namespace Cradle\basic\defaults;

use Illuminate\Database\Eloquent\SoftDeletes;
use Cradle\basic\supports\CascadeDeletes\CascadeSoftDeletes;

trait CradleModel
{
	use SoftDeletes;
	use CascadeSoftDeletes;
	
	public function __construct() 
	{
		parent::__construct();
		$this->dates = ['deleted_at'];
	}
}