<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\component\CradleHint;
use Cradle\basic\defaults\CradleController as CradleDefaultController;

class CradleController extends Controller
{
    use CradleDefaultController;
}