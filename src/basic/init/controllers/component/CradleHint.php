<?php
namespace App\Http\Controllers\component;

class CradleHint
{
	public function authMessage($request, $condition, $user="user") {

		switch ($condition) {
			case 'login':
				$msg = "Logged in as ". $user.".";
				break;
			case 'logout':
				$msg = "Logged out.";
				break;
			case 'register':
				$msg = "New Account created. Logged in as ". $user.".";
				break;
			case 'reset_password':
				$msg = "Your password has been reset.";
				break;	
			default:
				$msg = $condition;
				break;
		}
		if (isset($msg))
			$request->session()->flash('hint', $msg);
	}


}