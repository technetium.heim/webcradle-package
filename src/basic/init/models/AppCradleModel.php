<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cradle\basic\defaults\CradleModel;

class AppCradleModel extends Model
{
	use CradleModel;
	// ALL MODEL IN APP SHOULD BE EXTENDING FROM THIS MODEL
	// IMPLEMENT FOR ALL MODEL OF THE APP.


}