function isset(param)
{
    if(typeof param !== 'undefined' && param != null && param != '') {
        return true;
    }
    else {
        return false;
    }
}

//START: Cookie
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/;";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length,c.length);
            }
        }
        return "";
    }

    function deleteCookie(cname) {
        document.cookie = cname + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/;';
    }
//END

//Auth Validation and pass
function ajax_validate(submit_selector, target_route , add_var="" )
{
 $( submit_selector ).off('click');
 $( submit_selector ).on('click', function(e) {
    e.preventDefault(); 
    //send ajax
    var form = $(this).closest('form');
    
    if (add_var != "" ) {
        $.each(add_var, function(name, val) {
            $('<input />').attr('type', 'hidden')
                    .attr('name', name )
                    .attr('value', val )
                    .appendTo(form);
        });
    }
    
    var formData = form.serialize();
    $.ajax({
        method: "POST",
        url: target_route,
        data: formData,
        dataType: 'json',
        success: function(response_data) {
            // alert(JSON.stringify(response_data));

            if (response_data.status == "success") 
            {    
                if ('redirect' in response_data)
                    window.location= response_data.redirect;
                else
                    window.location.reload(false);
            }
            else 
            {   
                print_validate_errors (form, response_data) 
            }
        },
        error: function(data) {
            var errors = data.responseJSON;
            print_validate_errors (form, errors) 
        }
    });
});

}

function print_validate_errors (form, errors) {
    $('.form-group').removeClass('has-error');
    $('.help-block').remove();

    $.each( errors, function( key, value ) {
        form.find("input[name="+key+"]").parents('.form-group').addClass('has-error');

        error_block =  '<span class="help-block">'
                        + '<strong>'+value+'</strong>'
                        + '</span>'
        form.find("input[name="+key+"]").after(error_block);
    });
}

function openSidebar () {
    // if ($(window).width() <= "767" ) {
    //     if ( $(".with-sidebar").hasClass("sidebar-open")) {
    //         $(".with-sidebar").removeClass("sidebar-open");
    //     }else {
    //         $(".with-sidebar").addClass("sidebar-open");
    //     }
    // }else {
    //     if ( $(".with-sidebar").hasClass("sidebar-close")) {
    //         $(".with-sidebar").removeClass("sidebar-close");  
    //     }else {
    //         $(".with-sidebar").addClass("sidebar-close");
    //     }
    // }

    if ($(window).width() <= "767" ) {
        if ( $(".sidebar-main").hasClass("open")) {
            $(".sidebar-main").removeClass("open");
        }else {
            $(".sidebar-main").addClass("open");
        }
    }
}

function showHint(msg){
    $("#hint").html(msg);
    $("#hint").show().delay(1500).fadeOut(300);
};

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
