<?php

namespace Cradle\tools;
use Cradle\tools\foundation\WCMailTrait;
/* 
Send Email Settings
*/

class WCMail
{
	use WCMailTrait;
	// Copy and edit Send Custom in your project
    private function sendCustom( $target_email, $mailer ){
    	
    	if( !config('project.custom_send_mail_class')){
    		return "Error. Please Setup Config'custom_send_mail_class'.";            
    	}
    	

    	$custom_path = config('project.custom_send_mail_class');

    	if( !class_exists($custom_path)){
    		return "Error. '".$custom_path."' Class not found.";
    	}

    	$custom = new $custom_path;
    	$response = $custom->send( $target_email, $mailer );
    	return $response;      
    }
}