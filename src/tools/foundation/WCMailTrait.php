<?php

namespace Cradle\tools\foundation;

use Illuminate\Support\Facades\Mail;
use Config;
/* 
Send Email Settings
*/

trait WCMailTrait
{
    private function sendCustom( $target_email, $mailer ){ 
        return "Please Setup Custom WCMail";            
    }

    private function sendDefault($target_email , $mailer){
        // When Default
        try{
            Mail::to($target_email)->send( $mailer );
        }catch(\Exception $e){
            // Get error here
            $msg = "Failed. Exception Errors.";
            if(env('APP_DEBUG', true) ) $msg .= ' '. $e->getMessage();
            return $msg;
        }

        return "Success";   
    }

    public function send( $target_email , $mailer ){
        $env = env('MAIL_ENV', 'default');
        //default => use default email settings
        //custom => customize email function
        if( $env == 'custom' ){
            return $this->sendCustom( $target_email , $mailer );
        }else{
           return $this->sendDefault( $target_email , $mailer ); 
        }
    }
}